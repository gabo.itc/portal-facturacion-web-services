<?php
// declare(strict_types=1);
namespace WebServices\sifei;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use WebServices\cfdiSAT\FacturaException;
use WebServices\cfdiSAT\Funciones;
use WebServices\cfdiSAT\Configuracion;
use WebServices\cfdiSAT\ConexionDB;
use SoapClient;
use PDO;
// include '../cfdiSAT/Configuracion.php';

class WebServices{

	private $container;

   // constructor receives container instance
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	private function getParametros(){
		// $app = Slim::getInstance();/*obtenemos la instancia de Slim*/
		
		// $allPostVars = $app->request->post();
		// $parametros = json_decode(file_get_contents('php://input'), true);

		$json = file_get_contents('php://input');
		$params = json_decode($json);
		// $parametros = json_decode(file_get_contents('php://input'), true);

		// $allPostVars = $args;
		return $params;
	}

	private function validaParametrosRecibidos($arrParametros=array())
	{
		// extract($this->getParametros());//extraemos las variables post
		$parametros = $this->getParametros();//extraemos las variables post
		$parametrosNoRecibidos = "";

		foreach ($arrParametros as $key => $value) {
			if(is_null($parametros) || !isset($parametros->$value)){/*si la variable existe*/
				$parametrosNoRecibidos .= empty($parametrosNoRecibidos) ? "" : ",";
				$parametrosNoRecibidos .= $value;
			}
		}
		if(!empty($parametrosNoRecibidos)){
			$parametrosNoRecibidos = "Parametros No Recibidos [$parametrosNoRecibidos]";
		}
		return $parametrosNoRecibidos;
	}
	


	public function CFDIConsultar(Request $request, Response $response, array $args): Response
	{
		$arrParametros = array('uuid', 'rfcEmisor','rfcReceptor','total','fe');
		$validacion = $this->validaParametrosRecibidos($arrParametros);
		// $validacion = "";

		// extract($this->getParametros());//extraemos el post, cada parametro es convertido en variable

		$parametros = $this->getParametros();//extraemos las variables post
		try{
			$error = null;
			if(empty($validacion)){

				$config = new Configuracion();
				if(defined('urlSIFEICancelaciones')){
					$url = urlSIFEICancelaciones;
					try {
						
						$configSoap = array(
							'soap_version'=>SOAP_1_2,
							'connection_timeout' => 600,
							'trace' => 1,
							'exceptions' => true,
							'cache_wsdl' => WSDL_CACHE_NONE,
							'features' => 0
						);

						// $url = "https://consultaqr.facturaelectronica.sat.gob.mx/consultacfdiservice.svc?wsdl";
						// $url = "http://devcfdi.sifei.com.mx:8888/CancelacionSIFEI/Cancelacion?wsdl";
						// $url = "https://sat.sifei.com.mx:9000/CancelacionSIFEI/Cancelacion?wsdl";
						// $url = "https://sat.sifei.com.mx:8443/SIFEI/SIFEI?wsdl";
						$client = new SoapClient( $url, $configSoap );

					// var_dump($client->__getFunctions());/*para saber las funciones del WS*/
					// var_dump($client->__getTypes());/*para saber los parametros que recibe cada Funcion*/
						list($strTotalEntero, $strTotalDecimal) = preg_split('[\.]', $parametros->total);
						$strTotalEntero = str_pad($strTotalEntero,  18, "0",STR_PAD_LEFT); 
						$strTotalDecimal = str_pad($strTotalDecimal,  6, "0"); 
						$strTotal = $strTotalEntero.".".$strTotalDecimal;
					// echo "Total: ".$strTotal.PHP_EOL;
						$result = $client->consultaSATCFDI( [ "usuarioSIFEI" => usuarioSIFEI,
							"passwordSIFEI" => passwordSIFEI, 
							"id"=>$parametros->uuid,
							"re"=>$parametros->rfcEmisor,
							"rr"=>$parametros->rfcReceptor,
							"tt"=>$strTotal,
							"fe"=>$parametros->fe ] );

						$respuesta = $result->return;

						$funcion= __FUNCTION__;
						$timbres_usados = 0;
						Funciones::cargarSolicitud($funcion, $timbres_usados, 200, $respuesta);

						$respuesta = array();
						$respuesta["error"]   = false;
						$respuesta["message"] = ""; 
						$respuesta["body"]    = $respuesta;


						// $respuesta = 'urlSIFEICancelaciones';
						// $response->getBody()->withJson($respuesta);
						// $funcion= __FUNCTION__;
						// $timbres_usados = 0;
						// Funciones::cargarSolicitud($funcion, $timbres_usados, 200, $respuesta);
					} catch ( SoapFault | Exception $ex ) {


						if(isset($ex->detail->SifeiException)){/*si existe el componente*/
							FacturaException::$sifeiCodigo = $ex->detail->SifeiException->codigo;
							FacturaException::$sifeiError = $ex->detail->SifeiException->error;
							FacturaException::$sifeiMessage = $ex->detail->SifeiException->message;
							$error = $ex->detail->SifeiException->error;
						}else{
							$error = $ex->getMessage();
						}

						FacturaException::$codigo_estatus = 500;
						throw new FacturaException($error);
					}

					// $response->getBody()->write(urlSIFEICancelaciones);
				}else{
					$response->getBody()->write('urlSIFEICancelaciones No Existe');
				}

			}else{
				FacturaException::$codigo_estatus = 400;
				throw new FacturaException($validacion);
			}
		}catch(FacturaException $ex){
			FacturaException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		// return $response->withHeader('Content-Type', 'application/json');
		return $response;
	}

	public function CFDITimbrar(Request $request, Response $response, array $args): Response
	{
		$arrParametros = array('xmlcfdi');
		$validacion = $this->validaParametrosRecibidos($arrParametros);

		try{
			/*Validamos que el usuario cuente con timbres disponibles en caso de no ser asi regresara una excepcion*/
			Funciones::validarTimbresDisponibles();
			$error = null;
			if(empty($validacion)){
				// extract($this->getParametros());//extraemos el post, cada parametro es convertido en variable
				$parametros = $this->getParametros();
				if(is_string($parametros->xmlcfdi)){
					$arrayCFDI = json_decode($parametros->xmlcfdi);
					if(is_object($arrayCFDI)){
						$rutaZIP = Funciones::crearXML($arrayCFDI,true);
						$contenidoZip = file_get_contents($rutaZIP);
						unlink($rutaZIP);/*Eliminamos el ZIP despues de leer el contenio*/
						
						/*Conectando al WS de SIFEI*/
						$url = urlSIFEI;
						try{
							$client = new SoapClient($url, array("trace" => 1, "exception" => 0)); 
							$serieParaSifei = $arrayCFDI->Comprobante->Serie;
							// $serieParaSifei = isset($serieParaSifei) ? $serieParaSifei : "";
							$serieParaSifei = "";
							$result = $client->getCFDI( [ 
								"Usuario" => usuarioSIFEI,
								"Password" => passwordSIFEI, 
								"archivoXMLZip" => $contenidoZip,
								"Serie" => $serieParaSifei,
								"IdEquipo" => IdEquipoSIFEI
							] );

							$contentResponse = ($result->return);
							
							$funcion= __FUNCTION__;
							$timbres_usados = 1;
							Funciones::cargarSolicitud($funcion, $timbres_usados, 200, $contentResponse);

							$nombreAleatorio = "Respuesta_".rand();
							/*nombre random del archivo zip*/
							$respuestaZip = $nombreAleatorio.".zip";
							/*creamos el archivo y le cargamos el contenido*/
							file_put_contents($respuestaZip, $contentResponse);

							/*generamos un nombre de carpeta aleatoria*/
							$carpetaZip = $nombreAleatorio;

							// $xmlTimbrado = "";
							// $zip = new ZipArchive;
							// if ($zip->open($respuestaZip) === TRUE) {
							// 	/*contenido del archivo del indice 0*/
							// 	$xmlTimbrado = $zip->getFromIndex(0);
							// 	/*nombre del archivo del indice 0*/
							// 	$xmlComprimido = $zip->getNameIndex(0);
							// 	/*quitamos la extension*/
							// 	$nombrePDF = pathinfo($xmlComprimido,PATHINFO_FILENAME);

							// 	/*generamos el PDF*/
							// 	$archivoPDF = GeneraPDF::generar($nombrePDF,$xmlTimbrado,FALSE);
							// 	if(!empty($archivoPDF)){
							// 		/*agregamos el PDF al ZIP*/
							// 		$zip->addFile($archivoPDF, basename($archivoPDF));
							// 		/*cerramos el ZIP*/
							// 		$zip->close();

							// 		/*Eliminamos el PDF despues de agregarlo al ZIP*/
							// 		unlink($archivoPDF);
							// 	}else{
							// 		/*cerramos el ZIP*/
							// 		$zip->close();
							// 	}

							// } else {

							// }

							$nameFile = basename($respuestaZip);
							$response = $response->withHeader('Content-Description','File Transfer');
							$response = $response->withHeader('Content-Disposition',"attachment; filename={$nameFile}");
							$response = $response->withHeader('Content-Type','application/zip');
							$response->getBody()->write(file_get_contents($respuestaZip));
							unlink($respuestaZip);
							
						}catch(SoapFault $ex){

							FacturaException::$codigo_estatus = 500;
							if(isset($ex->detail->SifeiException)){/*si existe el componente*/
								FacturaException::$codigo_estatus = 400;
								FacturaException::$sifeiCodigo = $ex->detail->SifeiException->codigo;
								FacturaException::$sifeiError = $ex->detail->SifeiException->error;
								FacturaException::$sifeiMessage = $ex->detail->SifeiException->message;
								$error = $ex->detail->SifeiException->error;
							}else{
								$error = $ex->getMessage();
							}

							throw new FacturaException($error);
						}

					}else{
						FacturaException::$codigo_estatus = 400;
						throw new FacturaException("el parametro recibido no cumple con la estructura del objeto necesario ");
					}
				}else{
					FacturaException::$codigo_estatus = 400;
					throw new FacturaException("el valor del parametro no es string ");
				}
			}else{
				FacturaException::$codigo_estatus = 400;
				throw new FacturaException($validacion);
			}

		}catch(CFDIException $ex){
			CFDIException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(FacturaException $ex){
			FacturaException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}
		return $response;
	}

	/*
	**
	*Obtener el JSON del uso de cfdi permitidos
	*/
	public function CatalogoUsoCFDI(Request $request, Response $response, array $args): Response
	{
		try{
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();

			$sql = "SELECT clave,descripcion,fisica,moral FROM ".DB_NAME_REAL.".".cat_cfdi_uso_cfdi." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos) > 0){
				$respuesta = $datos;
				$estatus = 200;
			}else{
				$estatus = 300;
				$respuesta["error"]   = true;
				$respuesta["message"] = "Catalogo Vacio";
			}
			$response = $response->withStatus($estatus);

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
	}

	/*
	**
	*Obtener el JSON del regimen fiscal permitidos
	*/
	public function CatalogoRegimenFiscal(Request $request, Response $response, array $args): Response
	{
		try{
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();

			$sql = "SELECT clave,descripcion,fisica,moral FROM ".DB_NAME_REAL.".".cat_cfdi_regimen_fiscal." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos) > 0){
				$respuesta = $datos;
				$estatus = 200;
			}else{
				$estatus = 300;
				$respuesta["error"]   = true;
				$respuesta["message"] = "Catalogo Vacio";
			}
			$response = $response->withStatus($estatus);

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
	}

	/*
	*
	*Obtener el JSON de las Formas de Pago Registradas
	*/
	public function CatalogoFormaPago(Request $request, Response $response, array $args): Response
	{
		try{
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();

			$sql = "SELECT clave,descripcion FROM ".DB_NAME_REAL.".".cat_cfdi_forma_pago." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos) > 0){
				$respuesta = $datos;
			}else{
				$respuesta["error"]   = true;
				$respuesta["message"] = "";
				$response = $response->withStatus(300);
			}


			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, 200, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
	}
}

?>