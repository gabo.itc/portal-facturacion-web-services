<?php

class DBException extends Exception {
	public static $codigo_estatus = 0;
	public static $funcion_nombre = "";
	// function getMensaje(){
	// 	$errorMsg = 'Error en la Linea: '.$this->getLine().' en '.$this->getFile()
	// 	.': <b>'.$this->getMessage()."</b>";
	// 	return $errorMsg;
	// }
	function getMensaje(){
		$errorMsg = $this->getMessage();
		return $errorMsg;
	}

	function salidaError($estatus_code = 500){
		$funcion = self::$funcion_nombre;
		if(is_null($estatus_code)){
			$estatus_code = self::$codigo_estatus;
		}

		$errorMsg = 'Error en la Linea: '.$this->getLine().' en '.$this->getFile().': '.$this->getMessage();
		
		$response = array();
		$response["error"]   = true;
		$response["message"] = $this->getMessage(); 
		$response["body"]    = null;

		// $estatus_code = 500;
		http_response_code($estatus_code);
		header('Content-Type: application/json');
		echo json_encode($response);
		exit();
	}

}
?>