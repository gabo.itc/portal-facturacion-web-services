    <style type="text/Css">
        .img-logo {
            align-items: center;
            width: auto;
            height: auto;
            max-height: 35mm;
            border-color: #DDD; 
            border-style: solid;
            border-width: 3px
        }

        .th-titulo{
            font-size: 11px;
            font-weight: bold;
            padding: 3px;
            height: 13px;
            background-color: #DDD;
        }

        /***********************************************************************/
        .info-factura{
            font-size: 8px;
            border-collapse: collapse;
        }
        .info-factura tr th,.info-factura tr td{
            border: #DDD 2px solid;
            vertical-align: middle;
            padding: 0 2 0 2;
        }
        .info-factura .label{
            /*width: 125px;*/
            font-weight: bold;
        }
        .info-factura .valor{
            /*min-width: 180px;*/
            /*width: 60%;*/
            font-weight: normal;
        }
        /***********************************************************************/
        .info-detalle{
            font-size: 8px;
            border-collapse: collapse;
        }
        .info-detalle tr th,.info-detalle tr td{
            border: #DDD 2px solid;
            vertical-align: middle;
            padding: 0 5 0 5;
            min-height: 20px;
            /*            border-collapse: collapse;*/
        }
        .info-detalle .label{
            /*width: 80px;*/
            font-weight: bold;
        }
        .info-detalle .valor{
            /*width: 130px;*/
            font-weight: normal;
            height: 10px;
        }
        /***********************************************************************/

        .info-concepto{
            font-size: 8px;
            border-collapse: collapse;
            width: 100%;
            max-width: 100%;
            vertical-align: middle;
        }
        .info-concepto .cantidad{
            width: 7%;
        }
        .info-concepto .descripcion{
            width: 48%;
        }
        .info-concepto .unidad{
            width: 9%;
        }
        .info-concepto .claveunidad{
            width: 5%;
        }
        .info-concepto .clavprodserv{
            width: 7%;
        }
        .info-concepto .preciounitario{
            width: 12%;
        }
        .info-concepto .importe{
            width: 12%;
        }

        .info-concepto .label{
            /*width: 80px;*/
            font-weight: bold;
        }
        .info-concepto .valor{
            /*width: 130px;*/
            font-weight: normal;
        }

        .info-concepto tr th,.info-concepto tr td{
            border: #DDD 2px solid;
            vertical-align: middle;
            padding: 0 2 0 2;
            min-height: 10px;
            /*            border-collapse: collapse;*/
        }

    </style>
    
    <page backtop="38mm" backbottom="45mm">
        <!-- <page backtop="42mm" backbottom="40mm"  backleft="5mm" backright="5mm"> -->
            <page_header>
            <div style="width: 100%; ">
                <table cellspacing="0" class="info-detlle" style="width: 100%;" cellspacing="0">
                    <tr style=" height: 38mm; vertical-align: middle;">
                        <td style="width: 47.5%; max-width: 47.5%" align="center">
                            <?php if(isset($url_logo)) : ?>
                                <img src="<?=$url_logo?>" alt="Logo" class="img-logo" />
                            <?php endif;?>
                        </td>
                        <td style="width: 5%; max-width: 5%">
                        </td>
                        <td style="width: 47.5%; max-width: 47.5%;">
                            <table class="info-factura" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 100%;">
                                <tr>
                                    <th colspan="2" align="center" class="th-titulo" style="width: 100%; max-width: 100%;">Factura Electronica </th>
                                </tr>

                                <tr>
                                    <td class="label">Paginas</td>
                                    <td class="valor">[[page_cu]]/[[page_nb]]</td>
                                </tr>

                                <tr>
                                    <td class="label">Tipo Comprobante</td>
                                    <td class="valor"><?=$comprobanteTipoDeComprobante?></td>
                                </tr>

                                <tr>
                                    <td class="label">Serie - Folio</td>
                                    <td class="valor"><?=$facturaSerieFolio?></td>
                                </tr>
                                <tr>
                                    <td class="label">Fecha</td>
                                    <td class="valor"><?=$comprobanteFecha?></td>
                                </tr>

                                <tr>
                                    <td class="label">No. Serie Certificado SAT</td>
                                    <td class="valor"><?=$timbreFiscalDigitalNoCertificadoSAT?></td>
                                </tr>

                                <tr>
                                    <td class="label">No. Serie del CSD</td>
                                    <td class="valor"><?=$comprobanteNoCertificado?></td>
                                </tr>

                                <tr>
                                    <td class="label">Folio Fiscal</td>
                                    <td class="valor"><?=$timbreFiscalDigitalUUID?></td>
                                </tr>

                                <tr>
                                    <td class="label">Fecha y Hora de Certificación</td>
                                    <td class="valor"><?=$timbreFiscalDigitalFechaTimbrado?></td>
                                </tr>

                                <tr>
                                    <td class="label">Lugar de Expedición (CP)</td>
                                    <td class="valor"><?=$comprobanteLugarExpedicion?></td>
                                </tr>

                            </table> 
                        </td>
                    </tr>
                </table>

                <div style=" position: relative; width: 10%;"></div>
            </div>
            </page_header>

            <page_footer>
            <div>
                <?php if(isset($urlQR)) :?>
                    <table class="page_footer info-detalles" style="width: 100%; vertical-align: middle;" cellspacing="0">
                        <tr>
                            <td rowspan="8" style="width: 20%; padding-right: 5px">
                                <a href="<?=$urlQR?>"><qrcode value="<?=$urlQR?>" ec="L" style="width: 100%; background-color: white; color: black"></qrcode></a>
                            </td>
                            <td class="label" align="center" style="width: 80%; background-color: #DDD; font-size: 9px;">Sello del SAT</td>
                        </tr> 

                        <tr>
                            <td class="label" align="left" style="width: 80%; font-size: 6px;"><?=chunk_split($selloSAT,160,' ')?></td>
                        </tr>

                        <tr>
                            <td class="label" align="center" style="width: 80%;" id="SeparadorTD"></td>
                        </tr>

                        <tr>
                            <td class="label" align="center" style="width: 80%; background-color: #DDD; font-size: 9px;">Sello Digital</td>
                        </tr>

                        <tr>
                            <td class="label" align="left" style="width: 80%; font-size: 6px;"><?=chunk_split($selloDigital,160,' ')?></td>
                        </tr>

                        <tr>
                            <td class="label" align="center" style="width: 80%;" id="SeparadorTD"></td>
                        </tr>

                        <tr>
                            <td class="label" align="center" style="width: 80%; background-color: #DDD; font-size: 9px;">Cadena Original</td>
                        </tr>

                        <tr>
                            <td class="label" align="left" style="width: 80%; font-size: 6px;"><?=chunk_split($cadenaOriginalTFD,160,' ')?></td>
                        </tr>

                        <tr>
                            <td class="label" align="center" style="width: 100%; height: 5px" colspan="2" id="SeparadorTD"></td>
                        </tr>

                        <tr>
                            <td class="label" align="center" colspan="2" style="width: 100%; background-color: #999; color: white">Este documento es una representación impresa de un CFDI</td>
                        </tr>
                    <!-- <tr>
                        <td style="height: 14mm"></td>
                    </tr> -->
                </table>
            <?php endif;?>
        </div>
        </page_footer>

        <!-- Saldo de Linea -->
        <!-- <H6 style="margin: -2px;border: 1px solid #000; "></H6> -->

        <table cellspacing="0" class="info-detlle" style="width: 100%; border-spacing: 10px; vertical-align: top;" cellspacing="0">

            <tr>
                <td style="width: 47.5%; max-width: 47.5%;">

                    <table class="info-detalle" cellspacing="0" style="width: 100%;">
                        <tr>
                            <th colspan="2" align="center" class="th-titulo" style="width: 100%;">Emisor</th>
                        </tr>

                        <tr>
                            <th colspan="2" class="valor" style="width: 100%;"><?=$emisorNombre?></th>
                        </tr>

                        <tr>
                            <td class="label">RFC</td>
                            <td class="valor"><?=$emisorRfc?></td>
                        </tr>
                        <tr>
                            <td class="label">Regimen Fiscal</td>
                            <td class="valor"><?=$regimenFiscal?></td>
                        </tr>

                    </table>

                </td>

                <td style="width: 5%;">
                </td>

                <td style="width: 47.5%;">
                    <table class="info-detalle"  cellspacing="0" style="width: 100%;">
                        <tr>
                            <th colspan="2" align="center" class="th-titulo">Receptor</th>
                        </tr>

                        <tr>
                            <th colspan="2" class="valor" style="width: 100%;"><?=$receptorNombre?></th>
                        </tr>

                        <tr align="left">
                            <td class="label">RFC</td>
                            <td class="valor"><?=$receptorRfc?></td>
                        </tr>
                        <tr align="left">
                            <td class="label">Uso CFDI</td>
                            <td class="valor"><?=$receptorUsoCFDI?></td>
                        </tr>

                    </table>
                </td>
            </tr>

        </table>
        <!-- Saldo de Linea -->
        <H6 style="margin: -2px;border: 1px solid #000; "></H6>

        <table class="info-detalle page_header" style="width: 100%; border-spacing: 10px; page-break-inside:auto" cellspacing="0">
            <tr>
                <th colspan="4" align="center" style="width: 100%;" class="th-titulo">
                    Datos Generales Del Comprobante
                </th>
            </tr>
            <tr>
                <td class="label" style="width: 15%;">Moneda</td>
                <td class="valor" style="width: 15%;"><?=$comprobanteMoneda?></td>
                <td class="label" style="width: 15%;">Forma de Pago</td>
                <td class="valor" style="width: 55%;"><?=$comprobanteFormaPago?></td>
            </tr>
            <tr>
                <td class="label" style="width: 15%;">Tipo de Cambio</td>
                <td class="valor" style="width: 15%;"><?=$comprobanteTipoCambio?></td>
                <td class="label" style="width: 15%;">Metodo de Pago</td>
                <td class="valor" style="width: 55%;"><?=$comprobanteMetodoPago?></td>
            </tr>
            <tr>
                <td class="label" style="width: 15%;">Clave Confirmación</td>
                <td class="valor" style="width: 15%;"><?=$comprobanteConfirmacion?></td>
                <td class="label" style="width: 15%;">Condiciones de Pago</td>
                <td class="valor" style="width: 55%;"><?=$comprobanteCondicionesDePago?></td>
            </tr>
        </table>

        <!-- Saldo de Linea -->
        <H6 style="margin: -2px;border: 1px solid #000; "></H6>

        <?php if(isset($conceptoCfdiRelacionados)):?>
            <table class="info-concepto" cellspacing="0">
                <tr>
                    <td class="label" align="left" style="width: 15%; background-color: #DDD; border: #DDD 2px solid !important;">CFDI Relacionados</td>
                    <td class="valor" align="left" style="width: 85%;"><?=$conceptoCfdiRelacionados?></td>
                </tr>
            </table>
        <?php endif;?>
        
        <!-- Saldo de Linea -->
        <H6 style="margin: -2px;border: 1px solid #000; "></H6>

        <table class="info-concepto" cellspacing="0">
            <thead style="display:table-header-group;">
                <tr style="background-color: #DDD">
                    <td class="label cantidad" align="center">Cant.</td>
                    <td class="label descripcion" align="center">Descripción</td>
                    <td class="label unidad" align="center">Unidad</td>
                    <td class="label claveunidad" align="center">Clave Unidad</td>
                    <td class="label clavprodserv" align="center">ClavProd Serv</td>
                    <td class="label preciounitario" align="center">Precio Unitario</td>
                    <td class="label importe" align="center">Importe</td>
                </tr>
            </thead>

            <?php foreach ($arrConceptos as $key => $value) :?>
                <?php //for($i = 0; $i <= 94; $i++) :?>
                <tr style="width: 100%;max-width: 100%;">
                    <td class="valor cantidad" align="center"><?=$arrConceptos[$key]['Cantidad']?></td>
                    <!-- <td class="valor cantidad" align="left">256,234.00</td> -->
                    <td class="valor descripcion" align="left"><?=$arrConceptos[$key]['Descripcion']?></td>
                    <td class="valor unidad" align="center"><?=$arrConceptos[$key]['Unidad']?></td>
                    <td class="valor claveunidad" align="center"><?=$arrConceptos[$key]['ClaveUnidad']?></td>
                    <td class="valor clavprodserv" align="center"><?=$arrConceptos[$key]['ClaveProdServ']?></td>
                    <td class="valor preciounitario" align="right"><?=$arrConceptos[$key]['ValorUnitario']?></td>
                    <td class="valor importe" align="right"><?=$arrConceptos[$key]['Importe']?></td>
                </tr>
                <?php //endfor;?>
            <?php endforeach;?>

            <tr>
                <td class="label" align="center" style="height: 15px; border-left: none; border-bottom: none" colspan="5"></td>
                <td class="label" align="right" style="border: #DDD 2px solid !important;">Subtotal</td>
                <td class="valor" align="right"><?=$comprobanteSubTotal?></td>
            </tr>

            <tr>
                <td class="label" align="center" style="height: 15px; border-left: none; border-bottom: none" colspan="5"></td>
                <td class="label" align="right" style="border: #DDD 2px solid !important;">Descuento</td>
                <td class="valor" align="right" style=""><?=$comprobanteDescuento?></td>
            </tr>

            <?php if(isset($totalImpuesto)):?>
                <tr>
                    <td class="label" align="center" style="height: 15px; border-left: none; border-bottom: none" colspan="5"></td>
                    <td class="label" align="right" style="border: #DDD 2px solid !important;">Impuesto Trasladado</td>
                    <td class="valor" align="right"><?=$totalImpuesto?></td>
                </tr>
            <?php endif;?>

            <tr>
                <td class="label" align="center" style="height: 15px; border-left: none; border-bottom: none" colspan="5"></td>
                <td class="label" align="right" style="border: #DDD 2px solid !important;">Total</td>
                <td class="valor" align="right"><?=$comprobanteTotal?></td>
            </tr>

        </table>
    </page>