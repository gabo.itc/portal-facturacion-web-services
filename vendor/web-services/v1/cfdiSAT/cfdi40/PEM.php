<?php

namespace WebServices\cfdiSAT\cfdi40;

// set_include_path(get_include_path() .'/libs/phpseclib');
//set_include_path(get_include_path() . PATH_SEPARATOR . 'libs/phpseclib');
use DOMDocument;
use XSLTProcessor;
use WebServices\cfdiSAT\cfdi40\CFDIException;
use Exception;

class PEM{
	public $certificado;
	public $certificadoContenido;
	public $certificadoPEM;
	public $certificadoRuta;
	public $llave;
	public $llaveContenido;
	public $llavePEM;
	public $llaveRuta;
	public $password;

    function __construct($rutaCertificado='',$rutaLlave='',$pass='')
    {
        $this->certificadoRuta = $rutaCertificado;
        $this->llaveRuta = $rutaLlave;
        $this->password = $pass;

    }

    public function getPemCertificado($rutaCertificado,$esArchivo=true){
        $this->certificadoPEM = "No Existe";

        if(($esArchivo && file_exists($rutaCertificado)) || (!$esArchivo && !empty($rutaCertificado))){
            if($esArchivo){
                $this->certificadoRuta = $rutaCertificado;
                $this->certificadoContenido = file_get_contents($rutaCertificado);
            }else{
                $this->certificadoRuta = '';
                $this->certificadoContenido = $rutaCertificado;
            }

            /* Convert .cer to .pem, cURL uses .pem */
            $this->certificadoPEM = '-----BEGIN CERTIFICATE-----'.PHP_EOL;
            $this->certificadoPEM .= chunk_split(base64_encode($this->certificadoContenido), 64, PHP_EOL);
            $this->certificadoPEM .= '-----END CERTIFICATE-----'.PHP_EOL;
            $this->certificado = preg_replace('/(-+[^-]+-+)/', '', $this->certificadoPEM);
            $this->certificado = preg_replace('/\s+/', '', $this->certificado);
        }else{
            CFDIException::$codigo_estatus = 404;
            // $nombre_archivo = basename($rutaCertificado);
            // $ruta = str_replace('/'.$nombre_archivo, '', $rutaCertificado);
            throw new CFDIException("Certificado [Archivo CER $rutaCertificado] No Encontrado");
        }
    }

    public function getPemLlave($rutaLlave,$esArchivo=true)
    {
        $this->llavePEM = "No Existe";
        if(($esArchivo && file_exists($rutaLlave)) || (!$esArchivo && !empty($rutaLlave))){
        // if(file_exists($rutaLlave)){
            if($esArchivo){
                $this->llaveRuta = $rutaLlave;
                $this->llaveContenido = file_get_contents($rutaLlave);
            }else{
                $this->llaveRuta = '';
                $this->llaveContenido = $rutaCertificado;
            }

            /* Convert .key to .pem, cURL uses .pem */
            $this->llavePEM = '-----BEGIN ENCRYPTED PRIVATE KEY-----'.PHP_EOL;
            $this->llavePEM .= chunk_split(base64_encode($this->llaveContenido), 64, PHP_EOL);
            $this->llavePEM .= '-----END ENCRYPTED PRIVATE KEY-----'.PHP_EOL;
            $this->llave = preg_replace('/(-+[^-]+-+)/', '', $this->llavePEM);
            $this->llave = preg_replace('/\s+/', '', $this->llave);
        }else{
            CFDIException::$codigo_estatus = 404;
            throw new CFDIException("Archivo [KEY] No Encontrado");
        }

    }
    public function setPassword($pass='',$esArchivo = false)
    {
        if($esArchivo === true){
            if(file_exists($pass)){
                $pass = file_get_contents($pass);//
            }else{
                CFDIException::$codigo_estatus = 404;
                throw new CFDIException("Contraseña No Configurada [$pass]");
            }
        }

        $this->password = $pass;
    }
    public function NoCertificado()
    {
        $cerX509 = openssl_x509_parse($this->certificadoPEM);

        $serialNumber = $cerX509['serialNumberHex'];
        $NoCertificado = "";
        for ($i=0; $i < strlen($serialNumber); ($i++)) { 
            $NoCertificado .= $serialNumber[$i+1];
            $i++;
        }

        if(empty($NoCertificado)){
            CFDIException::$codigo_estatus = 500;
            throw new CFDIException("No fue posible extraer información [NoCertificado] ");
        }

        return $NoCertificado;
    }

    /**Convierte el Certificado(.cer) y la Llave(.key) en formato PFX, con la misma contraseña
    La funcion trabaja */
    public function getCertificadoPFX($passPFX){
        $certificadoPFX = null;/*variable que retornara el contenido del certificado PFX*/
        //REVISAR!!!
        // $this->getPemCertificado($this->certificadoRuta);
        // $this->getPemLlave($this->llaveRuta);
        if($this->certificadoPEM != "No Existe" && $this->llavePEM != "No Existe"){
            /*generamos certificado PEM x509*/
            // $cerX509 = openssl_x509_read($this->certificadoContenido);
            // $cerX509 = openssl_x509_parse($this->certificadoPEM);
            try{
                $cerX509 = openssl_x509_read ($this->certificadoPEM);
            }catch(Exception $ex){
                CFDIException::$codigo_estatus = 500;
                throw new CFDIException("No fue posible generar el archivo PFX, Problema al Tratar de Convertir el Certificados a Formato x509");
            }
            if($cerX509){
                $pkey = $this->generaPrivateKey();
                try{
                    openssl_pkcs12_export($cerX509, $certificadoPFX, $pkey, $passPFX);
                    // openssl_pkcs12_export($this->certificadoContenido, $certificadoPFX, $pkey, $passPFX);
                    file_put_contents($this->certificadoRuta.".pem", $certificadoPFX);
                }catch(Exception $ex){
                    CFDIException::$codigo_estatus = 500;
                    // throw new CFDIException($ex->getMessage());
                    throw new Exception($ex);
                }
            }
        }
        if(!isset($certificadoPFX)){
            CFDIException::$codigo_estatus = 500;
            throw new CFDIException("No fue posible generar el archivo PFX, Verifique sus Certificados(.cer, .key)");
        }
        return $certificadoPFX;
    }

    private function generaPrivateKey()
    {
        try{
            $pkey = openssl_pkey_get_private($this->llavePEM,$this->password);
        }catch(Excepcion $ex){
            CFDIException::$codigo_estatus = 500;
            throw new CFDIException("No fue posible generar La Llave de Sellado");
        }
        if($pkey){
            return $pkey;
        }else{
            CFDIException::$codigo_estatus = 500;
            throw new CFDIException("No fue posible generar La Llave de Sellado");
            return false;
        }
    }
    
    public function getSello(string $cadenaOriginal) 
    {
        $pkey = $this->generaPrivateKey();
        if($pkey){
            $signature = "";
            if(openssl_sign($cadenaOriginal, $signature, $pkey, OPENSSL_ALGO_SHA256)){
                $sello = base64_encode($signature);
                return $sello;
            }else{
                CFDIException::$codigo_estatus = 500;
                throw new CFDIException("No fue posible generar el Sello");
            }
        }else{
            CFDIException::$codigo_estatus = 500;
            throw new CFDIException("No fue posible generar La Llave de Sellado");
            return false;
        }
    }

    /**
     * SAT XSL endpoint.
     *
     * @var string
     */
    const XSL_ENDPOINT = 'http://www.sat.gob.mx/sitio_internet/cfd/4/cadenaoriginal_4_0/cadenaoriginal_4_0.xslt';
    // const XSL_ENDPOINT = 'http://www.sat.gob.mx/sitio_internet/cfd/timbrefiscaldigital/cadenaoriginal_TFD_1_1.xslt';

    /**
     * Gets the original string.
     *
     * @return string
     */
    function getCadenaOriginal(string $cadenaXML)
    {

    // CFDIException::$codigo_estatus = 504;
    // throw new CFDIException("Ruta Actual [".dirname(__FILE__)."]");
    // throw new CFDIException("Ruta Actual [".__DIR__."]");

    	$cadena_original_sat = false;
        $xlst = "";
        if(defined(RUTA_CADENA_ORIGINAL_XSLT_4_0)){
            $xlst = RUTA_CADENA_ORIGINAL_XSLT_4_0;
        }

        $xlstCadenaOriginal = null;
        $cargadoXLSTCadenaOriginal = false;

        if(!file_exists($xlst)){
            $linkCadenaOriginal = static::XSL_ENDPOINT;

            /*Sustiuimos la URL por la ruta local*/
            $xlst = __DIR__ . '/'.str_replace('http://www.sat.gob.mx/sitio_internet/cfd/4/cadenaoriginal_4_0', 'utilerias', $linkCadenaOriginal);
            
            $rutaXLST = __DIR__."/";
            if (!file_exists($rutaXLST)) {/*si el directorio no existe*/
                mkdir($rutaXLST, 0777, true);/*creamos el directorio*/
            }
                // throw new CFDIException("No Fue Posible Descargar el Archivo [".$rutaXLST."]");


            $xlstCadenaOriginal = new DOMDocument();
            $cargadoXLSTCadenaOriginal = $xlstCadenaOriginal->load($linkCadenaOriginal);/*cargamos el archivo*/

            /*cambiamos la version por compatibiliadad*/
            // $stylesheet = $xlstCadenaOriginal->getElementsByTagName('stylesheet');
            // $stylesheet->item(0)->setAttribute('version','1.0');

            if($cargadoXLSTCadenaOriginal){
                $xlstCadenaOriginal->save($xlst);
            }else{
                $xlst = $xlstCadenaOriginal;
            }
        }else{
            $xlstCadenaOriginal = new DOMDocument();
            $cargadoXLSTCadenaOriginal = $xlstCadenaOriginal->load($xlst);
        }

        if($cargadoXLSTCadenaOriginal){

            /*cambiamos la version de la plantilla por compatibiliadad*/
            $stylesheet = $xlstCadenaOriginal->getElementsByTagName('stylesheet');
            $stylesheet->item(0)->setAttribute('version','1.0');

            /*obtenemos los elementos que se deben de importar*/
            $elementosImportados = $xlstCadenaOriginal->getElementsByTagName('include');
            // $numeroDescargas = $elementosImportados->length;
            /*Recorremos la lista de importaciones*/
            foreach ($elementosImportados as $nodo) {
                $url = $nodo->getAttribute('href');/*obtenemos la url*/
                $nombreArchivo = basename($url);/*obtenemos el nombre del archivo*/
                $ruta =  __DIR__ . '/' . str_replace('http://www.sat.gob.mx/sitio_internet', 'utilerias', $url);/*Sustiuimos la URL por la ruta local*/
                //__DIR__ . '/'.
                // $directorio = __DIR__ . '/' . str_replace($nombreArchivo, '', $ruta);
                /*Obtenemos el directorio*/
                $directorio =  str_replace($nombreArchivo, '', $ruta);
                if (!file_exists($directorio)) {/*si el directorio no existe*/
                    mkdir($directorio, 0777, true);/*creamos el directorio*/
                }


                $archivoXLS = $url;
                if (file_exists($ruta)) {/*si el archivo existe*/
                    $archivoXLS = $ruta;/*obtenemos el archivo*/
                }

                $xslExtra = new DOMDocument();

                // $source = file_get_contents($url);/*Descargando el archivo*/
                // file_put_contents($ruta, $source);/*copiando el archivo*/

                $cargadoXLST = $xslExtra->load($archivoXLS);/*cargamos el archivo*/
                if($cargadoXLST){
                    /*cambiamos la version de la plantilla*/
                    $stylesheet = $xslExtra->getElementsByTagName('stylesheet');
                    $stylesheet->item(0)->setAttribute('version','1.0');
                    /*en caso de descargarse lo guardamos local y caso contrario se sobreescribe*/
                    $xslExtra->save($ruta);
                    // echo "<br>archivo descargado ".($numeroDescargas--).": ".$ruta;
                    /*modificamos el atributo por la ruta local*/
                    // $nodo->setAttribute( 'href' , str_replace('utilerias/', '', $ruta));
                    $nodo->setAttribute( 'href' , $ruta);
                }else{
                    CFDIException::$codigo_estatus = 504;
                    throw new CFDIException("No Fue Posible Descargar el Archivo [$archivoXLS]");
                }

            }

            $xlstCadenaOriginal->save($xlst."x");

            $xml = new DOMDocument();
            $xml->loadXML($cadenaXML);

    		// Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT
            $xsltP = new XSLTProcessor;
    		// Cargar las reglas de transformación desde el archivo XSLT.
            $xsltP->importStylesheet($xlstCadenaOriginal);
    		// $xsltP->importStylesheet($this->cadena());

            $cadena_original_sat = $xsltP->transformToXML($xml);
        }else{
            CFDIException::$codigo_estatus = 504;
            throw new CFDIException("No Fue Posible Descargar el Archivo [$xlst]");
        }

        return $cadena_original_sat;
    }

    /**
     * genera la cadena original basado en el nodo timbre fiscas digital
     *
     * @return string
     */
    public static function getCadenaOriginalTFD(string $stringXML, $tfdPrefix = '')
    {

        $cadena_original_tfd = false;

        $xlst = RUTA_CADENA_ORIGINAL_TFD_XSLT;

        $xsl = new DOMDocument();
        $cargadoXLST = $xsl->load($xlst);
        if($cargadoXLST){

            $xml = new DOMDocument();
            /*cargamos la cadena del XML*/
            $cargaXML = $xml->loadXML($stringXML);
            if($cargaXML){

                /*obtenemos solo el nodo TimbreFiscalDigital*/
                $TimbreFiscalDigital = $xml->getElementsByTagNameNS($tfdPrefix, 'TimbreFiscalDigital')->item(0);
                if(!is_null($TimbreFiscalDigital)){
                    /*eliminamos el atributo xsi del nodo*/
                    $TimbreFiscalDigital->removeAttribute("xsi:schemaLocation");
                    /*obtenemos el string*/
                    $cadenaTFD = $xml->saveXML($TimbreFiscalDigital);
                    /*recargamos unicamente el nodo TimbreFiscalDigital*/
                    $cargaXML = $xml->loadXML($cadenaTFD);

                    if($cargaXML){
                        /*cambiamos la version de la plantilla*/
                        $stylesheet = $xsl->getElementsByTagName('stylesheet');
                        if ($stylesheet->length > 0) {
                            $stylesheet->item(0)->setAttribute('version','1.0');
                        }

                        $cadena_original_tfd = "";

                        /*Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT*/
                        $xsltP = new XSLTProcessor;
                        /*Cargar las reglas de transformación desde el archivo XSLT.*/
                        $xsltP->importStylesheet($xsl);
                        $cadena_original_tfd = $xsltP->transformToXML($xml);
                    }
                }
            }
        }else{
            CFDIException::$codigo_estatus = 504;
            throw new CFDIException("No Fue Posible Descargar el Archivo [$xlst]");
        }

        return $cadena_original_tfd;
    }

}

?>