<?php

namespace WebServices\cfdiSAT;
/**
 * 
 */
 class Configuracion
{

    function __construct()
    {
        define('API_KEY',MD5(uniqid()));


        define('RUTA_LOCAL_XML', getcwd()."/xml/");
        define('RUTA_UTILERIAS', getcwd()."/utilerias/");
        define('RUTA_ESQUEMAS', getcwd()."/utilerias/");
        // define('RUTA_CERTIFICADO', getcwd()."/../utilerias/certificados");
        define('RUTA_CADENA_ORIGINAL_XSLT', RUTA_UTILERIAS."cadenaoriginal_3_3.xslt");
        define('RUTA_CADENA_ORIGINAL_TFD_XSLT', RUTA_UTILERIAS."cadenaoriginal_TFD_1_1.xslt");

        $host = $_SERVER["HTTP_HOST"];
        // $linkPrueba = strpos($host, 'demo')===0;
        $linkPrueba = true;

        define('MODO_PRUEBA', $linkPrueba);
        define('RUTA_LOCAL', getcwd());
        define('DB_HOST', 'tudescargamasiva.com');
        define('DB_PORT', '3306');
        define('DB_NAME_REAL', 'tudescar_ws_facturacion');
        define('DB_NAME', 'tudescar_ws_facturacion'.($linkPrueba===true ? "_prueba":"")); 
        define('DB_USER', 'tudescar_portal');
        define('DB_PASS', '2021_portal');

        /************************************************************************************************/
        /**DATOS DE PRUEBA*******************************************************************************/
        /************************************************************************************************/
        define('urlSIFEIPrueba',"http://devcfdi.sifei.com.mx:8080/SIFEI33/SIFEI?wsdl");
        define('urlSIFEICancelacionesPrueba',"http://devcfdi.sifei.com.mx:8888/CancelacionSIFEI/Cancelacion?wsdl");
        define('usuarioSIFEIPrueba',"SSO071219GHA");
        define('passwordSIFEIPrueba',"0809_Benito");
        define('IdEquipoSIFEIPrueba',"MmE5OTQ3ODMtNjc2Mi1kNWU4LTAzMDctNWE0MjZhOGJmMTNh");

        /************************************************************************************************/
        /**DATOS_DE_REALES*******************************************************************************/
        /************************************************************************************************/
        define('urlSIFEIReal',"https://sat.sifei.com.mx:8443/SIFEI/SIFEI?wsdl");
        define('urlSIFEICancelacionesReal',"https://sat.sifei.com.mx:9000/CancelacionSIFEI/Cancelacion?wsdl");
        define('usuarioSIFEIReal',"SSO071219GHA");
        define('passwordSIFEIReal',"c808869b");
        define('IdEquipoSIFEIReal',"Yzk1YTM0NTAtYjQ3Ny0yM2MxLWY4NzQtNWM1YzdkMzI1OGJk");


        define('urlSIFEI'              ,$linkPrueba ? urlSIFEIPrueba : urlSIFEIReal );
        define('urlSIFEICancelaciones' ,$linkPrueba ? urlSIFEICancelacionesPrueba : urlSIFEICancelacionesReal );
        define('usuarioSIFEI'          ,$linkPrueba ? usuarioSIFEIPrueba : usuarioSIFEIReal );
        define('passwordSIFEI'         ,$linkPrueba ? passwordSIFEIPrueba : passwordSIFEIReal );
        define('IdEquipoSIFEI'         ,$linkPrueba ? IdEquipoSIFEIPrueba : IdEquipoSIFEIReal);
    }
}
?>