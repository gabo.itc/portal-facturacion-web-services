<?php
namespace WebServices\cfdiSAT\cfdi33;

use WebServices\cfdiSAT\cfdi33\CFDIException;
use DateTime;
class Comprobante{
	public $Version = "3.3"; //required
	public $Serie; //optional
	public $Folio; //optional
	public $Fecha; //required
	public $Sello; //required
	public $FormaPago; //optional
	public $NoCertificado; //required
	public $Certificado; //required
	public $CondicionesDePago; //optional
	public $SubTotal; //required
	public $Descuento; //optional
	public $Moneda = "MXN"; //required
	public $TipoCambio; //optional
	public $Total; //required
	public $TipoDeComprobante; //required
	public $MetodoPago; //optional
	public $LugarExpedicion; //required
	public $Confirmacion; //optional
	function __construct (){

	}

	public function setFecha($Fecha)
	{
		if(is_null($Fecha) || empty($Fecha)){
			$this->Fecha = (new DateTime())->format('Y-m-d\TH:i:s');
		}else{
			$this->Fecha = $Fecha;
		}
	}

	public function procesaDescuentos($arrDescuentos = array())
	{
		$Descuento = 0;
		foreach ($arrDescuentos as $key => $value) {
			$Descuento += $value;
		}
		if(!isset($this->Descuento)){
			$this->Descuento = $Descuento;
		}
	}
}

class CfdiRelacionados{
	
	public $TipoRelacion;
	public $arrCfdiRelacionado = array();

	function agregarCfdiRelacionado(CfdiRelacionado $CfdiRelacionado = null)
	{
		if($CfdiRelacionado instanceof CfdiRelacionado){
			array_push($this->arrCfdiRelacionado, $CfdiRelacionado);/*agregamos un elemendo al array*/
		}
	}

	function sizeCfdiRelacionado(){
		return sizeof($this->arrCfdiRelacionado);
	}
}

class CfdiRelacionado{
	public $UUID;
	
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class Emisor{
	public $Rfc;
	public $Nombre;
	public $RegimenFiscal;
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class Receptor{
	public $Rfc;
	public $Nombre;
	public $UsoCFDI;
	public $ResidenciaFiscal;
	public $NumRegIdTrib;
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class Conceptos{
	public $arrConceptos = array();
	public $arrConceptoImpuestos = array();/*guarda los impuestos agregados por cada concepto*/
	public $arrConceptoDescuento = array();/*guarda los descuentos agregados por cada concepto*/
	function __construct ( ){
		/*de momento no se ocupa*/
	}
	function agregarConcepto(Concepto $Concepto = null){
		if($Concepto instanceof Concepto){
			array_push($this->arrConceptos, $Concepto);/*agregamos un elemendo al array*/
			// $this->arrConceptos[$this->sizeConceptos()] = $Concepto;
			// $this->arrConceptoImpuestos[$this->sizeImpuestos()] = $Concepto->arrConceptoImpuestos;
			$this->arrConceptoImpuestos = array_merge($this->arrConceptoImpuestos, $Concepto->arrConceptoImpuestos);
			if(isset($Concepto->Descuento)){
				$this->arrConceptoDescuento = array_merge($this->arrConceptoDescuento, $Concepto->Descuento);
			}
		}
	}
	function sizeConceptos(){
		return sizeof($this->arrConceptos);
	}
}

class Concepto{
	public $ClaveProdServ;
	public $NoIdentificacion;
	public $Cantidad;
	public $ClaveUnidad;
	public $Unidad;
	public $Descripcion;
	public $ValorUnitario;
	public $Importe;
	public $Descuento;
	public $arrConceptoImpuestos = array();
	function __construct ( ){
		/*de momento no se ocupa*/
	}
	function agregarImpuestos($ConceptoImpuestos = null){
		if($ConceptoImpuestos instanceof ConceptoImpuestos){
			array_push($this->arrConceptoImpuestos, $ConceptoImpuestos);/*agregamos un elemendo al array*/
			// $this->arrConceptoImpuestos[$this->sizeConceptoImpuestos()] = $ConceptoImpuestos;
		}
	}

	function sizeConceptoImpuestos(){
		return sizeof($this->arrConceptoImpuestos);
	}
}

class ConceptoImpuestos{
	public $arrRetenciones = array();
	public $arrTraslados = array();
	function __construct ( ){
		/*de momento no se ocupa*/
	}
	function agregarRetenciones($Retenciones = null){
		if($Retenciones instanceof ConceptoImpuestosRetencionesRetencion){
			array_push($this->arrRetenciones, $Retenciones);/*agregamos un elemendo al array*/
			// $this->arrRetenciones[$this->sizeRetenciones()] = $Retenciones;
		}
	}

	function agregarTraslados($Traslado = null){
		if($Traslado instanceof ConceptoImpuestosTrasladosTraslado){
			array_push($this->arrTraslados, $Traslado);/*agregamos un elemendo al array*/
			// $this->arrTraslados[$this->sizeTraslados()] = $Traslado;
		}
	}

	function sizeRetenciones(){
		return sizeof($this->arrRetenciones);
	}
	function sizeTraslados(){
		return sizeof($this->arrTraslados);
	}
}

/*No se Ocupa*/
class ConceptoImpuestosRetenciones{
	public $arrRetenciones = array();
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class ConceptoImpuestosRetencionesRetencion{
	public $Base;
	public $Impuesto;
	public $TipoFactor;
	public $TasaOCuota;
	public $Importe;
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}
/*No se Ocupa*/
class ConceptoImpuestosTraslados{
	public $arrTraslados = array();
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class ConceptoImpuestosTrasladosTraslado{
	public $Base;
	public $Impuesto;
	public $TipoFactor;
	public $TasaOCuota;
	public $Importe;
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class Impuestos{

	public $arrTraslados = array();
	public $arrRetenidos = array();
	public $TotalImpuestosRetenidos; //optional
	public $TotalImpuestosTrasladados; //optional
	function __construct ( ){
		/*de momento no se ocupa*/
	}
	function procesaConceptoImpuestos($impuestos = array()){
		if(is_array($impuestos)){
			//var_dump($impuestos);
			foreach ($impuestos as $ConceptoImpuestos => $valClass) {
				if($valClass instanceof ConceptoImpuestos){
					foreach ($valClass->arrRetenciones as $uno => $ConceptoImpuestosRetencionesRetencion ) {
						if($ConceptoImpuestosRetencionesRetencion instanceof ConceptoImpuestosRetencionesRetencion){
							$impuestosRetencionesRetencion = new ImpuestosRetencionesRetencion();
							$impuestosRetencionesRetencion->Impuesto = $ConceptoImpuestosRetencionesRetencion->Impuesto;
							$impuestosRetencionesRetencion->TipoFactor = $ConceptoImpuestosRetencionesRetencion->TipoFactor;
							$impuestosRetencionesRetencion->TasaOCuota = $ConceptoImpuestosRetencionesRetencion->TasaOCuota;
							$impuestosRetencionesRetencion->Importe = $ConceptoImpuestosRetencionesRetencion->Importe;
							if(strtoupper($impuestosRetencionesRetencion->TipoFactor) !=="EXENTO"){
								$this->agregarRetenidos($impuestosRetencionesRetencion);
							}
						}
					}

					foreach ($valClass->arrTraslados as $uno => $ConceptoImpuestosTrasladosTraslado ) {
						if($ConceptoImpuestosTrasladosTraslado instanceof ConceptoImpuestosTrasladosTraslado){
							$impuestosTrasladosTraslado = new ImpuestosTrasladosTraslado();
							$impuestosTrasladosTraslado->Impuesto = $ConceptoImpuestosTrasladosTraslado->Impuesto;
							$impuestosTrasladosTraslado->TipoFactor = $ConceptoImpuestosTrasladosTraslado->TipoFactor;
							$impuestosTrasladosTraslado->TasaOCuota = $ConceptoImpuestosTrasladosTraslado->TasaOCuota;
							$impuestosTrasladosTraslado->Importe = $ConceptoImpuestosTrasladosTraslado->Importe;
							if(strtoupper($impuestosTrasladosTraslado->TipoFactor) !=="EXENTO"){
								$this->agregarTraslados($impuestosTrasladosTraslado);
							}
						}
					}
				}
			}
		}

	}
	function agregarRetenidos($ImpuestosRetencionesRetencion = null){
		if($ImpuestosRetencionesRetencion instanceof ImpuestosRetencionesRetencion){
			$encontrado = false;
			foreach ($this->arrRetenidos as $posicion => $valClass) {
				if($valClass->Impuesto==$ImpuestosRetencionesRetencion->Impuesto
					&& $valClass->TipoFactor==$ImpuestosRetencionesRetencion->TipoFactor
					&& $valClass->TasaOCuota==$ImpuestosRetencionesRetencion->TasaOCuota
				){
					$encontrado = true;
					$valClass->Importe += $ImpuestosRetencionesRetencion->Importe;
					$this->TotalImpuestosRetenidos += $ImpuestosRetencionesRetencion->Importe;
				}
			}
			if(!$encontrado){
				array_push($this->arrRetenidos, $ImpuestosRetencionesRetencion);
				$this->TotalImpuestosRetenidos += $ImpuestosRetencionesRetencion->Importe;
			}
		}
	}
	function agregarTraslados($ImpuestosTrasladosTraslado = null){
		if($ImpuestosTrasladosTraslado instanceof ImpuestosTrasladosTraslado){
			$encontrado = false;
			foreach ($this->arrTraslados as $posicion => $valClass) {
				if($valClass->Impuesto==$ImpuestosTrasladosTraslado->Impuesto
					&& $valClass->TipoFactor==$ImpuestosTrasladosTraslado->TipoFactor
					&& $valClass->TasaOCuota==$ImpuestosTrasladosTraslado->TasaOCuota
				){
					$encontrado = true;
					$valClass->Importe += $ImpuestosTrasladosTraslado->Importe;
					$this->TotalImpuestosTrasladados += $ImpuestosTrasladosTraslado->Importe;
				}
			}
			if(!$encontrado){
				array_push($this->arrTraslados, $ImpuestosTrasladosTraslado);
				$this->TotalImpuestosTrasladados += $ImpuestosTrasladosTraslado->Importe;
			}
		}
	}
}

class ImpuestosRetencionesRetencion{
	public $Impuesto; //required
	public $Importe; //required
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

class ImpuestosTrasladosTraslado{
	public $Impuesto; //required
	public $TipoFactor; //required
	public $TasaOCuota; //required
	public $Importe; //required
	function __construct ( ){
		/*de momento no se ocupa*/
	}
}

?>