<?php
namespace WebServices\cfdiSAT\cfdi33;
/**
 * Html2Pdf Library - example
 *
 * HTML => PDF converter
 * distributed under the OSL-3.0 License
 *
 * @package   Html2pdf
 * @author    Laurent MINGUET <webmaster@html2pdf.fr>
 * @copyright 2017 Laurent MINGUET
 */
require_once 'vendor/autoload.php';

// require_once 'variablesCFDI.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use ZipArchive;
use DOMDocument;
// use PEM;
use WebServices\cfdiSAT\cfdi33\PEM;
use PDO;
use PDOException;
use WebServices\cfdiSAT\ConexionDB;
use WebServices\cfdiSAT\DBException;
use WebServices\cfdiSAT\FacturaException;
// use cfdi33\PEM;

/**
 * 
 */
class GeneraPDF
{
    public static $conexion;
    function __construct()
    {

    }

    public static function generar($nombreArchivo = "Factura", string $xmlTimbrado = '', bool $retornaExcepcion = false )
    {
        $archivoPDF = "";
        try {

            $xmlDOM = new DOMDocument();
            $cargaXML = $xmlDOM->loadXML($xmlTimbrado);
            if($cargaXML){

                setlocale(LC_MONETARY,"es_MX");/*para el formato moneda*/

                $cfdiPrefix = 'http://www.sat.gob.mx/cfd/3';
                $tfdPrefix = 'http://www.sat.gob.mx/TimbreFiscalDigital';
                $cadenaOriginalTFD = PEM::getCadenaOriginalTFD($xmlDOM->saveXML(),$tfdPrefix);

                $Comprobante = $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Comprobante')->item(0);
                if(!is_null($Comprobante)){
                    $comprobanteTipoDeComprobante = $Comprobante->getAttribute('TipoDeComprobante');
                    $comprobanteSerie = $Comprobante->getAttribute('Serie');
                    $comprobanteFolio = $Comprobante->getAttribute('Folio');
                    $facturaSerieFolio = (empty($comprobanteSerie) ? "" : $comprobanteSerie ." - ") . $comprobanteFolio;
                    $comprobanteFecha = $Comprobante->getAttribute('Fecha');
                    $comprobanteNoCertificado = $Comprobante->getAttribute('NoCertificado');
                    $comprobanteLugarExpedicion = $Comprobante->getAttribute('LugarExpedicion');
                    $comprobanteFormaPago = $Comprobante->getAttribute('FormaPago');
                    $comprobanteCertificado = $Comprobante->getAttribute('Certificado');
                    $comprobanteCondicionesDePago = $Comprobante->getAttribute('CondicionesDePago');
                    $comprobanteSubTotal = $Comprobante->getAttribute('SubTotal');
                    $comprobanteDescuento = $Comprobante->getAttribute('Descuento');
                    $comprobanteMoneda = $Comprobante->getAttribute('Moneda');
                    $comprobanteTipoCambio = $Comprobante->getAttribute('TipoCambio');
                    $comprobanteTotal = $Comprobante->getAttribute('Total');
                    $comprobanteMetodoPago = $Comprobante->getAttribute('MetodoPago');
                    $comprobanteConfirmacion = $Comprobante->getAttribute('Confirmacion');

                    $comprobanteSubTotal = empty($comprobanteSubTotal) ? "$ 0.00" : money_format("%.2n", $comprobanteSubTotal);
                    $comprobanteDescuento = empty($comprobanteDescuento) ? "$ 0.00" :  money_format("%.2n", $comprobanteDescuento);
                    $comprobanteTotal = empty($comprobanteTotal) ? "$ 0.00" : money_format("%.2n", $comprobanteTotal);

                    $descripcionMoneda = self::getDescripcionMoneda($comprobanteMoneda);
                    $comprobanteMoneda .= (empty($descripcionMoneda) ? "": " - ".$descripcionMoneda);

                    $descripcionTipoDeComprobante = self::getDescripcionTipoDeComprobante($comprobanteTipoDeComprobante);
                    $comprobanteTipoDeComprobante .= (empty($descripcionTipoDeComprobante) ? "": " - ".$descripcionTipoDeComprobante);

                    $descripcionFormaPago = self::getDescripcionFormaPago($comprobanteFormaPago);
                    $comprobanteFormaPago .= (empty($descripcionFormaPago) ? "": " - ".$descripcionFormaPago);

                    $descripcionMetodoPago = self::getDescripcionMetodoPago($comprobanteMetodoPago);
                    $comprobanteMetodoPago .= (empty($descripcionMetodoPago) ? "": " - ".$descripcionMetodoPago);
                }


                $CfdiRelacionados = $Comprobante->getElementsByTagNameNS($cfdiPrefix, 'CfdiRelacionados')->item(0);
                if(!is_null($CfdiRelacionados)){
                    $CfdiRelacionado = $CfdiRelacionados->getElementsByTagNameNS($cfdiPrefix, 'CfdiRelacionado');

                    $arrConceptos = array();
                    foreach ($CfdiRelacionado as $key => $value) :
                        $conceptoCfdiRelacionados .= (isset($conceptoCfdiRelacionados) ? ", ":"").$CfdiRelacionado->item($key)->getAttribute('UUID');
                    endforeach;
                }


                $Emisor = $Comprobante->getElementsByTagNameNS($cfdiPrefix, 'Emisor')->item(0);
                if(!is_null($Emisor)){
                    $emisorRfc = $Emisor->getAttribute('Rfc');
                    $emisorNombre = $Emisor->getAttribute('Nombre');
                    $regimenFiscal = $Emisor->getAttribute('RegimenFiscal');

                    $descripcionRegimenFiscal = self::getDescripcionRegimenFiscal($regimenFiscal);
                    $regimenFiscal .= (empty($descripcionRegimenFiscal) ? "": " - ".$descripcionRegimenFiscal);

                    $logo_numero = isset($logo_numero) ? $logo_numero : "01";
                    // $url_logo = "http://facturacion.sicanetsc.com/utilerias/certificados/{$emisorRfc}/logo{$formato}.png";
                    $url_logo = $_SERVER['DOCUMENT_ROOT']."/utilerias/certificados/{$emisorRfc}/logo{$logo_numero}.png";
                    if(!file_exists($url_logo)){
                        $url_logo = null;
                    }
                }


                $Receptor = $Comprobante->getElementsByTagNameNS($cfdiPrefix, 'Receptor')->item(0);
                if(!is_null($Receptor)){
                    $receptorRfc = $Receptor->getAttribute('Rfc');
                    $receptorNombre = $Receptor->getAttribute('Nombre');
                    $receptorUsoCFDI = $Receptor->getAttribute('UsoCFDI');
                    $descripcionUsoCFDI = self::getDescripcionUsoCFDI($receptorUsoCFDI);
                    $receptorUsoCFDI .= (empty($descripcionUsoCFDI) ? "": " - ".$descripcionUsoCFDI);
                }

                $Conceptos = $Comprobante->getElementsByTagNameNS($cfdiPrefix, 'Conceptos')->item(0);
                if(!is_null($Conceptos)){
                    $Concepto = $Conceptos->getElementsByTagNameNS($cfdiPrefix, 'Concepto');

                    $arrConceptos = array();
                    foreach ($Concepto as $key => $value) :
                        $conceptoProducto = $Concepto->item($key)->getAttribute('Descripcion');
                        $conceptoProducto = str_replace("<", "&lt;", $conceptoProducto);
                        $conceptoProducto = str_replace(">", "&gt;", $conceptoProducto);
                        $conceptoDescuento = $Concepto->item($key)->getAttribute('Descuento');
                        // $conceptoDescuento = empty($conceptoDescuento) ? 0 : $conceptoDescuento;
                        $conceptoDescuento = empty($conceptoDescuento) ? "" : "<br>Descuento: {$conceptoDescuento}";
                        $conceptoProducto .= $conceptoDescuento;

                        $NoIdentificacion = $Concepto->item($key)->getAttribute('NoIdentificacion');
                        $NoIdentificacion = empty($NoIdentificacion) ? "" : "<br><b>NoIdentificacion:</b> ".chunk_split($NoIdentificacion,55,' ')."";
                        $conceptoProducto .= $NoIdentificacion;

                        $arrConceptos[$key]['Cantidad']  = $Concepto->item($key)->getAttribute('Cantidad');
                        $arrConceptos[$key]['Descripcion']  = $conceptoProducto;
                        $arrConceptos[$key]['Unidad']  = $Concepto->item($key)->getAttribute('Unidad');
                        $arrConceptos[$key]['ClaveUnidad']  = $Concepto->item($key)->getAttribute('ClaveUnidad');
                        $arrConceptos[$key]['ClaveProdServ']  = $Concepto->item($key)->getAttribute('ClaveProdServ');
                        $arrConceptos[$key]['ValorUnitario']  = money_format("%.2n", $Concepto->item($key)->getAttribute('ValorUnitario'));
                        // $arrConceptos[$key]['Descuento']  = money_format("%.2n", $conceptoDescuento);
                        $arrConceptos[$key]['Importe']  = money_format("%.2n", $Concepto->item($key)->getAttribute('Importe'));
                        // ImpuestoClave
                        // ImpuestoImporte
                    endforeach;
                }

                $TimbreFiscalDigital = $Comprobante->getElementsByTagNameNS($tfdPrefix, 'TimbreFiscalDigital')->item(0);
                if(!is_null($TimbreFiscalDigital)){
                    $timbreFiscalDigitalNoCertificadoSAT = $TimbreFiscalDigital->getAttribute('NoCertificadoSAT');
                    $timbreFiscalDigitalUUID = $TimbreFiscalDigital->getAttribute('UUID');
                    $timbreFiscalDigitalFechaTimbrado = $TimbreFiscalDigital->getAttribute('FechaTimbrado');
                    $selloSAT = $TimbreFiscalDigital->getAttribute('SelloSAT');
                    $selloDigital = $TimbreFiscalDigital->getAttribute('SelloCFD');
                    $versionTimbre = $TimbreFiscalDigital->getAttribute('Version');

                    $totalCadena = money_format("%.6n", $Comprobante->getAttribute('Total'));
                    $arrRemplazos = array( "$" , "," , " " );
                    $totalCadena = str_replace($arrRemplazos,'',$totalCadena);/*eliminamos el signo de peso, comas y espacios*/
                    // $totalCadena = str_replace('/$|,| /','',$totalCadena);/*eliminamos el signo de peso, comas y espacios*/
                    $fe = substr($selloDigital,-8);/*obtenemos los ultimos 8 caracteres*/

                    $urlQR = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?";
                    $urlQR .= "&id=$timbreFiscalDigitalUUID";
                    $urlQR .= "&re=$emisorRfc";
                    $urlQR .= "&rr=$receptorRfc";
                    $urlQR .= "&tt=$totalCadena";
                    $urlQR .= "&fe=$fe";
                    // $urlQR = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=B0B866D4-D9A9-11EA-B5D2-9D9FCF87430D&re=VGH0907237N5&rr=XAXX010101000&tt=0000003507.770000&fe=3vaLKQ==";
                }

                $nodoImpuestos = $Comprobante->getElementsByTagNameNS($cfdiPrefix, 'Impuestos');
                $Impuestos = $nodoImpuestos->item(($nodoImpuestos->length-1)); /*obtenemos el ultimo elemento Impuesto*/

                if(!is_null($Impuestos)){
                    $totalImpuesto = $Impuestos->getAttribute('TotalImpuestosTrasladados');
                    $totalImpuesto = money_format("%.2n", empty($totalImpuesto) ? 0 : $totalImpuesto);
                }

                ob_start();

                include dirname(__FILE__).'/plantillasPDF/plantilla01.php';

                $content = ob_get_clean();

                // $html2pdf = new Html2Pdf('P', 'A4', 'es');
                $marginLeft = 15;
                $marginTop = 5;
                $marginRight = 10;
                $marginBottom = 5;
                $margins = array($marginLeft ,$marginTop ,$marginRight ,$marginBottom);

                $html2pdf = new Html2Pdf('P','LETTER','es',true,'UTF-8', $margins);
                $html2pdf->setDefaultFont('Arial');
                $html2pdf->writeHTML($content);
                // $html2pdf->output(dirname(__FILE__).'\plantilla01.pdf','F');
                $archivoPDF = dirname(__FILE__)."/$nombreArchivo.pdf";
                // $html2pdf->output($archivoPDF);
                $html2pdf->output($archivoPDF,'F');
            }
        } catch (Html2PdfException $e) {
            $html2pdf->clean();

            $formatter = new ExceptionFormatter($e);
            if($retornaExcepcion===TRUE){
                FacturaException::$codigo_estatus = 400;
                throw new FacturaException("Imposible Generar PDF");
            }
            // echo $formatter->getHtmlMessage();
        }
        return $archivoPDF;
    }


    public static function getDescripcionTipoDeComprobante($tipoDeComprobante){
        $descripcion = "";
        try{
            if(is_null(self::$conexion)){
                self::$conexion = new ConexionDB();
            }

            $sql = "SELECT descripcion FROM ".DB_NAME_REAL.".".cat_cfdi_tipo_comprobante." WHERE clave = :clave AND estatus = 'A'";
            $stmt = self::$conexion->queryPrepare($sql);

            $stmt->bindParam(':clave',$tipoDeComprobante);
            $stmt->execute();

            $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(sizeof($datos)>0){
                $descripcion = $datos[0]['descripcion'];
            }
        }catch(DBException $ex){

        }

        return $descripcion; 
    }

    public static function getDescripcionMoneda($moneda){
        $descripcion = "";
        try{
            if(is_null(self::$conexion)){
                self::$conexion = new ConexionDB();
            }

            $sql = "SELECT descripcion FROM ".DB_NAME_REAL.".".cat_moneda." WHERE clave = :clave AND estatus = 'A'";
            $stmt = self::$conexion->queryPrepare($sql);

            $stmt->bindParam(':clave',$moneda);
            $stmt->execute();

            $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(sizeof($datos)>0){
                $descripcion = $datos[0]['descripcion'];
            }
        }catch(DBException $ex){

        }

        return $descripcion; 
    }

    public static function getDescripcionUsoCFDI($usoCFDI){
        $descripcion = "";
        try{
            if(is_null(self::$conexion)){
                self::$conexion = new ConexionDB();
            }

            $sql = "SELECT descripcion FROM ".DB_NAME_REAL.".".cat_cfdi_uso_cfdi." WHERE clave = :clave AND estatus = 'A'";
            $stmt = self::$conexion->queryPrepare($sql);

            $stmt->bindParam(':clave',$usoCFDI);
            $stmt->execute();

            $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(sizeof($datos)>0){
                $descripcion = $datos[0]['descripcion'];
            }
        }catch(DBException $ex){

        }

        return $descripcion; 
    }

    public static function getDescripcionRegimenFiscal($regimenFiscal){
        $descripcion = "";
        try{
            if(is_null(self::$conexion)){
                self::$conexion = new ConexionDB();
            }

            $sql = "SELECT descripcion FROM ".DB_NAME_REAL.".".cat_cfdi_regimen_fiscal." WHERE clave = :clave AND estatus = 'A'";
            $stmt = self::$conexion->queryPrepare($sql);

            $stmt->bindParam(':clave',$regimenFiscal);
            $stmt->execute();

            $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(sizeof($datos)>0){
                $descripcion = $datos[0]['descripcion'];
            }
        }catch(DBException $ex){

        }

        return $descripcion; 
    }

    public static function getDescripcionMetodoPago($metodoPago){
        $descripcion = "";
        try{
            if(is_null(self::$conexion)){
                self::$conexion = new ConexionDB();
            }

            $sql = "SELECT descripcion FROM ".DB_NAME_REAL.".".cat_cfdi_metodo_pago." WHERE clave = :clave AND estatus = 'A'";
            $stmt = self::$conexion->queryPrepare($sql);

            $stmt->bindParam(':clave',$metodoPago);
            $stmt->execute();

            $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(sizeof($datos)>0){
                $descripcion = $datos[0]['descripcion'];
            }
        }catch(DBException $ex){

        }

        return $descripcion; 
    }

    public static function getDescripcionFormaPago($formaPago){
        $descripcion = "";
        try{
            if(is_null(self::$conexion)){
                self::$conexion = new ConexionDB();
            }

            $sql = "SELECT descripcion FROM ".DB_NAME_REAL.".".cat_cfdi_forma_pago." WHERE clave = :clave AND estatus = 'A'";
            $stmt = self::$conexion->queryPrepare($sql);

            $stmt->bindParam(':clave',$formaPago);
            $stmt->execute();

            $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(sizeof($datos)>0){
                $descripcion = $datos[0]['descripcion'];
            }
        }catch(DBException $ex){

        }

        return $descripcion; 
    }

    /*esta funcion aun no se ocupa, pero la intencion es procesar todo el XML y mandar las variables a la plantilla*/
    public static function prosesarXML(string $xmlTimbrado)
    {
        $generaPDF = new GeneraPDF($xmlTimbrado);

        $xmlDOM = new DOMDocument();
        $xmlDOM->loadXML($xmlTimbrado);

        $cfdiPrefix = 'http://www.sat.gob.mx/cfd/3';

        //***************************************************************************************************
        $generaPDF->comprobante = $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Comprobante')->item(0);

        $Comprobante = new Comprobante;
        /*obtenemos la lista de variables de la clase*/
        $variables = get_class_vars(get_class($Comprobante));
        /*recoremos la lista de variables de clase*/
        foreach ($variables as $variable => $valor) {
            $Comprobante->$variable = $generaPDF->comprobante->getAttribute($variable);
        }

        //***************************************************************************************************
        $generaPDF->emisor = $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Emisor')->item(0);

        $Emisor = new Emisor;
        /*obtenemos la lista de variables de la clase*/
        $variables = get_class_vars(get_class($Emisor));
        /*recoremos la lista de variables de clase*/
        foreach ($variables as $variable => $valor) {
            $Emisor->$variable = $generaPDF->emisor->getAttribute($variable);
        }

        //***************************************************************************************************
        $generaPDF->receptor = $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Receptor')->item(0);

        $Receptor = new Receptor;
        /*obtenemos la lista de variables de la clase*/
        $variables = get_class_vars(get_class($Receptor));
        /*recoremos la lista de variables de clase*/
        foreach ($variables as $variable => $valor) {
            $Receptor->$variable = $generaPDF->receptor->getAttribute($variable);
        }
        //***************************************************************************************************

        $generaPDF->conceptos = $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Concepto')->item(0);

        $Conceptos = new Conceptos;
        $Conceptos->agregarConcepto();
        /*obtenemos la lista de variables de la clase*/
        $variables = get_class_vars(get_class($Conceptos));
        /*recoremos la lista de variables de clase*/
        foreach ($variables as $variable => $valor) {
            if(is_array($Conceptos->$variable)){

            }else{
                $Conceptos->$variable = $generaPDF->receptor->getAttribute($variable);
            }
        }

    }
}

/********************************************************************/
// define('RUTA_UTILERIAS', getcwd()."/../../utilerias/");
// define('RUTA_CADENA_ORIGINAL_XSLT', RUTA_UTILERIAS."cadenaoriginal_3_3.xslt");
// define('RUTA_CADENA_ORIGINAL_TFD_XSLT', RUTA_UTILERIAS."cadenaoriginal_TFD_1_1.xslt");
// require_once 'variablesCFDI.php';
// require_once 'PEM.php';
// require_once 'CFDIException.php';


// require_once '../../config/config.php';
// require_once '../../config/conexion.php';


// use cfdi33\GeneraPDF;

// $xmlTimbrado = "";
// $archivoZIP = "../Respuesta_988509114.zip";
// $archivoZIP = "LOOA650604E83_2.zip";
// $zip = new ZipArchive;
// if ($zip->open($archivoZIP) === TRUE) {
//     $xmlTimbrado = $zip->getFromIndex(0);
//     $nombreComprimido = $zip->getNameIndex(0);
//     $zip->close();
// } else {
//     echo 'falló';
// }

// // echo PHP_EOL."$nombreComprimido <br>";
// // echo PHP_EOL.pathinfo($nombreComprimido,PATHINFO_FILENAME)."<br>";
// // echo PHP_EOL.$xmlTimbrado."<br>";
// // echo PHP_EOL.gettype($xmlTimbrado)."<br>";
// // echo PHP_EOL."cadenaOriginalTFD <br>";
// // echo PEM::getCadenaOriginalTFD($xmlTimbrado);
// // echo pathinfo($nombreComprimido,PATHINFO_FILENAME);
// // echo "<br>";
// // $xmlPHP = str_replace("<", "&lt;", $xmlTimbrado);
// // echo str_replace(">", "&gt;", $xmlPHP);
// try{
//     // echo RUTA_CADENA_ORIGINAL_XSLT."<br>";
//     // echo RUTA_CADENA_ORIGINAL_TFD_XSLT."<br>";
//     GeneraPDF::generar(pathinfo($nombreComprimido,PATHINFO_FILENAME),$xmlTimbrado,true);
// }catch(CFDIException $ex){
//     echo $ex->getMessage();
// }
/********************************************************************/

// 
// if(isset($xmlTimbrado)){
    // echo PHP_EOL;
    // echo " ".gettype($xmlTimbrado);
    // echo PHP_EOL;
    // echo "Contenido del XML: ".$xmlTimbrado;
// 
    // echo PHP_EOL;
    // echo "SimpleXML";
    // echo PHP_EOL;
// 
    // $xmlSimple = simplexml_load_string($xmlTimbrado);
    // var_dump($xmlSimple);
    // echo PHP_EOL;
    // echo "DOMXML";
    // echo PHP_EOL;
// 
    // $xmlDOM = new DOMDocument();
    // $xmlDOM->loadXML($xmlTimbrado);
    // echo $xmlDOM->saveXML();
    // echo PHP_EOL;
    // echo "Emisor";
    // echo PHP_EOL;
    // $cfdiPrefix = 'http://www.sat.gob.mx/cfd/3';
    // $tfdPrefix = 'http://www.sat.gob.mx/TimbreFiscalDigital';
    // foreach ($xmlDOM->getElementsByTagNameNS($tfdPrefix, '*') as $element) {
        // echo 'local name: ', $element->localName, ', prefix: ', $element->prefix, "\n";
        // echo PHP_EOL;
    // }
    // $Emisor = $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Emisor')->item(0);
    // echo PHP_EOL;
    // $classEmisor = new Emisor();
    // /*obtenemos la lista de variables de la clase*/
    // $variables = get_class_vars(get_class($classEmisor));
    // /*recoremos la lista de variables de clase*/
    // foreach ($variables as $variable => $valor) {
        // $classEmisor->$variable = $Emisor->getAttribute($variable);
    // }
// 
    // echo "00.".$classEmisor->Nombre;
    // echo PHP_EOL;
    // echo $xmlDOM->getElementsByTagNameNS($cfdiPrefix, 'Emisor');
    // echo PHP_EOL;
// }
// echo "string";