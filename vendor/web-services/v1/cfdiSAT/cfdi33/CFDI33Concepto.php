<?php
namespace WebServices\cfdiSAT\cfdi33;

use DOMDocument;
use DOMElement;
use WebServices\cfdiSAT\cfdi33\CFDIException;

class CFDI33Concepto{
	public $impuestos;
	
	public function creaConcepto($Concepto=null){
		$concepto = null;
		if($Concepto instanceof Concepto) {
			$concepto = $this->cfdi->createElement( 'cfdi:Concepto' );
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($Concepto));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($Concepto->$variable)){
					// echo "<br>tiene $variable ".sizeof($Concepto->$variable)." registros<br>";
					foreach ($Concepto->$variable as $x => $val){
						if($val instanceof ConceptoImpuestos){
							$elemento = $this->creaConceptoImpuestos($val);
							if(($elemento instanceof DOMElement)){
								$concepto->appendChild($elemento);
							}
						}
					}
				}else{
					/*validamos si tiene asignado valor*/
					if(!is_null($Concepto->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Concepto-> decimos que cargue la variable */
						$concepto->setAttribute( $variable, $Concepto->$variable);
					}
				}
			}
		}
		return $concepto;
	}

	public function creaConceptoImpuestos($ConceptoImpuestos=null){
		$impuestos = null;
		if($ConceptoImpuestos instanceof ConceptoImpuestos) {
			// $this->impuestos = $this->cfdi->createElement( 'cfdi:Impuestos' );/*creamos el nodo Impuestos del CFDI*/
			$impuestos = $this->cfdi->createElement( 'cfdi:Impuestos' );/*creamos el nodo Impuestos del Concepto*/
			$traslados = null;
			$retenciones = null;
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($ConceptoImpuestos));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($ConceptoImpuestos->$variable)){
					foreach ($ConceptoImpuestos->$variable as $x => $valClass){
						$elemento = null;
						if($valClass instanceof ConceptoImpuestosRetencionesRetencion){
							$elemento = $this->creaConceptoImpuestosRetencionesRetencion($valClass);
							if(($elemento instanceof DOMElement)){
								if(is_null($retenciones)){
									$retenciones = $this->cfdi->createElement( 'cfdi:Retenciones' );
								}
								$retenciones->appendChild( $elemento );
							}
						}
						if($valClass instanceof ConceptoImpuestosTrasladosTraslado){
							$elemento = $this->creaConceptoImpuestosTrasladosTraslado($valClass);
							if(($elemento instanceof DOMElement)){
								if(is_null($traslados)){
									$traslados = $this->cfdi->createElement( 'cfdi:Traslados' );
								}
								$traslados->appendChild( $elemento );
							}
						}
					}
					if(($retenciones instanceof DOMElement)){
						$impuestos->appendChild($retenciones);
					}
					if(($traslados instanceof DOMElement)){
						$impuestos->appendChild($traslados);
					}
				}else{
					/*validamos si tiene asignado valor*/
					if(!is_null($ConceptoImpuestos->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Concepto-> decimos que cargue la variable */
						$impuestos->setAttribute( $variable, $ConceptoImpuestos->$variable);
					}
				}
			}
		}
		return $impuestos;
	}
	protected function creaConceptoImpuestosTraslados($ConceptoImpuestosTraslados=null){
		$traslados = null;
		if($ConceptoImpuestosTraslados instanceof ConceptoImpuestosTraslados) {
			$traslados = $this->cfdi->createElement( 'cfdi:Traslados' );
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($ConceptoImpuestosTraslados));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($ConceptoImpuestosTraslados->$variable)){
					foreach ($ConceptoImpuestosTraslados->$variable as $x => $val){
						if($val instanceof ConceptoImpuestosTrasladosTraslado){
							$elemento = $this->creaConceptoImpuestosTrasladosTraslado($val);
							if(($elemento instanceof DOMElement)){
								$traslados->appendChild($elemento);
							}
						}
					}
				}else{
					/*validamos si tiene asignado valor*/
					if(!is_null($ConceptoImpuestosTraslados->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Concepto-> decimos que cargue la variable */
						$traslados->setAttribute( $variable, $ConceptoImpuestosTraslados->$variable);
					}
				}
			}
		}
		return $traslados;
	}
	
	protected function creaConceptoImpuestosRetencionesRetencion($ConceptoImpuestosRetencionesRetencion=null){
		$retenciones = null;
		if($ConceptoImpuestosRetencionesRetencion instanceof ConceptoImpuestosRetencionesRetencion) {
			$retenciones = $this->cfdi->createElement( 'cfdi:Retencion' );
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($ConceptoImpuestosRetencionesRetencion));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($ConceptoImpuestosRetencionesRetencion->$variable)){

				}else{
					/*validamos si tiene asignado valor*/
					if(!is_null($ConceptoImpuestosRetencionesRetencion->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Concepto-> decimos que cargue la variable */
						$retenciones->setAttribute( $variable, $ConceptoImpuestosRetencionesRetencion->$variable);
					}
				}
			}
		}
		return $retenciones;
	}

	protected function creaConceptoImpuestosTrasladosTraslado($ConceptoImpuestosTrasladosTraslado=null){
		$traslados = null;
		if($ConceptoImpuestosTrasladosTraslado instanceof ConceptoImpuestosTrasladosTraslado) {
			$traslados = $this->cfdi->createElement( 'cfdi:Traslado' );
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($ConceptoImpuestosTrasladosTraslado));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($ConceptoImpuestosTrasladosTraslado->$variable)){

				}else{
					/*validamos si tiene asignado valor*/
					if(!is_null($ConceptoImpuestosTrasladosTraslado->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Concepto-> decimos que cargue la variable */
						$traslados->setAttribute( $variable, $ConceptoImpuestosTrasladosTraslado->$variable);
					}
				}
			}
		}
		return $traslados;
	}
}

?>