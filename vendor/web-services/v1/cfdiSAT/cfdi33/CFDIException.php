<?php
namespace WebServices\cfdiSAT\cfdi33;

use WebServices\cfdiSAT\Funciones;
use Exception;

class CFDIException extends Exception {
	public static $codigo_estatus = 0;
	public static $funcion_nombre = "";
	
	function getMensaje(){
		$errorMsg = $this->getMessage();
		return $errorMsg;
	}

	function salidaError($estatus_code=null){
		$funcion = self::$funcion_nombre;
		if(is_null($estatus_code)){
			$estatus_code = self::$codigo_estatus;
		}

		$errorMsg = 'Error en la Linea: '.$this->getLine().' en '.$this->getFile().': '.$this->getMessage();

		$response = array();
		$response["error"]   = true;
		$response["message"] = $this->getMessage(); 
		$response["body"]    = null;

		$cadenaMysql =  ((json_encode($response,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)));
		// $errorMsg .= ' Codificacion: '.$cadenaMysql;
		// Funciones::cargarSolicitud("No User",$funcion, $estatus_code,json_encode($response),$errorMsg);
		$timbres_usados = 0;
		Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus_code, $cadenaMysql ,$errorMsg);

		echoResponse($estatus_code, $response);
		
		/*Reestablecemos los valores estaticos*/
		self::$funcion_nombre = "";
		self::$codigo_estatus = "";
	}

}
?>