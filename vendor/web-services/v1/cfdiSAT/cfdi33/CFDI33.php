<?php
namespace WebServices\cfdiSAT\cfdi33;

// require_once 'cfdi33/CFDI33Esquema.php';
// require_once 'CFDI33Concepto.inc';


use DOMDocument;
use DOMElement;
use WebServices\cfdiSAT\cfdi33\CFDIException;
use ZipArchive;
// use ImpuestosRetencionesRetencion;
// use ImpuestosTrasladosTraslado;
// use cfdi33\CFDI33Esquema;

class CFDI33 extends CFDI33Concepto
{

	public $cfdi;
	public $comprobante;
	public $cfdirelacionados;
	public $emisor;
	public $receptor;
	public $conceptos;
	public $impuestos;


	function __construct (  )
	{
		$this->cfdi = new DOMDocument('1.0','UTF-8');
		// $this->comprobante = $this->cfdi->createElementNS( 'http://www.sat.gob.mx/cfd/3', 'cfdi:Comprobante', ' ' );
		// $this->comprobante = $this->cfdi->appendChild( $this->comprobante );
    //$emisor = new Emisor($this->cfdi);
    // $this->comprobante = $this->cfdi->createElement( 'cfdi:Comprobante', ' ' );
    // $this->comprobante = $this->cfdi->appendChild( $this->comprobante );
	}

	function creaComprobante($Comprobante = null){

		if($Comprobante instanceof Comprobante) {
			$this->comprobante = $this->cfdi->createElement( 'cfdi:Comprobante' );
			$this->comprobante = $this->cfdi->appendChild( $this->comprobante );
			$this->comprobante->setAttribute( "xmlns:cfdi","http://www.sat.gob.mx/cfd/3");
			$this->comprobante->setAttribute( "xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			$this->comprobante->setAttribute("xsi:schemaLocation","http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd");

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($Comprobante));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				/*validamos si tiene asignado valor*/
				if(!is_null($Comprobante->$variable)){
					/*creamo el atributo con el nombre de la variable y su valor
					al poner el signo $ despues de $Comprobante-> decimos que cargue la variable */
					$this->comprobante->setAttribute( $variable, $Comprobante->$variable);
				}
			}

		}
	}

	function creaCfdiRelacionados($CfdiRelacionados = null){
		if($CfdiRelacionados instanceof CfdiRelacionados) {

			if(is_null($this->comprobante)){
				throw new CFDIException("No se ha creado la instancia Comprobante");
			}

			$this->cfdirelacionados = $this->cfdi->createElement( 'cfdi:CfdiRelacionados' );
			$this->cfdirelacionados = $this->comprobante->appendChild($this->cfdirelacionados);

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($CfdiRelacionados));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				/*validamos si tiene asignado valor*/
				if(is_array($CfdiRelacionados->$variable)){
					foreach ($CfdiRelacionados->$variable as $x => $varClase){
						$elemento = $this->creaCfdiRelacionado($varClase);
						if(($elemento instanceof DOMElement)){
							$this->cfdirelacionados->appendChild($elemento);
						}
					}
				}else{
					if(!is_null($CfdiRelacionados->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Conceptos-> decimos que cargue la variable */
						$this->cfdirelacionados->setAttribute( $variable, $CfdiRelacionados->$variable);
					}
				}
			}
		}
	}

	public function creaCfdiRelacionado(CfdiRelacionado $CfdiRelacionado=null){
		$cfdirelacionado = null;
		if($CfdiRelacionado instanceof CfdiRelacionado) {
			$cfdirelacionado = $this->cfdi->createElement( 'cfdi:CfdiRelacionado' );
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($CfdiRelacionado));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(!is_array($CfdiRelacionado->$variable)){
					/*validamos si tiene asignado valor*/
					if(!is_null($CfdiRelacionado->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Concepto-> decimos que cargue la variable */
						$cfdirelacionado->setAttribute( $variable, $CfdiRelacionado->$variable);
					}
				}
			}
		}
		return $cfdirelacionado;
	}


	function creaEmisor($Emisor = null){
      // $Emisor = new Emisor($this->cfdi);
		if($Emisor instanceof Emisor) {

			if(is_null($this->comprobante)){
				throw new CFDIException("No se ha creado la instancia Comprobante");
			}

			$this->emisor = $this->cfdi->createElement( 'cfdi:Emisor' );
			$this->emisor = $this->comprobante->appendChild( $this->emisor );

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($Emisor));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				/*validamos si tiene asignado valor*/
				if(!is_null($Emisor->$variable)){
					/*creamos el atributo con el nombre de la variable y su valor
					al poner el signo $ despues de $Emisor-> decimos que cargue la variable */
					$this->emisor->setAttribute( $variable, $Emisor->$variable);
				}
			}
			
		}
	}

	function creaReceptor($Receptor = null){
		if($Receptor instanceof Receptor) {

			if(is_null($this->comprobante)){
				throw new CFDIException("No se ha creado la instancia Comprobante");
			}

			$this->receptor = $this->cfdi->createElement( 'cfdi:Receptor' );
			$this->receptor = $this->comprobante->appendChild( $this->receptor );

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($Receptor));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				/*validamos si tiene asignado valor*/
				if(!is_null($Receptor->$variable)){
					/*creamo el atributo con el nombre de la variable y su valor*/
					/*al poner el signo $ despues de $Receptor-> decimos que cargue la variable */
					$this->receptor->setAttribute( $variable, $Receptor->$variable);
				}
			}
		}
	}
	function creaConceptos($Conceptos = null){
		if($Conceptos instanceof Conceptos) {

			if(is_null($this->comprobante)){
				throw new CFDIException("No se ha creado la instancia Comprobante");
			}

			$this->conceptos = $this->cfdi->createElement( 'cfdi:Conceptos' );
			$this->conceptos = $this->comprobante->appendChild($this->conceptos);

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($Conceptos));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				//echo $variable."=".$Conceptos->$variable." ".is_array($Conceptos->$variable)."<br>";
				/*validamos si tiene asignado valor*/
				if(is_array($Conceptos->$variable)){
					foreach ($Conceptos->$variable as $x => $varClase){
						//echo get_class($varClase);
						if($varClase instanceof Concepto){
							$elemento = $this->creaConcepto($varClase);
							if(($elemento instanceof DOMElement)){
								$this->conceptos->appendChild($elemento);
							}
						}
					}
				}else{
					if(!is_null($Conceptos->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Conceptos-> decimos que cargue la variable */
						$this->conceptos->setAttribute( $variable, $Conceptos->$variable);
					}
				}
			}
		}
	}

	function creaImpuestos($Impuestos = null){
		if($Impuestos instanceof Impuestos) {

			if(is_null($this->comprobante)){
				throw new CFDIException("No se ha creado la instancia Comprobante");
			}

			$this->impuestos = $this->cfdi->createElement( 'cfdi:Impuestos' );
			$this->impuestos = $this->comprobante->appendChild($this->impuestos);
			$traslados = null;
			$retenciones = null;
			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($Impuestos));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($Impuestos->$variable)){
					foreach ($Impuestos->$variable as $x => $varClase){
						if($varClase instanceof ImpuestosRetencionesRetencion){
							$elemento = $this->creaImpuestosRetencionesRetencion($varClase);
							if(($elemento instanceof DOMElement)){
								if(is_null($retenciones)){
									$retenciones = $this->cfdi->createElement( 'cfdi:Retenciones' );
								}
								$retenciones->appendChild( $elemento );
							}
						}
						if($varClase instanceof ImpuestosTrasladosTraslado){
							$elemento = $this->creaImpuestosTrasladosTraslado($varClase);
							if(($elemento instanceof DOMElement)){
								if(is_null($traslados)){
									$traslados = $this->cfdi->createElement( 'cfdi:Traslados' );
								}
								$traslados->appendChild( $elemento );
							}
						}
					}
					if(($retenciones instanceof DOMElement)){
						$this->impuestos->appendChild($retenciones);
					}
					if(($traslados instanceof DOMElement)){
						$this->impuestos->appendChild($traslados);
					}
				}else{
					if(!is_null($Impuestos->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Impuestos-> decimos que cargue la variable */
						$this->impuestos->setAttribute( $variable, $Impuestos->$variable);
					}
				}
			}
		}
	}

	private function creaImpuestosTrasladosTraslado($ImpuestosTrasladosTraslado = null){
		$traslados = null;
		if($ImpuestosTrasladosTraslado instanceof ImpuestosTrasladosTraslado) {
			$traslados = $this->cfdi->createElement( 'cfdi:Traslado' );
			$traslados = $this->comprobante->appendChild($traslados);

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($ImpuestosTrasladosTraslado));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($ImpuestosTrasladosTraslado->$variable)){

				}else{
					if(!is_null($ImpuestosTrasladosTraslado->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Impuestos-> decimos que cargue la variable */
						$traslados->setAttribute( $variable, $ImpuestosTrasladosTraslado->$variable);
					}
				}
			}
		}
		return $traslados;
	}

	private function creaImpuestosRetencionesRetencion($ImpuestosRetencionesRetencion = null){
		$retencion = null;
		if($ImpuestosRetencionesRetencion instanceof ImpuestosRetencionesRetencion) {
			$retencion = $this->cfdi->createElement( 'cfdi:Retencion' );
			$retencion = $this->comprobante->appendChild($retencion);

			/*obtenemos la lista de variables de la clase*/
			$variables = get_class_vars(get_class($ImpuestosRetencionesRetencion));
			/*recoremos la lista de variables de clase*/
			foreach ($variables as $variable => $valor) {
				if(is_array($ImpuestosRetencionesRetencion->$variable)){

				}else{
					if(!is_null($ImpuestosRetencionesRetencion->$variable)){
						/*creamo el atributo con el nombre de la variable y su valor*/
						/*al poner el signo $ despues de $Impuestos-> decimos que cargue la variable */
						$retencion->setAttribute( $variable, $ImpuestosRetencionesRetencion->$variable);
					}
				}
			}
		}
		return $retencion;
	}

	function getXML()
	{
		// Parse the XML.
		// print $this->cfdi->saveXML();
		return $this->cfdi->saveXML();
	}
	function guardar($outXML = "cfdi33.xml"){
		$this->cfdi->save($outXML);
		return $outXML;
		// $this->guardarZIP();
	} 

	function guardarZIP()
	{
		$this->guardar();
		/* 
		Crear un fichero en el temporal 
		directorio de archivos utilizando sys_get_temp_dir()
		*/
		$temp_file = tempnam(sys_get_temp_dir(), 'FACXML');
		$gestor = fopen($temp_file, "w");
		fwrite($gestor, $this->cfdi->saveXML());
		fclose($gestor);

		$zip = new ZipArchive();

		$name = basename($temp_file);
		$filename = $name.'.zip';

		if($zip->open($filename,ZIPARCHIVE::CREATE)===true) {
			// $zip->addFile('a.txt');
			$zip->addFile($temp_file,$name.".xml");
			$zip->close();
			unlink($temp_file);
		} else {
			$filename = false;
			throw new CFDIException("Problemas al Comprimir el CFDI Generado, Contacte al Area de Soporte");
		}

		return $filename;
	}
}