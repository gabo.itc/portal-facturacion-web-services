<?php

namespace WebServices\cfdiSAT;

use Psr\Http\Message\ResponseInterface as Response;
use WebServices\cfdiSAT\Funciones;
use Exception;

class FacturaException extends Exception {
	public static $codigo_estatus = 0;
	public static $funcion_nombre = "";
	public static $sifeiCodigo;
	public static $sifeiError;
	public static $sifeiMessage;
	// public static $funcion_nombre = "";
	// function getMensaje(){
	// 	$errorMsg = 'Error en la Linea: '.$this->getLine().' en '.$this->getFile()
	// 	.': <b>'.$this->getMessage()."</b>";
	// 	return $errorMsg;
	// }
	function getMensaje(){
		$errorMsg = $this->getMessage();
		return $errorMsg;
	}

	function salidaError(Response $response = null,$estatus_code=null) : Response{
		$funcion = self::$funcion_nombre;
		if(is_null($estatus_code)){
			$estatus_code = self::$codigo_estatus;
		}

		$contenidoError = $this->getMessage();
		if(isset(self::$sifeiCodigo)){
			$contenidoError = "";
			$contenidoError .= ";Codigo: ".self::$sifeiCodigo;
			$contenidoError .= ";Error: ".self::$sifeiError;
			$contenidoError .= ";Mensaje: ".self::$sifeiMessage;
		}

		$errorMsg = 'Error en la Linea: '.$this->getLine().' en '.$this->getFile().': '.$contenidoError;

		$respuesta = array();
		$respuesta["error"]   = true;
		$respuesta["message"] = $this->getMessage(); 
		$respuesta["body"]    = null;

		$cadenaMysql =  ((json_encode($respuesta,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)));
		// $errorMsg .= ' Codificacion: '.$cadenaMysql;
		// echo "l ".$this->getTrace()[2]['function'].PHP_EOL;
		// var_dump($this->getTrace()[2]);
		// $funcion = $this->getTrace()[2]['function'];
		// Funciones::cargarSolicitud("No User",$funcion, $estatus_code,json_encode($response),$errorMsg);
		$timbres_usados = 0;
		Funciones::cargarSolicitud($funcion,$timbres_usados, $estatus_code,$cadenaMysql,$errorMsg);
		
		// $estatus_code = 500;
		// http_response_code('E501');
		// header('Content-Type: application/json');
		// echo json_encode($response);
		// echoResponse($estatus_code, $response);
		// if(!is_null($response)){
		// 	http_response_code($estatus_code);
		// 	header('Content-Type: application/json');
		// 	echo json_encode($respuesta);
		// 	exit();
		// }else{
		// 	$response->getBody()->write(json_encode($respuesta));
		// 	$response = $response->withHeader('Content-Type', 'application/json');
		// 	// $response->withHeader('Content-Type', 'application/json');
		// }
		/*Reestablecemos los valores estaticos*/
		self::$codigo_estatus = 0;
		self::$funcion_nombre = "";
		self::$sifeiCodigo = null;
		self::$sifeiError = null;
		self::$sifeiMessage = null;
		return $response;
	}

}
?>