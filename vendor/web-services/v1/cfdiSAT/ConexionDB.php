<?php

namespace WebServices\cfdiSAT;

// include_once 'Configuracion.php';
include_once 'DBException.php';
use WebServices\cfdiSAT\Configuracion;
// use Configuracion;
use PDO;
/**
 * 
 */
define('cat_cfdi_forma_pago','cat_cfdi_forma_pago');
define('cat_cfdi_metodo_pago','cat_cfdi_metodo_pago');
define('cat_cfdi_regimen_fiscal','cat_cfdi_regimen_fiscal');
define('cat_cfdi_tipo_comprobante','cat_cfdi_tipo_comprobante');
define('cat_cfdi_uso_cfdi','cat_cfdi_uso_cfdi');
define('cat_moneda','cat_moneda');

define('cat_clientes','cat_clientes');
define('cat_clientes_contacto','cat_clientes_contacto');
define('cat_usuario','cat_usuario');
define('configuracion_sifei','configuracion_sifei');
define('control_solicitud_cfdi','control_solicitud_cfdi');
define('control_timbres','control_timbres');

if( !defined('DB_HOST') ){
	$config = new Configuracion();
}

class ConexionDB
{
	public $DBHost = DB_HOST;
	public $DBPort = DB_PORT;
	public $DBName = DB_NAME;
	public $DBUser = DB_USER;
	public $DBPass = DB_PASS;
	public $charset = "utf8";
	public $pdo;
	public $stmt; 

	public $config = [PDO::ATTR_CASE => PDO::CASE_LOWER, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_ORACLE_NULLS => PDO::NULL_TO_STRING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ];

	function __construct()
	{
		try{
			$this->pdo = new PDO("mysql:host={$this->DBHost};dbname={$this->DBName};port={$this->DBPort};charset={$this->charset}",$this->DBUser,$this->DBPass,$this->config);
	// $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $ex){
			throw new DBException("Problemas en la Conexion a la Base de Datos, Contacte con el Administrador del Sistema");
		}
	}
	
	public function close() {
		$this->queryExecute("KILL CONNECTION_ID();");
		$this->pdo=null;
	}

	public function queryExecute($sql){
		return $this->pdo->query($sql);
	}

	public function queryPrepare($sql){
		/*Retorna el Statement*/
		$this->stmt = $this->pdo->prepare($sql);
		return $this->stmt;
	}
	
	public function bind_param($stmt,$tipo,$valor){
		if(is_null($stmt)){/*si el parametro es nulo*/
			$stmt = $this->stmt;/*cargamos el valor de la variable local*/
		}
		if(is_null($stmt)){/*si sigue siendo NULO mandamos una excepcion*/
			throw new DBException("Instancia Statement No Creada");
		}
		return $stmt->bind_param($tipo,$valor);
	}
	
	public function execute($stmt){
		if(is_null($stmt)){/*si el parametro es nulo*/
			$stmt = $this->stmt;/*cargamos el valor de la variable local*/
		}
		if(is_null($stmt)){/*si sigue siendo NULO mandamos una excepcion*/
			throw new DBException("Instancia Statement No Creada");
		}
		return $stmt->execute();
	}
}

?>