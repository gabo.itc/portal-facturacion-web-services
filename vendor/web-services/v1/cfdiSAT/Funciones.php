<?php


namespace WebServices\cfdiSAT;

use WebServices\cfdiSAT\cfdi33\CFDI33;
use WebServices\cfdiSAT\cfdi33\Comprobante;
use WebServices\cfdiSAT\cfdi33\CfdiRelacionados;
use WebServices\cfdiSAT\cfdi33\CfdiRelacionado;
use WebServices\cfdiSAT\cfdi33\Emisor;
use WebServices\cfdiSAT\cfdi33\Receptor;
use WebServices\cfdiSAT\cfdi33\Conceptos;
use WebServices\cfdiSAT\cfdi33\Concepto;
use WebServices\cfdiSAT\cfdi33\ConceptoImpuestos;
use WebServices\cfdiSAT\cfdi33\ConceptoImpuestosTrasladosTraslado;
use WebServices\cfdiSAT\cfdi33\ConceptoImpuestosRetencionesRetencion;
use WebServices\cfdiSAT\cfdi33\Impuestos;
use WebServices\cfdiSAT\cfdi33\PEM;
use WebServices\cfdiSAT\cfdi33\CFDIException;
use WebServices\cfdiSAT\ConexionDB;
use stdClass;
use split;
use ErrorException;
use PDOStatement;
use PDO;
/**
 * 
 */
class Funciones 
{
	public $msgExcepcion;
	public $msgExcepcionFaltante;
	public $msgExcepcionAdicional;

	public $arrValidacionXML = array
	('Comprobante{R}' => array
		(
			'Serie' => '{"acepta":{"vacio":true,"pattern":"/^([a-zA-Z0-9]+)$/"}}', 
			'Folio{R}' => '{"acepta":{"pattern":"/^([a-zA-Z0-9]+)$/"}}',
			'Fecha{R}' => '{"acepta":{"vacio":true,"pattern":"/^(20[1-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])$/"}}',
			'FormaPago{R}' => '{"acepta":{"vacio":false}}',
			'SubTotal{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
			'Descuento' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
			/*'Moneda' => '',= "MXN";*/ 
			/*'TipoCambio' => '', */
			'Total{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
			'TipoDeComprobante{R}' => '{"acepta":{"pattern":"/^[A-Z]{1}$/"}}',
			'MetodoPago{R}' => '{"acepta":{"vacio":false}}',
			'LugarExpedicion{R}' => '{"acepta":{"pattern":"/^([0-9]{5})$/"}}',
			/*'Confirmacion' => '',*/
			'CfdiRelacionados' => array
			(
				'TipoRelacion{R}' => '',
				'CfdiRelacionado{R}' => array
				(
					'UUID{R}' => '{"acepta":{"pattern":"/^[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}$/"}}'
				)
			),
			'Emisor{R}' => array
			(
				'Rfc' => '{"acepta":{"pattern":"/^([A-Z]{3,4})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{3})$/"}}',
				'Nombre' => '', 
				'RegimenFiscal{R}' => ''
			),
			'Receptor{R}' => array
			(
				'Rfc{R}' => '{"acepta":{"pattern":"/^([A-Z]{3,4})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{3})$/"}}',
				'Nombre' => '{"acepta":{"vacio":true}}', 
				'ResidenciaFiscal' => '{"acepta":{"vacio":false}}',
				'NumRegIdTrib' => '{"acepta":{"vacio":false}}',
				'UsoCFDI{R}' => '{"acepta":{"vacio":false}}'
			),
			'Conceptos' => array
			(
				'Concepto{R}' => array
				(
					'ClaveProdServ{R}' => '{"acepta":{"pattern":"/^([0-9]{8})$/"}}',
					'NoIdentificacion' => '',
					'Cantidad{R}' =>  '{"acepta":{"pattern":"/^([0-9]+)(.[0-9]{6}\*)$/"}}',
					'ClaveUnidad{R}' => '',
					'Unidad' => '',
					'Descripcion{R}' => '',
					'ValorUnitario{R}' =>  '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
					'Importe{R}' =>  '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
					'Descuento' =>  '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
					'Impuestos' => array
					(
						'Retenciones' => array
						(
							'Retencion{R}' => array
							( 
								'Base{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
								'Impuesto{R}' => '',
								'TipoFactor{R}' => '',
								'TasaOCuota{R}' =>  '{"acepta":{"pattern":"/^([0-9]+).([0-9]{6})$/"}}',
								'Importe{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}'
							)
						),
						'Traslados' => array
						(
							'Traslado{R}' => array
							( 
								'Base{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}',
								'Impuesto{R}' => '',
								'TipoFactor{R}' => '',
								'TasaOCuota{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{6})$/"}}',
								'Importe{R}' => '{"acepta":{"pattern":"/^([0-9]+).([0-9]{2,6})$/"}}'
							)
						)
					)
				)
			)
		)
	);
	
	function __construct()
	{
		# code...
	}

	public function validaArray($arrJsonValue,$valKey,$nvl)
	{
		// $this->msgExcepcion .= "x";
		// if($valKey=="CfdiRelacionados" || strpos($nvl, "CfdiRelacionados")!==false)
		// if($valKey=="Comprobante" || $nvl == "Comprobante")
		if(sizeof($arrJsonValue)<=0){
			$this->msgExcepcionFaltante .= "[$nvl (Vacio)]";
		}else{
			foreach ($arrJsonValue as $key => $value) {

				if(isset($arrJsonValue[$key]->$valKey)){/*si el parametro fue enviado*/
					$conErrores = true;
							// $this->msgExcepcionAdicional .= "[typeof]".json_encode($arrJsonValue);

					$patternForArray = '/\([0-9]\)/';
					$cadenaAuxiliar = $nvl.$valKey;/*cadena Nivel*/
					$cadenaAuxiliar = preg_replace($patternForArray, '', $cadenaAuxiliar);
					$arrAux = preg_split ("(->)", $cadenaAuxiliar);
					$arrValXML = $this->arrValidacionXML;/*Arreglo Validacion Original*/
					foreach ($arrAux as $_key => $_value) {
						// $this->msgExcepcionAdicional .= "[index $_value]";//sout de ejemplo
						if (array_key_exists($_value, $arrValXML)){
							$auxValXML = $arrValXML[$_value];
						}else if (array_key_exists($_value."{R}", $arrValXML)){
							$auxValXML = $arrValXML[$_value."{R}"];
						}
						$arrValXML = $auxValXML;
					}
					// $this->msgExcepcionFaltante .= "[$nvl$valKey($key)]";
					// $this->msgExcepcionFaltante .= "$[".$nvl.$valKey."(".(!is_array($arrValXML)).")]$";
						//echo "vardum->$key ";
						//var_dump($arrJsonValue);

					$this->validacionRecursiva($arrValXML,$arrJsonValue[$key]->$valKey,$nvl.$valKey."($key)->");
				}else{/*Recorremos los hijos del atributo*/
					if($value instanceof stdClass){
						foreach ($value as $_key => $_value) {

							// $auxnvl = substr($nvl, 0,strlen($nvl)-2);
							$this->msgExcepcionAdicional .= "[$nvl($key)$_key]".json_encode($valKey);
						}
					}else{
						$this->msgExcepcionFaltante .= "[$nvl$valKey($key)]";
					}
					//$this->validacionRecursiva($value,$arrJsonValue->$key,$nvl.$key."->");
				}
			}
		}
		return !is_null($conErrores);
	}

	/*
	 * Metodo para la validacion del parametro recibido basado en un Arreglo el cual contiene los que son obligatorios
	 * @param $arrName variable Array que contiene los parametros a validar
	 * @param $arrValue variable Array que contiene los valores de los parametros recibidos
	 * @param $nvl variable que contiene el nivel que se esta validando
	 */
	public function validacionRecursiva($arrValidacion,$arrJsonValue,$nvl='')
	{
		try{
			if(!is_null($arrValidacion)){
				foreach ($arrValidacion as $key => $valorArreglo) {
					$esRequerido = strpos($key, "{R}");
					$key = str_replace("{R}", "", $key);
					if(is_array($valorArreglo)){
						// echo "$nvl.{$key}[".is_array($arrJsonValue)."][".!isset($arrJsonValue->$key)."] \n";
						if($key=="Concepto"){
							// echo "string".json_encode($arrJsonValue).PHP_EOL;
						}
						if(is_array($arrJsonValue)){/*validamos si el contenido del JSON recibido es un array*/
							$this->validaArray($arrJsonValue,$key,$nvl);
						}else{	/*no es un array es parte del stdClass*/
							if(!isset($arrJsonValue->$key)){
								if($esRequerido){
									$this->msgExcepcionFaltante .= "[$nvl$key]";
								}
							}else{
								if($key=="CfdiRelacionado"){
									$CfdiRelacionado = array();
									foreach ($arrJsonValue->$key as $keyRelacionado => $value) {
										$cfdx = array("CfdiRelacionado" => $value);
										array_push($CfdiRelacionado,$cfdx);
									}
									//$CfdiRelacionados = array("CfdiRelacionados" => $CfdiRelacionado);
									// echo "Aqui esta la prueba -> ".json_encode($CfdiRelacionado).PHP_EOL;

									// echo "$nvl.{$key}[".is_array($arrValidacion)."][".json_encode($arrJsonValue->$key)."] \n";
									$CfdiRelacionado = json_encode($CfdiRelacionado);
									$CfdiRelacionado = json_decode($CfdiRelacionado);
									$this->validaArray($CfdiRelacionado,$key,$nvl);
								}else{
									$this->validacionRecursiva($valorArreglo,$arrJsonValue->$key,$nvl.$key."->");
								}
							}
						}

					}else{

						$id = $nvl.$key;

						if(is_array($arrJsonValue)){
							$this->validaArray($arrJsonValue,$key,$id);
						}else{
							if(isset($arrJsonValue->$key)){/*si existe el atributo*/
								if(!empty($valorArreglo)){
							// echo PHP_EOL.$valorArreglo;
									$valorParaValidar = json_decode($valorArreglo);
									if(empty($arrJsonValue->$key)){
										if(!$valorParaValidar->acepta->vacio){
											$this->msgExcepcionFaltante .= "[$id{No Puede Ser Vacio}]";
										}
									}else{
										if (isset($valorParaValidar->acepta->pattern)){
											$pattern = $valorParaValidar->acepta->pattern;

											if(isset($pattern)){
												// echo PHP_EOL.$pattern." ".$arrJsonValue->$key;	
												$matches = null;
												if(!preg_match($pattern,$arrJsonValue->$key,$matches)){
													$this->msgExcepcionFaltante .= "[$id{Formato No Valido}]";
												}
											}
										}
									}
									if($valorParaValidar){

									}
								}
							}else{/*si el atributo no existe*/
								if($esRequerido){
									$this->msgExcepcionFaltante .= "[$id]";
								}
							}
						}

					}
				}
			}else{
				$this->msgExcepcionFaltante .= "[Array is NULL".json_encode($arrJsonValue)."]".$nvl;
			}
		}catch(ErrorException $ex){
			// $this->msgExcepcion .= "Error de Parseo->".$nvl."{$ex->getMessage()}";
			$this->msgExcepcion .= "Error de Parseo->".$nvl."{$ex->getLine()}";
		}
	}

	/**
	 * Valida la estructura del json recibido
	 * @throw Devuelve una excepcion en caso de no cumplir con la estructura minima
	 * */
	public static function validar($json){

		$valid = new Funciones();
		// var_dump($json);
		$valid->validacionRecursiva($valid->arrValidacionXML,$json);

		if(isset($valid->msgExcepcionAdicional)){
			CFDIException::$codigo_estatus = 409;
			throw new CFDIException("Parametros Adicionales No Permitidos ".$valid->msgExcepcionAdicional);
		}
		if(isset($valid->msgExcepcionFaltante)){
			CFDIException::$codigo_estatus = 409;
			throw new CFDIException("No se Encontro Valor Para ".$valid->msgExcepcionFaltante);
		}
		if(isset($valid->msgExcepcion)){
			CFDIException::$codigo_estatus = 409;
			throw new CFDIException($valid->msgExcepcion);
		}

	}

	/**
	 * Recupera el contenido del certificado del CSD
	 * */
	static function getMySQLCSDCertificado($rfc='')
	{
		$contenido = '';
		/*Consulta para obtener los timbres usados*/
		$conexion = new ConexionDB();
		$sql = "SELECT csd_certificado FROM ".DB_NAME_REAL.".".cat_clientes." MV WHERE estatus = 'A' AND rfc = '{$rfc}' ";
		$stmt = $conexion->queryPrepare($sql);

		$stmt->execute();
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if(sizeof($datos) > 0){
			$contenido = $datos[0]['csd_certificado'];
		}
		return $contenido;
	}

	/**
	 * Recupera el contenido de la llave del CSD 
	 * 
	**/
	static function getMySQLCSDLlave($rfc='')
	{
		$contenido = '';
		/*Consulta para obtener los timbres usados*/
		$conexion = new ConexionDB();
		$sql = "SELECT csd_llave FROM ".DB_NAME_REAL.".".cat_clientes." MV WHERE estatus = 'A' AND rfc = '{$rfc}' ";
		$stmt = $conexion->queryPrepare($sql);

		$stmt->execute();
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if(sizeof($datos) > 0){
			$contenido = $datos[0]['csd_llave'];
		}
		return $contenido;
	}

	/**
	 * Recupera el la contraseña de la llave del CSD 
	 * 
	**/
	static function getMySQLCSDPassword($rfc='')
	{
		$contenido = '';
		/*Consulta para obtener los timbres usados*/
		$conexion = new ConexionDB();
		$sql = "SELECT csd_password FROM ".DB_NAME_REAL.".".cat_clientes." MV WHERE estatus = 'A' AND rfc = '{$rfc}' ";
		$stmt = $conexion->queryPrepare($sql);

		$stmt->execute();
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if(sizeof($datos) > 0){
			$contenido = $datos[0]['csd_password'];
		}
		return $contenido;
	}

	static function crearXML($json)	
	{
		self::validar($json);

		// var_dump($json->Comprobante);
		$folio = $json->Comprobante->Folio;
		// echo $folio;

		$emisorRfc = $json->Comprobante->Emisor->Rfc;
		$emisorNombre = $json->Comprobante->Emisor->Nombre;
		$emisorRegimenFiscal = $json->Comprobante->Emisor->RegimenFiscal;

		$receptorRfc = $json->Comprobante->Receptor->Rfc;
		$receptorNombre =  $json->Comprobante->Receptor->Nombre;
		$receptorNombre = empty($receptorNombre) ? null : $receptorNombre;
		$receptorUsoCFDI =  $json->Comprobante->Receptor->UsoCFDI;

		// $nombre_archivo = RUTA_LOCAL_XML.$emisorRfc."_".$receptorRfc."_".$folio.".xml";
		/** *****************************************************************/

		// $rutaCertificado = RUTA_CERTIFICADO."/$emisorRfc/certificado.cer";
		// $rutaLlave = RUTA_CERTIFICADO."/$emisorRfc/llave.key";
		// $rutaPass = RUTA_CERTIFICADO."/$emisorRfc/pass.txt";

		$pem = new PEM();
		$pem->getPemCertificado(self::getMySQLCSDCertificado($emisorRfc),false);
		$pem->getPemLlave(self::getMySQLCSDLlave($emisorRfc),false);
		$pem->setPassword(self::getMySQLCSDPassword($emisorRfc),false);
		$pem->NoCertificado();/*Generamos el Numero de Certificado*/
		// try{

		$cfdi33 = new CFDI33();
		$comprobante = new Comprobante();

		/*$comprobante->Version = "";*/
		if(!empty($json->Comprobante->Serie)){
			$comprobante->Serie = $json->Comprobante->Serie;
		}
		$comprobante->Folio = $json->Comprobante->Folio;
		$comprobante->setFecha($json->Comprobante->Fecha);
		$comprobante->Sello = null;
		$comprobante->FormaPago = $json->Comprobante->FormaPago;
		$comprobante->NoCertificado = $pem->NoCertificado();
		$comprobante->Certificado = $pem->certificado;
		$comprobante->CondicionesDePago = $json->Comprobante->CondicionesDePago;
		$comprobante->SubTotal = $json->Comprobante->SubTotal;
		$comprobante->Descuento = $json->Comprobante->Descuento;
		$comprobante->Moneda = $json->Comprobante->Moneda;
		$comprobante->TipoCambio = $json->Comprobante->TipoCambio;
		$comprobante->Total = $json->Comprobante->Total;
		$comprobante->TipoDeComprobante = $json->Comprobante->TipoDeComprobante;
		$comprobante->MetodoPago = $json->Comprobante->MetodoPago;
		$comprobante->LugarExpedicion = $json->Comprobante->LugarExpedicion;
		/* $comprobante->Confirmacion = "";*/

		$cfdi33->creaComprobante($comprobante);

		if(isset($json->Comprobante->CfdiRelacionados)){

			$cfdiRelacionados = new CfdiRelacionados();
			{
				$cfdiRelacionados->TipoRelacion = $json->Comprobante->CfdiRelacionados->TipoRelacion;

				$arrCfdiRelacionados = $json->Comprobante->CfdiRelacionados->CfdiRelacionado;
				if(!is_array($arrCfdiRelacionados)){
					$arrCfdiRelacionados = array($arrCfdiRelacionados);
				}

				$CfdiRelacionado = array();
				foreach ($arrCfdiRelacionados as $keyRelacionado => $value) {
					$cfdx = array("CfdiRelacionado" => $value);
					array_push($CfdiRelacionado,$cfdx);
				}

				$CfdiRelacionado = json_encode($CfdiRelacionado);
				$arrCfdiRelacionados = json_decode($CfdiRelacionado);

				if(sizeof($arrCfdiRelacionados) > 15){
					CFDIException::$codigo_estatus = 409;
					throw new CFDIException("No Es Posible Relacionar Mas de 15 Documentos en el Mismo CFDI");
				}

				foreach ($arrCfdiRelacionados as $key => $value) {
					/*Proceso para cargar los valores del CfdiRelacionado*/
					$cfdiRelacionado = new CfdiRelacionado();
					$arrVarCfdiRelacionado = get_class_vars(get_class($cfdiRelacionado));
					foreach ($arrVarCfdiRelacionado as $_key => $_value) {
						if(!is_array($cfdiRelacionado->$_key)){/*si el atributo no es un array*/
							$cfdiRelacionado->$_key = $value->CfdiRelacionado->$_key;
						}
					}				
					$cfdiRelacionados->agregarCfdiRelacionado($cfdiRelacionado);
				}
			}
			$cfdi33->creaCfdiRelacionados($cfdiRelacionados);
		}


		$emisor = new Emisor();
		$emisor->Rfc = $emisorRfc;
		$emisor->Nombre = $emisorNombre;
		$emisor->RegimenFiscal = $emisorRegimenFiscal;
		$cfdi33->creaEmisor($emisor);

		$receptor = new Receptor();
		$receptor->Rfc = $receptorRfc;
		$receptor->Nombre = $receptorNombre;
		$receptor->UsoCFDI = $receptorUsoCFDI;
		$cfdi33->creaReceptor($receptor);

		$conceptos = new Conceptos();
		{
			$arrConceptos = $json->Comprobante->Conceptos;
			if(!is_array($json->Comprobante->Conceptos)){
				$arrConceptos = array($json->Comprobante->Conceptos);
			}
			if(sizeof($arrConceptos) > 3000){
				CFDIException::$codigo_estatus = 409;
				throw new CFDIException("No Es Posible Timbrar Mas de 3000 Conceptos en el Mismo CFDI");
			}
			foreach ($arrConceptos as $key => $value) {
				/*Proceso para cargar los valores del Concepto*/
				$concepto = new Concepto();
				$arrVarConceptos = get_class_vars(get_class($concepto));
				foreach ($arrVarConceptos as $_key => $_value) {
					if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
						$concepto->$_key = $value->Concepto->$_key;
						// var_dump($value);
					}
				}
				if(isset($value->Concepto->Impuestos)){
					$conceptoImpuestos = new ConceptoImpuestos();

					
					if(isset($value->Concepto->Impuestos->Retenciones)){

						$arrRetenciones = $value->Concepto->Impuestos->Retenciones;
						if(!is_array($arrRetenciones)){
							$arrRetenciones = array($arrRetenciones);
						}

						foreach ($arrRetenciones as $keyRetencion => $valueRetencion) {
							if(isset($valueRetencion->Retencion)){
								$conceptoImpuestosRetencionesRetencion = new ConceptoImpuestosRetencionesRetencion();
								$arrVarIRRetencion = get_class_vars(get_class($conceptoImpuestosRetencionesRetencion));
								foreach ($arrVarIRRetencion as $_key => $_value) {
									if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
										$conceptoImpuestosRetencionesRetencion->$_key = $valueRetencion->Retencion->$_key;
									}
								}
								$conceptoImpuestos->agregarRetenciones($conceptoImpuestosRetencionesRetencion);
							}
						}
					}

					if(isset($value->Concepto->Impuestos->Traslados)){

						$arrTraslados = $value->Concepto->Impuestos->Traslados;
						if(!is_array($arrTraslados)){
							$arrTraslados = array($arrTraslados);
						}

						foreach ($arrTraslados as $keyTraslado => $valueTraslado) {
							if(isset($valueTraslado->Traslado)){
								$conceptoImpuestosTrasladosTraslado = new ConceptoImpuestosTrasladosTraslado();
								$arrVarITTraslado = get_class_vars(get_class($conceptoImpuestosTrasladosTraslado));
								foreach ($arrVarITTraslado as $_key => $_value) {
									if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
										$conceptoImpuestosTrasladosTraslado->$_key = $valueTraslado->Traslado->$_key;
									}
								}
								$conceptoImpuestos->agregarTraslados($conceptoImpuestosTrasladosTraslado);
							}
						}
					}

					$concepto->agregarImpuestos($conceptoImpuestos);
				}				
				$conceptos->agregarConcepto($concepto);
			}
		}

		$cfdi33->creaConceptos($conceptos);

		if(sizeof($conceptos->arrConceptoImpuestos)>0){
			$impuesto = new Impuestos();
			$impuesto->procesaConceptoImpuestos($conceptos->arrConceptoImpuestos);
			$cfdi33->creaImpuestos($impuesto);
		}

		$domXML = $cfdi33->getXML();

		$cadenaOriginal = $pem->getCadenaOriginal($domXML);

		$Sello = $pem->getSello($cadenaOriginal);
		
		// echo "xml->".PHP_EOL.$domXML;
		// CFDIException::$codigo_estatus = 409;
		// throw new CFDIException("Excepcion de prueba");
		if(!is_bool($Sello)){
			$cfdi33->comprobante->setAttribute('Sello',$Sello);

			$rutaZip = $cfdi33->guardarZip();
			if(!is_bool($rutaZip)){
				return $rutaZip;
			}
		}
		return false;
	}

	static function crearXMLTest($json,$creaZip = false)	
	{
		self::validar($json);

		$folio = $json->Comprobante->Folio;

		$emisorRfc = $json->Comprobante->Emisor->Rfc;
		$emisorNombre = $json->Comprobante->Emisor->Nombre;
		$emisorRegimenFiscal = $json->Comprobante->Emisor->RegimenFiscal;

		$receptorRfc = $json->Comprobante->Receptor->Rfc;
		$receptorNombre =  $json->Comprobante->Receptor->Nombre;
		$receptorUsoCFDI =  $json->Comprobante->Receptor->UsoCFDI;

		$nombre_archivo = RUTA_LOCAL_XML.$emisorRfc."_".$receptorRfc."_".$folio.".xml";
		/** *****************************************************************/

		$rutaCertificado = RUTA_CERTIFICADO."/$emisorRfc/certificado.cer";
		$rutaLlave = RUTA_CERTIFICADO."/$emisorRfc/llave.key";
		$rutaPass = RUTA_CERTIFICADO."/$emisorRfc/pass.txt";

		$pem = new PEM();
		$pem->getPemCertificado($rutaCertificado);
		$pem->getPemLlave($rutaLlave);
		$pem->setPassword($rutaPass,true);

		$cfdi33 = new CFDI33();
		$comprobante = new Comprobante();

		/*$comprobante->Version = "";*/
		$comprobante->Serie = $json->Comprobante->Serie;
		$comprobante->Folio = $json->Comprobante->Folio;
		$comprobante->setFecha($json->Comprobante->Fecha);
		$comprobante->Sello = null;
		$comprobante->FormaPago = $json->Comprobante->FormaPago;
		$comprobante->NoCertificado = $pem->NoCertificado();
		$comprobante->Certificado = $pem->certificado;
		$comprobante->CondicionesDePago = $json->Comprobante->CondicionesDePago;
		$comprobante->SubTotal = $json->Comprobante->SubTotal;
		$comprobante->Descuento = $json->Comprobante->Descuento;
		$comprobante->Moneda = $json->Comprobante->Moneda;
		$comprobante->TipoCambio = $json->Comprobante->TipoCambio;
		$comprobante->Total = $json->Comprobante->Total;
		$comprobante->TipoDeComprobante = $json->Comprobante->TipoDeComprobante;
		$comprobante->MetodoPago = $json->Comprobante->MetodoPago;
		$comprobante->LugarExpedicion = $json->Comprobante->LugarExpedicion;
		/* $comprobante->Confirmacion = "";*/

		$cfdi33->creaComprobante($comprobante);

		$emisor = new Emisor();
		$emisor->Rfc = $emisorRfc;
		$emisor->Nombre = $emisorNombre;
		$emisor->RegimenFiscal = $emisorRegimenFiscal;
		$cfdi33->creaEmisor($emisor);

		$receptor = new Receptor();
		$receptor->Rfc = $receptorRfc;
		$receptor->Nombre = $receptorNombre;
		$receptor->UsoCFDI = $receptorUsoCFDI;
		$cfdi33->creaReceptor($receptor);

		$conceptos = new Conceptos();
		{
			$arrConceptos = $json->Comprobante->Conceptos;
			if(!is_array($json->Comprobante->Conceptos)){
				$arrConceptos = array($json->Comprobante->Conceptos);
			}
			/*Recorremos la lista deconceptos************************************/
			foreach ($arrConceptos as $key => $value) {
				/*Proceso para cargar los valores del Concepto*/
				$concepto = new Concepto();
				$arrVarConceptos = get_class_vars(get_class($concepto));
				foreach ($arrVarConceptos as $_key => $_value) {
					if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
						$concepto->$_key = $value->Concepto->$_key;
						// var_dump($value);
					}
				}

				if(isset($value->Concepto->Impuestos)){/*Si tiene Impuestos*/
					$conceptoImpuestos = new ConceptoImpuestos();

					if(isset($value->Concepto->Impuestos->Retenciones)){

						$arrRetenciones = $value->Concepto->Impuestos->Retenciones;
						if(!is_array($arrRetenciones)){
							$arrRetenciones = array($arrRetenciones);
						}

						foreach ($arrRetenciones as $keyRetencion => $valueRetencion) {
							if(isset($valueRetencion->Retencion)){
								$conceptoImpuestosRetencionesRetencion = new ConceptoImpuestosRetencionesRetencion();
								$arrVarIRRetencion = get_class_vars(get_class($conceptoImpuestosRetencionesRetencion));
								foreach ($arrVarIRRetencion as $_key => $_value) {
									if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
										$conceptoImpuestosRetencionesRetencion->$_key = $valueRetencion->Retencion->$_key;
									}
								}
								$conceptoImpuestos->agregarRetenciones($conceptoImpuestosRetencionesRetencion);
							}
						}
					}

					if(isset($value->Concepto->Impuestos->Traslados)){

						$arrTraslados = $value->Concepto->Impuestos->Traslados;
						if(!is_array($arrTraslados)){
							$arrTraslados = array($arrTraslados);
						}

						foreach ($arrTraslados as $keyTraslado => $valueTraslado) {
							if(isset($valueTraslado->Traslado)){
								$conceptoImpuestosTrasladosTraslado = new ConceptoImpuestosTrasladosTraslado();
								$arrVarITTraslado = get_class_vars(get_class($conceptoImpuestosTrasladosTraslado));
								foreach ($arrVarITTraslado as $_key => $_value) {
									if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
										$conceptoImpuestosTrasladosTraslado->$_key = $valueTraslado->Traslado->$_key;
									}
								}
								$conceptoImpuestos->agregarTraslados($conceptoImpuestosTrasladosTraslado);
							}
						}
					}
					// if(isset($value->Concepto->Impuestos->Traslados->Traslado)){

					// 	$conceptoImpuestosTrasladosTraslado = new ConceptoImpuestosTrasladosTraslado();
					// 	$arrVarITTraslado = get_class_vars(get_class($conceptoImpuestosTrasladosTraslado));
					// 	foreach ($arrVarITTraslado as $_key => $_value) {
					// 		if(!is_array($concepto->$_key)){/*si el atributo no es un array*/
					// 			$conceptoImpuestosTrasladosTraslado->$_key = $value->Concepto->Impuestos->Traslados->Traslado->$_key;
					// 		}
					// 	}
					// 	$conceptoImpuestos->agregarTraslados($conceptoImpuestosTrasladosTraslado);
					// }

					$concepto->agregarImpuestos($conceptoImpuestos);
				}				
				$conceptos->agregarConcepto($concepto);
			}
			/*Termina recorrido de lista de conceptos**************************************/
		}

		$cfdi33->creaConceptos($conceptos);

		$impuesto = new Impuestos();
		$impuesto->procesaConceptoImpuestos($conceptos->arrConceptoImpuestos);
		$cfdi33->creaImpuestos($impuesto);

		$domXML = $cfdi33->getXML();

		$cadenaOriginal = $pem->getCadenaOriginal($domXML);

		$Sello = $pem->getSello($cadenaOriginal);

		if(!is_bool($Sello)){
			$cfdi33->comprobante->setAttribute('Sello',$Sello);
			if($creaZip){
				$rutaZip = $cfdi33->guardarZip();
			}else{
				$rutaZip = $cfdi33->guardar($nombre_archivo);
			}
			if(!is_bool($rutaZip)){
				return $rutaZip;
			}
		}

		return false;
	}

	public static function generarPFX($emisorRfc = '', $passPFX = ''){

		$rutaCertificado = RUTA_CERTIFICADO."/$emisorRfc/certificado.cer";
		$rutaLlave = RUTA_CERTIFICADO."/$emisorRfc/llave.key";
		$rutaPass = RUTA_CERTIFICADO."/$emisorRfc/pass.txt";
		
		$pem = new PEM();
		$pem->getPemCertificado($rutaCertificado);
		$pem->getPemLlave($rutaLlave);
		$pem->setPassword($rutaPass,true);

		return $pem->getCertificadoPFX($passPFX);
	}

	public static function getPassword($emisorRfc = ''){

		$rutaPass = RUTA_CERTIFICADO."/$emisorRfc/pass.txt";
		
		$pem = new PEM();
		$pem->setPassword($rutaPass,true);
		$passPFX = $pem->password;
		return $passPFX;
	}

	public static function cargarSolicitud($servicio_consumido="", $timbres_usados = 0,$codigo_respuesta="",$respuesta="",$respuesta_excepcion = "")
	{
		// try{

		$usuario_nombre = $GLOBALS['WSUsuario'];
		$fk_cat_cliente = $GLOBALS['fk_cat_cliente'];

		$solicitud_host = $_SERVER["REMOTE_ADDR"];  /*Imprime la dirección IP del cliente*/
		$solicitud_puerto = $_SERVER["REMOTE_PORT"]; /*Imprime puerto empleado por la máquina del usuario */
		$solicitud_tiempo = $_SERVER['REQUEST_TIME']; /*Imprime el tiempo de respuesta*/
		$solicitud_agente = $_SERVER['HTTP_USER_AGENT']; /*Imprime la información de S.O y navegador del cliente*/
		$solicitud_metodo = $_SERVER['REQUEST_METHOD']; /*Imprime el método de petición empleado*/

		$conexion = new ConexionDB();
		$sql = "INSERT INTO ".control_solicitud_cfdi." SET "
		."usuario = :usuario"
		.", fk_cat_cliente = :fk_cat_cliente"
		.", servicio_consumido = :servicio_consumido"
		.", timbres_usados = :timbres_usados"
		.", codigo_respuesta = :codigo_respuesta"
		.", respuesta = :respuesta"
		.", respuesta_excepcion = :respuesta_excepcion"
		.", solicitud_host = :solicitud_host"
		.", solicitud_puerto = :solicitud_puerto"
		.", solicitud_fecha_hora = :solicitud_fecha_hora"
		.", solicitud_tiempo = :solicitud_tiempo"
		.", solicitud_agente = :solicitud_agente"
		.", solicitud_metodo = :solicitud_metodo";

		$solicitud_datetime = date("Y-m-d H:i:s", $solicitud_tiempo);
		$stmt = $conexion->queryPrepare($sql);
		// $stmt->bindParam(':nombre_tabla',"".control_solicitud_cfdi);
		$stmt->bindParam(':usuario'              , $usuario_nombre      );
		$stmt->bindParam(':fk_cat_cliente'       , $fk_cat_cliente      );
		$stmt->bindParam(':servicio_consumido'   , $servicio_consumido  );
		$stmt->bindParam(':timbres_usados'       , $timbres_usados      );
		$stmt->bindParam(':codigo_respuesta'     , $codigo_respuesta    );
		$stmt->bindParam(':respuesta'            , $respuesta           );
		$stmt->bindParam(':respuesta_excepcion'  , $respuesta_excepcion );
		$stmt->bindParam(':solicitud_host'       , $solicitud_host      );
		$stmt->bindParam(':solicitud_puerto'     , $solicitud_puerto    );
		$stmt->bindParam(':solicitud_fecha_hora' , $solicitud_datetime  );
		$stmt->bindParam(':solicitud_tiempo'     , $solicitud_tiempo    );
		$stmt->bindParam(':solicitud_agente'     , $solicitud_agente    );
		$stmt->bindParam(':solicitud_metodo'     , $solicitud_metodo    );
		$stmt->execute();
		// }catch(DBException $ex){
		// 	DBException::$funcion_nombre = __FUNCTION__;
		// 	$ex->salidaError();/*Lanza Respuesta y termina el proceso*/
		// }
	}

	public static function validarTimbresDisponibles()
	{
		$conexion = new ConexionDB();
		/*Consulta para obtener los timbres Comprados*/
		$sql = "SELECT IFNULL(SUM(cantidad),0) timbres_comprados FROM ".control_timbres." WHERE fk_cat_cliente = :fk_cat_cliente AND estatus = 'A'";
		$stmt = $conexion->queryPrepare($sql);
		
		$stmt->bindParam(':fk_cat_cliente',$GLOBALS['fk_cat_cliente']);
		$stmt->execute();

		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		$timbresComprados = $datos[0]['timbres_comprados'];
		$timbresDisponibles = $timbresComprados;
		if($timbresDisponibles > 0){
			/*Consulta para obtener los timbres Transferidos*/
			$sql = "SELECT IFNULL(SUM(cantidad),0) timbres_transferidos FROM ".control_timbres." WHERE fk_cat_cliente_transfiere = :fk_cat_cliente AND estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->bindParam(':fk_cat_cliente',$GLOBALS['fk_cat_cliente']);
			$stmt->execute();

			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC); 
			$timbresTransferidos = $datos[0]['timbres_transferidos'];

			/*Descontamos los timbres transferidos*/
			$timbresDisponibles -= $timbresTransferidos;
			if($timbresDisponibles > 0){

				/*Consulta para obtener los timbres usados*/
				$sql = "SELECT IFNULL(SUM(timbres_usados),0) timbres_usados FROM ".control_solicitud_cfdi." WHERE fk_cat_cliente = :fk_cat_cliente AND estatus = 'A' ";
				$stmt = $conexion->queryPrepare($sql);

				$stmt->bindParam(':fk_cat_cliente',$GLOBALS['fk_cat_cliente']);

				$stmt->execute();
				$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$timbresUsados = $datos[0]['timbres_usados'];
				$timbresDisponibles -= $timbresUsados;
			}
			//validacion para pruebas
			// if($timbresTransferidos <= 0){
			// 	CFDIException::$codigo_estatus = 409;
			// 	$fk_cliente = $GLOBALS['fk_cat_cliente'];
			// 	throw new CFDIException("Usuario No Transferido Timbres \ntransferidos:{$timbresTransferidos} \ncomprados: {$timbresComprados} \nusados: {$timbresUsados} ".DB_NAME." ".MODO_PRUEBA." ".$_SERVER["HTTP_HOST"]);
			// }
			if($timbresDisponibles <= 0){
				CFDIException::$codigo_estatus = 409;
				throw new CFDIException("Usuario No Cuenta Con Timbres Disponibles");
			}

		}else{
			CFDIException::$codigo_estatus = 409;
			throw new CFDIException("Usuario No Ha Realizado la Compra de Timbres ");
		}

	}

}
?>