<?php
// declare(strict_types=1);
namespace WebServices\portal;

use Psr\Container\ContainerInterface;
use Slim\Psr7\Factory\StreamFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use WebServices\portal\ServicesException;
use WebServices\portal\Funciones;
use WebServices\portal\ConexionDB;
use WebServices\portal\Encrypter;
// use WebServices\portal\Configuracion;
use SoapClient;
use PDO;
use stdClass;
use DateTime;
// include '../cfdiSAT/Configuracion.php';

class WebServices{
	private $container;

   // constructor receives container instance
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	private function getParametros(){
		
		$json = file_get_contents('php://input');
		$params = json_decode($json);
		
		return $params;
	}

	private function validaParametrosRecibidos($arrParametros=array())
	{
		// extract($this->getParametros());//extraemos las variables post
		$parametros = $this->getParametros();//extraemos las variables post
		$parametrosNoRecibidos = "";

		foreach ($arrParametros as $key => $value) {
			if(is_null($parametros) || !isset($parametros->$value)){/*si la variable existe*/
				$parametrosNoRecibidos .= empty($parametrosNoRecibidos) ? "" : ",";
				$parametrosNoRecibidos .= $value;
			}
		}
		if(!empty($parametrosNoRecibidos)){
			$parametrosNoRecibidos = "Parametros No Recibidos [$parametrosNoRecibidos]";
		}
		return $parametrosNoRecibidos;
	}
	
/**
	*Obtener el JSON de las plataformas registradas
	*/
	public function login(Request $request, Response $response, array $args): Response
	{
		try{
			$parametros = $this->getParametros();
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$campos = "pk_usuario,usuario_nombre,usuario_clave,nombre,apellido_paterno,apellido_materno,fk_cliente";
			$sql = "SELECT {$campos} FROM ".cat_usuario." WHERE usuario_nombre = '$parametros->usuario_nombre' ";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos)>0){
				$claveRecibida = Encrypter::cifrado($parametros->usuario_clave);
				if($datos[0]['usuario_clave']==$claveRecibida){

					$Usuario = new stdClass;
					$Usuario->pk_usuario = Encrypter::cifrado($datos[0]['pk_usuario']);
					$Usuario->usuario_nombre = ($datos[0]['usuario_nombre']);
					$Usuario->usuario_clave = ($datos[0]['usuario_clave']);
					$Usuario->nombre = ($datos[0]['nombre']);
					$Usuario->apellido_paterno = ($datos[0]['apellido_paterno']);
					$Usuario->apellido_materno = ($datos[0]['apellido_materno']);
					$Usuario->fk_cliente = Encrypter::cifrado($datos[0]['fk_cliente']);
					$respuesta    = $Usuario;
					$estatus = 200;
				}else{
					$respuesta["error"]   = true;
					$respuesta["message"] = "Usuario y/o Contraseña Incorrecta"; 
					$respuesta["body"]    = "";
					$estatus = 300;
				}
			}else{
				$respuesta["error"]   = true;
				$respuesta["body"]    = "";
				$respuesta["message"] = "Usuario No Válido"; 
				$estatus = 300;
			}
			$response = $response->withStatus($estatus);

			$error = null;

			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			
			$respuestaMYSQL = json_encode($respuesta);
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}


	/**
	*Registrar las publicaciones 
	*/
	public function publicacionRegistro(Request $request, Response $response, array $args): Response
	{
		try{

			$arrParametros = array('publicaciones');
			$validacion = $this->validaParametrosRecibidos($arrParametros);
			if(!empty($validacion)){
				ServicesException::$codigo_estatus = 400;
				throw new ServicesException($validacion);
			}
			/*Consulta para obtener los timbres usados*/

			$parametros = $this->getParametros();
			
			$sizeof = sizeof($parametros->publicaciones);

			$estatus = 200;
			$respuesta = array();
			$respuesta["error"]   = false;
			$respuesta["message"] = "";
			$respuesta["body"] = "";

			if($sizeof>0){
				$fecha_venta = new DateTime( $parametros->fecha_venta ); 
				$fecha_ultimo_dia_mes = $fecha_venta->format( 'Y-m-t' );


				// $conexion = new ConexionDB();
				foreach ($parametros->publicaciones as $key => $publicacion) {
					if($publicacion->fecha_publicacion==''){
						$publicacion->fecha_publicacion = '1900-01-01';
					}
					$cadenaInsert .= empty($cadenaInsert) ? "INSERT INTO ".cat_publicacion." (fecha_publicacion,plataforma_acronimo,titulo,numero_identificador,unidad,sat_clave_unidad,sat_clave_producto_servicio,precio_unitario,importe,link) VALUES ":",";
					$cadenaInsert .= "("				
					."'$publicacion->fecha_publicacion','$parametros->plataforma','$parametros->titulo',"
					."'$publicacion->identificador','$publicacion->unidad','$publicacion->sat_clave_unidad',"
					."'$publicacion->sat_claveprodserv','$publicacion->precio_unitario','$publicacion->importe','$publicacion->link'"
					.")";
				}
				
				$stmt = $conexion->queryPrepare($cadenaInsert);

				$stmt->execute();


				if($sizeof==1){
					$respuesta["body"] = 'Publicacion Registrada';
				}else{
					$respuesta["body"] = 'Publicaciones Registradas';
				}
			}else{
				$estatus = 300;
				$respuesta["message"] = 'La Lista Recibida esta Vacia';
			}


			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	* Obtener el registro de ventas del folio proporcionado para su facturacion posterior
	*/
	public function VentaConsulta(Request $request, Response $response, array $args): Response
	{
		// try{
			// $body = $request->getBody();
		$parametros = $this->getParametros();
		if(!isset($parametros->folio)){
			$validacion = "Parametro Folio No Recibido";
		}
		if(!isset($parametros->importe_total)){
			$validacion .= (empty($validacion) ? "Parametro":" e") . " Importe Total No Recibido";
		}
		if(!isset($parametros->rfc)){
			$validacion .= (empty($validacion) ? "Parametro":" y") . " RFC No Recibido";
		}

		if(!empty($validacion)){
			ServicesException::$codigo_estatus = 400;
			throw new ServicesException($validacion);
				// return $response;
		}
		/*Consulta para obtener los timbres usados*/
		$conexion = new ConexionDB();
		$sql = "SELECT * FROM ".movimiento_venta." MV WHERE estatus = 'A' AND estatus_venta = 'A' AND folio_venta = '{$parametros->folio}'";
		$stmt = $conexion->queryPrepare($sql);
		$stmt->execute();
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if(sizeof($datos) > 0){
			$total_venta = 0;
			$Conceptos = array();
			foreach ($datos as $key => $venta) {
				$importe_iva = "";
				if($venta['incluye_iva']=="S"){
					$importe_total = $venta['importe_venta'];
					$tasa_iva = $venta['aplica_iva']=="S" ? $venta['tasa_iva']/100 : 0;
					$venta['importe_venta'] = round($venta['importe_venta'] / (1 + ($tasa_iva)),2);
					$importe_iva = round($importe_total-$venta['importe_venta'],2);
				}else{
					$tasa_iva = $venta['aplica_iva']=="S" ? $venta['tasa_iva']/100 : 0;
					$importe_total = round($venta['importe_venta'] * (1 + ($tasa_iva)),2);
					$importe_iva = round($importe_total-$venta['importe_venta'],2);
				}
				$total_venta = round($total_venta + $importe_total,2);

				$Concepto = new stdClass;
				$Concepto->Cantidad = $venta['cantidad'];
				$Concepto->Descripcion = $venta['concepto'];
				$Concepto->ClaveProdServ = $venta['sat_clave_producto_servicio'];
				$Concepto->NoIdentificacion = $venta['numero_identificador'];
				$Concepto->ClaveUnidad = $venta['sat_clave_unidad'];
				$Concepto->Unidad = $venta['unidad'];
				$Concepto->ValorUnitario = $venta['precio_unitario'];
				$Concepto->Importe = $venta['importe_venta'];
				$Concepto->Importe_Iva = $importe_iva;
				$Concepto->Importe_Total = $importe_total;
				$Concepto->Descuento = $venta['descuento'];
				$Conceptos[] = array("Concepto" => $Concepto);
				$estatus = 200;					
			}
			if($total_venta!=$parametros->importe_total){
				$estatus = 300;
				$mensaje = "No. de Ticket/Importe Total Incorrecto";
			}
		}else{
			$estatus = 300;
			$mensaje = "No. de Ticket/Importe Total Incorrecto";
		}
		if($estatus == 200){
			$cliente = null;
			$sql = "SELECT CC.razon_social,IFNULL(CCO.contacto,'') correo FROM ".cat_cliente." CC "
			."LEFT JOIN ".cat_contacto." CCO ON CCO.fk_cliente = CC.pk_cliente AND CCO.tipo_contacto = 'CORREO' "
			."WHERE estatus != 'B' AND rfc = '".$parametros->rfc."' ORDER BY CCO.fecha_sistema DESC LIMIT 1 ";
			$stmt = $conexion->queryPrepare($sql);
			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($datos) > 0){
				$cliente = new stdClass;
				$cliente->razon_social = $datos[0]['razon_social'];
				$cliente->correo = $datos[0]['correo'];
			}
			$respuesta = array();
			$respuesta['conceptos'] = $Conceptos;
			$respuesta['cliente'] = $cliente;
			$respuesta['sql'] = $sql;
		}else{
			$respuesta = array();
			$respuesta["error"]   = true;
			$respuesta["message"] = $mensaje;
			$respuesta["body"] = "";
		}

		$respuestaMYSQL = json_encode($respuesta);
		$error = null;
			// $config = new Configuracion();
		$funcion= __FUNCTION__;
		$timbres_usados = 0;
		Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

		$response = $response->withStatus($estatus);
		$response->getBody()->write(json_encode($respuesta));

		// }catch(ServicesException $ex){
		// 	ServicesException::$funcion_nombre = __FUNCTION__;
		// 	$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		// }catch(DBException $ex){
		// 	DBException::$funcion_nombre = __FUNCTION__;
		// 	$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		// }

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*Consulta de las ventas
	*/
	public function VentaConsultaFiltro(Request $request, Response $response, array $args): Response
	{
		try{
			$parametros = $this->getParametros();
			$validacion = "";
			if(!isset($parametros->tipo_filtro) || empty($parametros->tipo_filtro)){
				$validacion .= (empty($validacion) ? "":" y ") . "Tipo de Filtro es Obligatorio";
			}else{
				if($parametros->tipo_filtro=="Ticket"){
					if(!isset($parametros->folio) || empty($parametros->folio)){
						$validacion .= (empty($validacion) ? "":" y ") . "No. de Ticket No Recibido";
					}
				}else if($parametros->tipo_filtro=="RangoFecha"){
					if(!isset($parametros->fecha_inicial) || empty($parametros->fecha_inicial)){
						$validacion .= (empty($validacion) ? "":" y ") . "Fecha Inicial No Recibido";
					}
					if(!isset($parametros->fecha_final) || empty($parametros->fecha_final)){
						$validacion .= (empty($validacion) ? "":" y ") . "Fecha Final No Recibido";
					}
				}else{
					$validacion .= (empty($validacion) ? "":" y ") . "Tipo de Filtro No Válido";
				}
			}
			if(!empty($validacion)){
				ServicesException::$codigo_estatus = 400;
				throw new ServicesException($validacion);
			}


			$condicion = "";

			if($parametros->tipo_filtro=="Ticket"){
				$condicion .= " AND folio_venta = '{$parametros->folio}'";
			}
			if($parametros->tipo_filtro=="RangoFecha"){
				$condicion .= " AND fecha_venta BETWEEN '{$parametros->fecha_inicial}' AND '{$parametros->fecha_final}'";
			}

			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT * FROM ".movimiento_venta." MV WHERE estatus = 'A' AND estatus_venta = 'A' {$condicion}";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if(sizeof($datos) >= 0){
				$importe_total = 0;
				$ventas = array();
				foreach ($datos as $key => $venta) {
					$importe_iva = "";
					if($venta['aplica_iva']=="S"){
						$importe_total = $venta['importe_venta'];
						$tasa_iva = $venta['aplica_iva']=="S" ? $venta['tasa_iva']/100 : 0;
						$venta['importe_venta'] = round($venta['importe_venta'] / (1 + ($tasa_iva)),2);
						$importe_iva = round($importe_total-$venta['importe_venta'],2);
					}else{
						$tasa_iva = 0;
						$importe_total = $venta['importe_venta'];
						$importe_iva = round($importe_total-$venta['importe_venta'],2);
					}
					// $total_venta = round($total_venta + $importe_total,2);


					// $importe_total = round($importe_total + $venta['importe_venta'],2);
					// $importe_venta = $venta['importe_venta'] * ($venta['aplica_iva']=='S' ? (1+($venta['tasa_iva']/100)) : 1);
					// $importe_iva = $venta['aplica_iva']=='S' ? $importe_venta - $venta['importe_venta'] : "";
					$Concepto = new stdClass;
					$Concepto->id_venta = Encrypter::cifrado($venta['pk_venta']);
					$Concepto->fecha_venta = $venta['fecha_venta'];
					$Concepto->folio_venta = $venta['folio_venta'];
					$Concepto->cantidad = $venta['cantidad'];
					$Concepto->concepto = $venta['concepto'];
					$Concepto->sat_clave_producto_servicio = $venta['sat_clave_producto_servicio'];
					$Concepto->numero_identificador = $venta['numero_identificador'];
					$Concepto->sat_clave_unidad = $venta['sat_clave_unidad'];
					$Concepto->unidad = $venta['unidad'];
					$Concepto->precio_unitario = $venta['precio_unitario'];
					$Concepto->importe_venta = $venta['importe_venta'];
					$Concepto->aplica_iva = $venta['aplica_iva'];
					$Concepto->tasa_iva = $venta['tasa_iva'];
					$Concepto->importe_iva = $importe_iva;
					$Concepto->descuento = $venta['descuento'];
					$Concepto->importe_total = $importe_total;
					// $ventas[] = array("concepto" => $Concepto);
					$ventas[] = $Concepto;
				}
				$respuesta = $ventas;
				$estatus = 200;
			}else{
				$estatus = 300;
				$respuesta = array();
				$respuesta["error"]   = true;
				$respuesta["message"] = "Problemas al Realizar la Consulta";
				$respuesta["body"] = "";
			}

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response = $response->withStatus($estatus);
			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*Consulta de las ventas
	*/
	public function VentaEliminacion(Request $request, Response $response, array $args): Response
	{
		try{
			if(!isset($args['folio_venta'])){
				ServicesException::$codigo_estatus = 400;
				throw new ServicesException("Folio No Recibido");
			}
			// $folio_venta = Encrypter::decifrado($args['folio_venta']);
			$folio_venta = $args['folio_venta'];
			if(empty($folio_venta)){
				ServicesException::$codigo_estatus = 400;
				throw new ServicesException("Folio No Válido");
			}
			$condicion = " AND folio_venta = '{$folio_venta}'";
			$parametros = $this->getParametros();
			if(isset($parametros->id_venta)){
				$pk_venta = Encrypter::decifrado($parametros->id_venta);
				if(empty($pk_venta)){
					ServicesException::$codigo_estatus = 400;
					throw new ServicesException("ID del Concepto No Válido");
				}
				$condicion .= " AND pk_venta = '{$pk_venta}'";
			}



			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "UPDATE ".movimiento_venta." SET estatus = 'C' , estatus_venta = 'C' WHERE estatus = 'A' AND estatus_venta = 'A' {$condicion}";
			$stmt = $conexion->queryPrepare($sql);
			$stmt->execute();

			$registros_afectados = $stmt->rowCount();

			if($registros_afectados > 0){

				$respuesta = array();
				$respuesta["message"] = "";
				$respuesta["body"] = "Registro Eliminado";
				$estatus = 200;
			}else{
				$estatus = 300;
				$respuesta = array();
				$respuesta["error"]   = true;
				$respuesta["message"] = "Sin Registros Afectados";
				$respuesta["body"] = $parametros;
			}

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response = $response->withStatus($estatus);
			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
	}

	/**
	*Registrar las ventas y generar folio de ticket
	*/
	public function VentaRegistro(Request $request, Response $response, array $args): Response
	{
		try{

			$arrParametros = array('fecha_venta','tipo_venta','forma_pago','ventas');
			$validacion = $this->validaParametrosRecibidos($arrParametros);
			if(!empty($validacion)){
				ServicesException::$codigo_estatus = 400;
				throw new ServicesException($validacion);
			}
			/*Consulta para obtener los timbres usados*/

			$parametros = $this->getParametros();
			$fecha_venta = new DateTime( $parametros->fecha_venta ); 
			$fecha_ultimo_dia_mes = $fecha_venta->format( 'Y-m-t' );

			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT IFNULL(MAX(CAST(SUBSTR(folio_venta,-4) AS UNSIGNED)),0)+1 folio_consecutivo,count(*) FROM ".movimiento_venta." WHERE fecha_venta BETWEEN '$parametros->fecha_venta' AND '$fecha_ultimo_dia_mes' ";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$folio_consecutivo = str_pad($datos[0]['folio_consecutivo'], 4, "0", STR_PAD_LEFT);

			// $cadenaFolio = $this->generaAleatoria();
			$cadenaFolio = $parametros->tipo_venta.$fecha_venta->format( 'Ym' ).$folio_consecutivo;
			$cadenaInsert = "";

			// $conexion = new ConexionDB();
			foreach ($parametros->ventas as $key => $venta) {
				$cadenaInsert .= empty($cadenaInsert) ? "INSERT INTO ".movimiento_venta." (folio_venta,tipo_venta,forma_pago,fecha_venta,fecha_vencimiento,cantidad,concepto,numero_identificador,unidad,sat_clave_unidad,sat_clave_producto_servicio,precio_unitario,descuento,importe_venta) VALUES ":",";
				$cadenaInsert .= "("				
				."'$cadenaFolio','$parametros->tipo_venta','$parametros->forma_pago',"
				."'$parametros->fecha_venta','$fecha_ultimo_dia_mes','$venta->cantidad','$venta->concepto','$venta->identificador',"
				."'$venta->unidad','$venta->sat_clave_unidad','$venta->sat_claveprodserv',"
				."'$venta->precio_unitario','$venta->descuento','$venta->importe'"
				.")";
			}
			
			$stmt = $conexion->queryPrepare($cadenaInsert);

			$stmt->execute();

			$encontrado = array("folio_venta"=>$cadenaFolio);

			$respuesta = array();
			$respuesta["error"]   = false;
			$respuesta["message"] = "";
			$respuesta["body"] = $encontrado;

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, 200, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*Timbrar las ventas, generar la factura y actualizar la venta con la informacion de la factura
	*/
	public function VentaFacturar(Request $request, Response $response, array $args): Response
	{
		// try{
		// $body = $request->getBody();
		$parametros = $this->getParametros();
		$validacion = "";
		if(!isset($parametros->uso_cfdi)){
			$validacion = "Parametro UsoCFDI No Recibido";
		}
		if(!isset($parametros->xxxx)){
			$validacion = "Parametro xxxxx No Recibido";
		}

		if(!empty($validacion)){
			ServicesException::$codigo_estatus = 400;
			throw new ServicesException($validacion);
		}

		$this->VentaConsulta($request, $response, $args);
		if($response->getStatusCode() != 200){ /*si hubo algun error o excepcion*/
			return $response;
		}else{

			$estatus = 300;
			$respuesta = array();

			$respuestaJSON = json_decode($response->getBody());

			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT rfc,razon_social,regimen_fiscal,codigo_postal FROM ".informacion_empresa." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if(sizeof($datos)>0){
				$emisorRfc = $datos[0]['rfc'];
				$emisorNombre = $datos[0]['razon_social'];
				$emisorRegimenFiscal = $datos[0]['regimen_fiscal'];
				$emisorCodigoPostal = $datos[0]['codigo_postal'];
				$sub_total = 0;
				$importe_mayor = 0;
				$forma_pago = '';
				foreach ($respuestaJSON->conceptos as $concepto) {
					$sub_total += $concepto->Importe;
					if($importe_mayor < $concepto->Importe){

					}
				}
					/*
					"FormaPago": "30",
					"SubTotal": "100.00",
					"Fecha": "2020-08-08T11:25:00",
					*/
					$comprobante = new stdClass;
					$comprobante->Moneda = 'MXN';
					$comprobante->TipoDeComprobante = 'I';
					$comprobante->Folio = $parametros->folio;
					$comprobante->FormaPago = $forma_pago;
					$comprobante->SubTotal = $sub_total;
					$comprobante->MetodoPago = 'PUE';
					$comprobante->LugarExpedicion = $emisorCodigoPostal;
					// $comprobante->Fecha = 'I';
					$comprobante->Total = $parametros->importe_total;

					$comprobante->Emisor = new stdClass;
					$comprobante->Emisor->Rfc = $emisorRfc;
					$comprobante->Emisor->Nombre = $emisorNombre;
					$comprobante->Emisor->RegimenFiscal = $emisorRegimenFiscal;

					$comprobante->Receptor = new stdClass;
					$comprobante->Receptor->Rfc = $parametros->rfc;
					$comprobante->Receptor->Nombre = $parametros->razon_social;
					$comprobante->Receptor->UsoCFDI = $parametros->uso_cfdi;

					$comprobante->Conceptos = $respuestaJSON->conceptos;

					$cfdi = new stdClass;
					$cfdi->Comprobante = $cfdi;
					$dataArray = array( 'xmlcfdi' => json_encode($cfdi));
					$dataJSON = json_encode($dataArray);

					$ch = curl_init();
					if ($ch) {
						$solicitud_agente = 'PortalFacturacion/1.0 '.$_SERVER['HTTP_USER_AGENT']; /*Imprime la información de S.O y navegador del cliente*/

						$headers = array(
							"Content-type: application/json;charset=\"utf-8\"",
							"Accept: *",
							"Cache-Control: no-cache",
						);
						$webServicesPortal = 'https://portal.tudescargamasiva.com/ws/WebServicesFacturacion/public';
						$webServicesPortal = 'https://ws.portal.tudescargamasiva.com';
						$webServicesPortal = 'https://ws.portal.tudescargamasiva.com';
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
  						// curl_setopt($ch, CURLOPT_URL, 'https://portal.tudescargamasiva.com/ws/WebServicesFacturacion/public/users');
  						// curl_setopt($ch, CURLOPT_URL, $webServicesPortal.'/venta/consulta/filtro');
						curl_setopt($ch, CURLOPT_URL, $webServicesPortal.'/CFDITimbrar');
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
						curl_setopt($ch, CURLOPT_TIMEOUT, 10);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
						curl_setopt($ch, CURLOPT_TIMEOUT_MS, 50000);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJSON);
						curl_setopt($ch, CURLOPT_USERAGENT, $solicitud_agente);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

						$curl_respuesta = curl_exec($ch);
						$error          = curl_error($ch);
						$errno          = curl_errno($ch);
						$codeEstatus    = curl_getinfo($ch, CURLINFO_HTTP_CODE);

						curl_close($ch);
					}else{
						$error = true;
					}

  					// echo "estatus code: {$codeEstatus}<br>";

					if ($error) {
						$respuesta["error"]   = true;
						$respuesta["message"] = "No fue Posible Procesar la Solicitud de Timbrado";
						$respuesta["body"] = "";
					}else{
						if($codeEstatus>=200 && $codeEstatus <= 205){

							$estatus = 200;

							$respuesta["error"]   = false;
							$respuesta["message"] = "";
							$respuesta["body"] = $curl_respuesta;

						}else{
							$estatus = $codeEstatus;
						// $respuesta = $curl_respuesta;
							$respuesta = json_decode($curl_respuesta);
						}
					}
				}else{
					$respuesta["error"]   = true;
					$respuesta["message"] = "Problemas al Realizar la Consulta de Validación";
					$respuesta["body"] = "";
				}

				//ejemplo de respuesta
				// "{"conceptos":[{"Concepto":{"Cantidad":"1","Descripcion":"PRUEBA","ClaveProdServ":"84101604","NoIdentificacion":"01","ClaveUnidad":"E48","Unidad":"E48","ValorUnitario":"15000.00","Importe":"15000.00","Importe_Iva":2400,"Importe_Total":17400,"Descuento":"0.00"}}],"cliente":{"razon_social":"GABRIEL OROZCO RUIZ","correo":"gabo.itc@gmail.com"},"sql":"SELECT CC.razon_social,IFNULL(CCO.contacto,'') correo FROM cat_cliente CC LEFT JOIN cat_contacto CCO ON CCO.fk_cliente = CC.pk_cliente AND CCO.tipo_contacto = 'CORREO' WHERE estatus != 'B' AND rfc = 'OORG911226DY9' ORDER BY CCO.fecha_sistema DESC LIMIT 1 "}[{"Concepto":{"Cantidad":"1","Descripcion":"PRUEBA","ClaveProdServ":"84101604","NoIdentificacion":"01","ClaveUnidad":"E48","Unidad":"E48","ValorUnitario":"15000.00","Importe":"15000.00","Importe_Iva":2400,"Importe_Total":17400,"Descuento":"0.00"}}]"
				// falta el llenado del JSON para consumir el WS de facturacion

			$response = $response->withBody( (new StreamFactory())->createStream() );//reseteamos el body
			$response->getBody()->write(json_encode($respuesta));
			// $response->getBody()->write(json_encode($respuesta));

			return $response->withStatus($estatus)->withHeader('Content-Type', 'application/json');
		}
	// }catch(ServicesException $ex){
	// 	ServicesException::$funcion_nombre = __FUNCTION__;
	// 	$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
	// }catch(DBException $ex){
	// 	DBException::$funcion_nombre = __FUNCTION__;
	// 	$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
	// }
	}

	/**
	*Obtener el JSON de las plataformas registradas
	*/
	public function CatalogoTipoVenta(Request $request, Response $response, array $args): Response
	{
		try{
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT * FROM ".cat_tipo_venta." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos)>0){
				$respuesta   = $datos ;
				$estatus = 200;
			}else{
				$respuesta["error"]   = true;
				$respuesta["message"] = "Catálogo vacio"; 
				$estatus = 300;
			}
			$response = $response->withStatus($estatus);

			$error = null;
			$funcion= __FUNCTION__;
			$timbres_usados = 0;

			$respuestaMYSQL = json_encode($respuesta);
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*Obtener el JSON de las plataformas registradas
	*/
	public function CatalogoPlataformas(Request $request, Response $response, array $args): Response
	{
		try{
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT * FROM ".cat_plataforma." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos)>0){
				$respuesta   = $datos ;
				$estatus = 200;
			}else{
				$respuesta["error"]   = true;
				$respuesta["message"] = "Catálogo vacio"; 
				$estatus = 300;
			}
			$response = $response->withStatus($estatus);

			$error = null;
			$funcion= __FUNCTION__;
			$timbres_usados = 0;

			$respuestaMYSQL = json_encode($respuesta);
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*Consulta de las ventas
	*/
	public function FacturacionConsultaFiltro(Request $request, Response $response, array $args): Response
	{
		try{
			$parametros = $this->getParametros();
			$validacion = "";
			if(!isset($parametros->tipo_filtro) || empty($parametros->tipo_filtro)){
				// $validacion .= (empty($validacion) ? "":" y ") . "Tipo de Filtro es Obligatorio";
			}else{
				if($parametros->tipo_filtro=="SerieFolio"){
					if(!isset($parametros->folio) || empty($parametros->folio)){
						$validacion .= (empty($validacion) ? "":" y ") . "Serie/Folio No Recibido";
					}
				}else if($parametros->tipo_filtro=="RangoFecha"){
					if(!isset($parametros->fecha_inicial) || empty($parametros->fecha_inicial)){
						$validacion .= (empty($validacion) ? "":" y ") . "Fecha Inicial No Recibido";
					}
					if(!isset($parametros->fecha_final) || empty($parametros->fecha_final)){
						$validacion .= (empty($validacion) ? "":" y ") . "Fecha Final No Recibido";
					}
				}else if($parametros->tipo_filtro=="PorMes"){
					if(!isset($parametros->ejercicio) || empty($parametros->ejercicio)){
						$validacion .= (empty($validacion) ? "":" y ") . "Año de Ejercicio No Recibido";
					}
					if(!isset($parametros->mes) || empty($parametros->mes)){
						$validacion .= (empty($validacion) ? "":" y ") . "Mes No Recibido";
					}
				}else if($parametros->tipo_filtro=="Ultimos10"){
					
				}else{
					$validacion .= (empty($validacion) ? "":" y ") . "Tipo de Filtro No Válido";
				}
			}
			if(!empty($validacion)){
				ServicesException::$codigo_estatus = 400;
				throw new ServicesException($validacion);
			}


			$condicion = "";

			if($parametros->tipo_filtro=="Ticket"){
				$condicion .= " AND folio_venta = '{$parametros->folio}'";
			}
			if($parametros->tipo_filtro=="RangoFecha"){
				$condicion .= " AND fecha_venta BETWEEN '{$parametros->fecha_inicial}' AND '{$parametros->fecha_final}'";
			}
			if($parametros->tipo_filtro=="PorMes"){
				$condicion .= " AND YEAR(fecha_venta) = '{$parametros->ejercicio}' AND MONTH(fecha_venta) = '{$parametros->mes}'";
			}
			if($parametros->tipo_filtro=="Ultimos10"){
				$condicion .= " LIMIT 0,10 ";
			}

			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT * FROM ".facturas." MV WHERE estatus = 'A' {$condicion}";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if(sizeof($datos) >= 0){
				$importe_total = 0;
				$facturas = array();
				foreach ($datos as $key => $factura) {
					$factura['pk_factura'] = Encrypter::cifrado($factura['pk_factura']);

					$facturas[] = $factura;
				}
				$respuesta = $facturas;
				$estatus = 200;
			}else{
				$estatus = 300;
				$respuesta = array();
				$respuesta["error"]   = true;
				$respuesta["message"] = "Problemas al Realizar la Consulta";
				$respuesta["body"] = "";
			}

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response = $response->withStatus($estatus);
			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}
	/**
	*Obtener el JSON de las plataformas registradas
	*/
	public function FacturarVenta123(Request $request, Response $response, array $args): Response
	{
		try{
			// $body = $request->getBody();
			$parametros = $this->getParametros();
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT * FROM" .movimiento_venta." MV WHERE estatus = 'A' AND estatus_venta = 'A' AND folio_venta = '{$parametros->folio}'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$Conceptos = array();
			if(sizeof($datos) > 0){

				$sat_forma_pago = "";
				$importe_mayor = 0;
				$importe_total = 0;
				$Conceptos["Conceptos"] = array();
				foreach ($datos as $key => $venta) {

					if($importe_mayor < $venta['importe_venta']){
						$importe_mayor = $venta['importe_venta']; 
						$sat_forma_pago = $venta['sat_forma_pago'];
					}

					$tasa_iva = $venta['tasa_iva']/100;

					$importe_total = round($importe_total + $venta['importe_venta'],2);
					$Concepto = new stdClass;
					$Concepto->Cantidad = $venta['cantidad'];
					$Concepto->Descripcion = $venta['concepto'];
					$Concepto->ClaveProdServ = $venta['sat_clave_producto_servicio'];
					$Concepto->NoIdentificacion = $venta['numero_identificador'];
					$Concepto->ClaveUnidad = $venta['sat_clave_unidad'];
					$Concepto->Unidad = $venta['unidad'];
					$Concepto->ValorUnitario = round($venta['precio_unitario']/(1+$tasa_iva),2);
					$Concepto->Importe = round($venta['importe_venta']/(1+$tasa_iva),2);


					$Traslado = new stdClass;
					$Traslado->Base = $Concepto->Importe;
					$Traslado->Impuesto = "002";
					$Traslado->TipoFactor = "Tasa";
					$Traslado->TasaOCuota = "0.160000";
					$Traslado->Importe = round($venta['importe_venta'] - $Concepto->Importe,2);

					$Traslados = new stdClass;
					$Traslados->Traslado = $Traslado;
					$Impuestos = new stdClass;
					$Impuestos->$Traslados = $Traslados;

					$Concepto->Impuestos = $Impuestos;
					$Conceptos["Conceptos"][] = array("Concepto" => $Concepto);
				}
					// $Concepto->Descuento = "";
				$Comprobante = new stdClass;
				$Comprobante->Folio = $parametros->folio;
				$Comprobante->Fecha = (new DateTime())->format('Y-m-d\TH:i:s');
				$Comprobante->FormaPago = $sat_forma_pago;
				$Comprobante->SubTotal = "";
				$Comprobante->Total = $importe_total;
				$Comprobante->TipoDeComprobante = "I";
				$Comprobante->MetodoPago = "PUE";
				$Comprobante->LugarExpedicion = "";

				$Emisor = new stdClass;
				$Emisor->Rfc = $parametros->rfc;
				$Emisor->Nombre = $parametros->nombre;
				$Emisor->RegimenFiscal = $parametros->usocfdi;

				$Comprobante->Emisor = $Emisor;
				
				$Receptor = new stdClass;
				$Receptor->Rfc = $parametros->rfc;
				$Receptor->Nombre = $parametros->nombre;
				$Receptor->UsoCFDI = $parametros->usocfdi;

				$Comprobante->Receptor = $Receptor;

				$Comprobante->Conceptos = $Conceptos;

			}



			$respuesta = array();
			$respuesta["error"]   = false;
			$respuesta["message"] = "";
			$respuesta["body"] = $Comprobante;

			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, 200, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	public function cadenaAleatoria(Request $request, Response $response, array $args): Response
	{
		$respuesta = array('cadena' => $this->generaAleatoria() );
		$response->getBody()->write(json_encode($respuesta));
		return $response->withHeader('Content-Type', 'application/json');
	}

	private function generaAleatoria($numeroLetrasAleatorias = 5): string
	{
		# Tips de Ejemplo como generar un número y letra y mostrarlo en php
		$DesdeLetra = "a";
		$HastaLetra = "z";
		$DesdeNumero = 1;
		$HastaNumero = 10000;
		$cadena = "";

		for ($i=1; $i <= $numeroLetrasAleatorias; $i++) { 
			$letraAleatoria = chr(rand(ord($DesdeLetra), ord($HastaLetra)));
			$cadena .= $letraAleatoria;
		}

		$numeroAleatorio = rand($DesdeNumero, $HastaNumero);

		return strtoupper($cadena);
	}

	//******************************************************************
	//*Catalogos del sat************************************************
	//******************************************************************


	/**
	*Obtener el JSON de las plataformas registradas
	*/
	public function CatalogoUnidades(Request $request, Response $response, array $args): Response
	{
		try{

			$ClaveUnidad = isset($args['ClaveUnidad']) ? $args['ClaveUnidad'] : null;
			$condicionUnidad = isset($ClaveUnidad) ? "AND clave_unidad = '{$ClaveUnidad}' ":"";
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$sql = "SELECT clave_unidad,descripcion FROM ".cat_sat_clave_unidad." WHERE estatus = 'A' {$condicionUnidad}";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos)>0){
				$respuesta   = isset($ClaveUnidad) ? $datos[0] :$datos ;
				$estatus = 200;
			}else{
				$respuesta["error"]   = true;
				$respuesta["message"] = "Clave Unidad no Encontrada"; 
				$respuesta["body"]    = "";
				$estatus = 300;
			}
			$response = $response->withStatus($estatus);

			$error = null;

			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			
			$respuestaMYSQL = json_encode($respuesta);
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*Obtener el JSON de las plataformas registradas
	*/
	public function CatalogoProductoServicio(Request $request, Response $response, array $args): Response
	{
		try{

			$ClaveProductoServicio = isset($args['ClaveProductoServicio']) ? $args['ClaveProductoServicio'] : null;
			$condicion = isset($ClaveProductoServicio) ? "AND clave_producto_servicio = '{$ClaveProductoServicio}' ":"";
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();
			$campos_consulta = "clave_producto_servicio,descripcion,palabras_similares,incluir_iva_trasladado,incluir_ieps_trasladado,estimulo_fiscal";
			$sql = "SELECT {$campos_consulta} FROM ".cat_sat_clave_produto_servicio." WHERE estatus = 'A' {$condicion}";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			if(sizeof($datos)>0){
				$respuesta = $datos;
				$estatus = 200;
			}else{
				$respuesta["error"]   = true;
				if(isset($ClaveProductoServicio)){
					$respuesta["message"] = "Clave Producto o Servicio no Encontrada"; 
				}else{
					$respuesta["message"] = "Catálogo Vacio"; 
				}
				$respuesta["body"]    = "";
				$estatus = 300;
			}
			$response = $response->withStatus($estatus);

			$error = null;

			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			$respuestaMYSQL = json_encode($respuesta);
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}

	/**
	*
	*Obtener el JSON de las Formas de Pago Registradas
	*/
	public function CatalogoFormaPago(Request $request, Response $response, array $args): Response
	{
		try{
			/*Consulta para obtener los timbres usados*/
			$conexion = new ConexionDB();

			$sql = "SELECT clave_forma_pago,descripcion FROM ".cat_sat_forma_pago." WHERE estatus = 'A'";
			$stmt = $conexion->queryPrepare($sql);

			$stmt->execute();
			$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$respuesta = array();
			$estatus = 0;
			if(sizeof($datos) > 0){
				$respuesta = $datos;
				$estatus = 200;
			}else{
				$respuesta["error"]   = true;
				$respuesta["message"] = "";
				$estatus = 300;
			}
			$response = $response->withStatus($estatus);


			$respuestaMYSQL = json_encode($respuesta);
			$error = null;
			// $config = new Configuracion();
			$funcion= __FUNCTION__;
			$timbres_usados = 0;
			Funciones::cargarSolicitud($funcion, $timbres_usados, $estatus, $respuestaMYSQL);

			$response->getBody()->write(json_encode($respuesta));

		}catch(ServicesException $ex){
			ServicesException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
			// $response->getBody()->write(WebServices::$hola);
		}catch(DBException $ex){
			DBException::$funcion_nombre = __FUNCTION__;
			$response = $ex->salidaError($response);/*Lanza Respuesta y termina el proceso*/
		}

		return $response->withHeader('Content-Type', 'application/json');
	}
}

?>