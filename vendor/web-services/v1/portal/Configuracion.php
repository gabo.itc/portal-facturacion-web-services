<?php

namespace WebServices\portal;
/**
 * 
 */
class Configuracion
{

    function __construct()
    {
        define('API_KEY',MD5(uniqid()));

        $host = $_SERVER["HTTP_HOST"];
        // $linkPrueba = strpos($host, 'demo')===0;
        $linkPrueba = false;

        define('MODO_PRUEBA', $linkPrueba);
        define('RUTA_LOCAL', getcwd());
        define('DB_HOST', 'tudescargamasiva.com');
        define('DB_PORT', '3306');
        define('DB_NAME_REAL', 'tudescar_portal');
        define('DB_NAME', 'tudescar_portal'.($linkPrueba===true ? "_pruebas":""));
        define('DB_USER', 'tudescar_portal');
        define('DB_PASS', '2021_portal');

        if(!defined('control_solicitud_cfdi')) define('control_solicitud_cfdi','control_solicitud_cfdi');
        if(!defined('cat_sat_clave_unidad')) define('cat_sat_clave_unidad','cat_sat_clave_unidad');
        if(!defined('cat_sat_clave_produto_servicio')) define('cat_sat_clave_produto_servicio','cat_sat_clave_produto_servicio');
        if(!defined('cat_sat_forma_pago')) define('cat_sat_forma_pago','cat_sat_forma_pago');
        if(!defined('cat_cliente')) define('cat_cliente','cat_cliente');
        if(!defined('cat_contacto')) define('cat_contacto','cat_contacto');
        if(!defined('cat_plataforma')) define('cat_plataforma','cat_plataforma');
        if(!defined('cat_tipo_venta')) define('cat_tipo_venta','cat_tipo_venta');
        if(!defined('cat_publicacion')) define('cat_publicacion','cat_publicacion');
        if(!defined('cat_tipo_producto')) define('cat_tipo_producto','cat_tipo_producto');
        if(!defined('cat_usuario')) define('cat_usuario','cat_usuario');
        if(!defined('facturas')) define('facturas','facturas');
        if(!defined('informacion_empresa')) define('informacion_empresa','informacion_empresa');
        if(!defined('movimiento_venta')) define('movimiento_venta','movimiento_venta');

    }
}
?>