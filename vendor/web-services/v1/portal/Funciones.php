<?php


namespace WebServices\portal;

use WebServices\portal\ConexionDB;
use stdClass;
use split;
use ErrorException;
use PDOStatement;
use PDO;
/**
 * 
 */
class Funciones 
{
	public static $msgExcepcion;
	public static $msgExcepcionFaltante;
	public static $msgExcepcionAdicional;
	
	function __construct()
	{
		# code...
	}

	public static function cargarSolicitud($servicio_consumido="", $timbres_usados = 0,$codigo_respuesta="",$respuesta="",$respuesta_excepcion = "")
	{
		// try{

		$usuario_nombre = $GLOBALS['WSUsuario'];
		$fk_cat_cliente = $GLOBALS['fk_cat_cliente'];

		$solicitud_host = $_SERVER["REMOTE_ADDR"];  /*Imprime la dirección IP del cliente*/
		$solicitud_puerto = $_SERVER["REMOTE_PORT"]; /*Imprime puerto empleado por la máquina del usuario */
		$solicitud_tiempo = $_SERVER['REQUEST_TIME']; /*Imprime el tiempo de respuesta*/
		$solicitud_agente = $_SERVER['HTTP_USER_AGENT']; /*Imprime la información de S.O y navegador del cliente*/
		$solicitud_metodo = $_SERVER['REQUEST_METHOD']; /*Imprime el método de petición empleado*/

		$conexion = new ConexionDB();
		$sql = "INSERT INTO ".control_solicitud_cfdi." SET "
		."usuario = :usuario"
		.", fk_cat_cliente = :fk_cat_cliente"
		.", servicio_consumido = :servicio_consumido"
		.", timbres_usados = :timbres_usados"
		.", codigo_respuesta = :codigo_respuesta"
		.", respuesta = :respuesta"
		.", respuesta_excepcion = :respuesta_excepcion"
		.", solicitud_host = :solicitud_host"
		.", solicitud_puerto = :solicitud_puerto"
		.", solicitud_fecha_hora = :solicitud_fecha_hora"
		.", solicitud_tiempo = :solicitud_tiempo"
		.", solicitud_agente = :solicitud_agente"
		.", solicitud_metodo = :solicitud_metodo";

		$solicitud_datetime = date("Y-m-d H:i:s", $solicitud_tiempo);
		$stmt = $conexion->queryPrepare($sql);
		// $stmt->bindParam(':nombre_tabla',"".control_solicitud_cfdi);
		$stmt->bindParam(':usuario'              , $usuario_nombre      );
		$stmt->bindParam(':fk_cat_cliente'       , $fk_cat_cliente      );
		$stmt->bindParam(':servicio_consumido'   , $servicio_consumido  );
		$stmt->bindParam(':timbres_usados'       , $timbres_usados      );
		$stmt->bindParam(':codigo_respuesta'     , $codigo_respuesta    );
		$stmt->bindParam(':respuesta'            , $respuesta           );
		$stmt->bindParam(':respuesta_excepcion'  , $respuesta_excepcion );
		$stmt->bindParam(':solicitud_host'       , $solicitud_host      );
		$stmt->bindParam(':solicitud_puerto'     , $solicitud_puerto    );
		$stmt->bindParam(':solicitud_fecha_hora' , $solicitud_datetime  );
		$stmt->bindParam(':solicitud_tiempo'     , $solicitud_tiempo    );
		$stmt->bindParam(':solicitud_agente'     , $solicitud_agente    );
		$stmt->bindParam(':solicitud_metodo'     , $solicitud_metodo    );
		$stmt->execute();
		// }catch(DBException $ex){
		// 	DBException::$funcion_nombre = __FUNCTION__;
		// 	$ex->salidaError();/*Lanza Respuesta y termina el proceso*/
		// }
	}

}
?>