<?php

namespace WebServices\portal;

class Encrypter {

//Debes cambiar esta cadena, debe ser larga y unica
//nadie mas debe conocerla
	private static $clave  = 'portal para el servicio de facturacion en la nube      ';

//Metodo de encriptación
	private static $method = 'aes-256-cbc';

// Puedes generar una diferente usando la funcion $getIV()
	private static $ivKey = "znNoPM/MlY9zcoO2aQSU2g==";

	public static function cifrado ($input) {
		$iv = base64_decode(self::$ivKey);
		$output = openssl_encrypt ($input, self::$method, self::$clave, false, $iv);
		          // openssl_encrypt($data, "aes-128-cbc", AesCipher::fixKey($key), OPENSSL_RAW_DATA, $iv)
		return $output;
	}

	public static function decifrado ($input) {
		$iv = base64_decode(self::$ivKey);
		$output = openssl_decrypt($input, self::$method, self::$clave, false, $iv);;
		return $output;
	}

	public static function getIV () {
		return base64_encode(openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->method)));
	}

}