<?php
declare(strict_types=1);

use WebServices\sifei\WebServices as sifei;
use WebServices\portal\WebServices as portal;
use App\Application\Actions\Plataforma\ListAction as PlataformaList;
use App\Application\Actions\Plataforma\ViewAction as PlataformaView;
use App\Application\Actions\Publicacion\ListAction as PublicacionList;
use App\Application\Actions\Publicacion\ViewAction as PublicacionView;
use App\Application\Actions\Publicacion\AddAction as PublicacionAdd;
use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequesstInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Slim\Routing\RouteCollectorProxy;
use Slim\Routing\RouteContext;

return function (App $app) {

    $app->map(['GET','POST'], '',  function (Request $request, Response $response) {
        // $response->getBody()->write('Hello world!');
        $response->getBody()->write("WebServices Portal Facturación");
        return $response;
    });

    // $app->map(['DELETE','PUT'], '/CFDIConsultar',  function (Request $request, Response $response) {
    //     // $response->getBody()->write('Hello world!');
    //     $response->getBody()->write("consultar cfdi");
    //     return $response;
    // });

    // $app->map(['GET','POST'], '/CFDIConsultar', '\sifei:CFDIConsultar');
    $app->map(['GET','POST'], '/CFDIConsultar', sifei::class . ':CFDIConsultar');
    // $app->get('/CFDIConsultar', \sifei::class . ':CFDIConsultar');
    // $app->get('/', function (Request $request, Response $response) {
    //     $response->getBody()->write('Hello world!');
    //     return $response;
    // });
    $app->map(['GET','POST'], '/CFDITimbrar', sifei::class . ':CFDITimbrar');
    // $app->map(['GET','POST'], '/catalogo/UsoCFDI', sifei::class . ':UsoCFDI');
    // $app->map(['GET','POST'], '/catalogo/Plataformas', portal::class . ':Plataformas');
    // $app->map(['GET','POST'], '/catalogo/Unidades', portal::class . ':CatalogoUnidades');
    // $app->map(['GET','POST'], '/catalogo/Unidades/{ClaveUnidad}', portal::class . ':CatalogoUnidades');
    // $app->map(['GET','POST'], '/catalogo/ProductoServicio/{ClaveProductoServicio}', portal::class . ':CatalogoProductoServicio');

    $app->map(['GET','POST'],'/hello/{name}', function (Request $request, Response $response, array $args) {
        $app->map(['GET','POST'], '/Folio', portal::class . ':ConsultaVenta');
        $app->map(['GET','POST'], '/consulta/venta', portal::class . ':ConsultaVenta');
        $name = $args['name'];
        $response->getBody()->write("Hello, $name");
        return $response;
    });

    $app->map(['GET','POST'],'/login', portal::class. ':login');

    $app->group('/catalogo', function (Group $group) {
        // $group->map(['GET','POST'],'', function (Request $request, Response $response) {
        // // $response->getBody()->write('Hello world!');
        //     $response->getBody()->write("Consumo de Catálogos");
        //     return $response;
        // });
        $group->map(['GET','POST'],'/uso-cfdi', sifei::class. ':CatalogoUsoCFDI');
        $group->map(['GET','POST'],'/regimen-fiscal', sifei::class. ':CatalogoRegimenFiscal');
        $group->map(['GET','POST'],'/forma-pago', sifei::class. ':CatalogoFormaPago');
        
        $group->map(['GET','POST'],'/tipo-venta', portal::class. ':CatalogoTipoVenta');
        // $group->map(['GET','POST'],'/plataforma', portal::class. ':CatalogoPlataformas');

        // $group->group('/publicacion', function (Group $_group) {
        //     $_group->map(['GET','POST'],'', PublicacionList::class);
        //     // $_group->map(['GET','POST'],'/{ClaveUnidad}', portal::class. ':CatalogoUnidades');
        // });

        $group->group('/plataforma', function (Group $_group) {
            $_group->map(['GET','POST'],'', PlataformaList::class);
            $_group->map(['GET','POST'],'/{id}', PlataformaView::class);
            $_group->map(['GET','POST'],'/acronimo/{acronimo}', PlataformaView::class);
            // $_group->map(['GET','POST'],'/{ClaveUnidad}', portal::class. ':CatalogoUnidades');
        });

        $group->group('/unidad', function (Group $_group) {
            $_group->map(['GET','POST'],'', portal::class. ':CatalogoUnidades');
            $_group->map(['GET','POST'],'/{ClaveUnidad}', portal::class. ':CatalogoUnidades');
        });

        // $group->map(['GET','POST'],'/producto-servicio/{ClaveProductoServicio}', portal::class. ':CatalogoProductoServicio');
        //otra forma de hacerlo
        $group->group('/producto-servicio', function (Group $_group) {
            $_group->map(['GET','POST'],'', portal::class. ':CatalogoProductoServicio');
            $_group->map(['GET','POST'],'/{ClaveProductoServicio}', portal::class. ':CatalogoProductoServicio');
        });

    });

    $app->group('/venta', function (Group $group) {
        $group->map(['GET','POST'],'', portal::class. ':cadenaAleatoria');
        $group->map(['GET','POST'],'/registro', portal::class. ':VentaRegistro');
        $group->map(['GET','POST'],'/consulta', portal::class. ':VentaConsulta');
        $group->map(['GET','POST'],'/consulta/filtro', portal::class. ':VentaConsultaFiltro');
        $group->map(['GET','POST'],'/cancelar/{folio_venta}', portal::class. ':VentaEliminacion');
        $group->map(['GET','POST'],'/cancelar/{folio_venta}/{pk_venta}', portal::class. ':VentaEliminacion');
        $group->map(['GET','POST'],'/timbrar', portal::class. ':VentaFacturar');
    });

    // $app->group('/publicacion', function (Group $group) {

    //     $group->map(['GET','POST'],'/registro', portal::class. ':publicacionRegistro');
    //     // $group->map(['GET','POST'],'/consulta', portal::class. ':VentaConsulta');
    //     // $group->map(['GET','POST'],'/consulta/filtro', portal::class. ':VentaConsultaFiltro');
    //     // $group->map(['GET','POST'],'/cancelar/{folio_venta}', portal::class. ':VentaEliminacion');
    //     // $group->map(['GET','POST'],'/cancelar/{folio_venta}/{pk_venta}', portal::class. ':VentaEliminacion');
    //     // $group->map(['GET','POST'],'/timbrar', portal::class. ':VentaFacturar');
    // });

    $app->group('/facturacion', function (Group $group) {
        // $group->map(['GET','POST'],'/registro', portal::class. ':VentaRegistro');
        // $group->map(['GET','POST'],'/consulta', portal::class. ':VentaConsulta');
        $group->map(['GET','POST'],'/consulta/filtro', portal::class. ':FacturacionConsultaFiltro');
        // $group->map(['GET','POST'],'/cancelar/{uuid}', portal::class. ':VentaEliminacion');
    });


    // $app->map('/CFDIConsultar', 'authenticate', '\SIFEIWS:CFDIConsultar' )->via('GET', 'POST')->name('CFDIConsultar');
    // $app->map('/abc', function (Request $request, Response $response, array $args) {
    //     $name = $args['name'];
    //     $response->getBody()->write("Hello, $name");
    //     return $response;
    // })->via('GET', 'POST')->name('abc');

    // $app->group('/publicaciones', function (Group $group) {
    //     $group->map(['GET','POST'],'', PublicacionList::class);
    //     $group->map(['GET','POST'],'/{id}', PublicacionView::class);
    //     $group->map(['GET','POST'],'/acronimo/{acronimo}', PublicacionView::class);
    // });

    $app->group('/publicacion', function (Group $group) {
        $group->map(['GET','POST'],'/registro', PublicacionAdd::class);
        $group->map(['GET','POST'],'/{id}', PublicacionView::class);
    });

    $app->group('/users', function (Group $group) {
        $group->map(['GET','POST'],'', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    
    // This middleware will append the response header Access-Control-Allow-Methods with all allowed methods
    $app->add(function (Request $request, RequestHandler $handler): Response {
        $routeContext = RouteContext::fromRequest($request);
        $routingResults = $routeContext->getRoutingResults();
        $methods = $routingResults->getAllowedMethods();
        $requestHeaders = $request->getHeaderLine('Access-Control-Request-Headers');

        $response = $handler->handle($request);

        $response = $response->withHeader('Access-Control-Allow-Origin', '*');
        $response = $response->withHeader('Access-Control-Allow-Methods', implode(',', $methods));
        $response = $response->withHeader('Access-Control-Allow-Headers', $requestHeaders);

    // Optional: Allow Ajax CORS requests with Authorization header
    // $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');

        return $response;
    });

};
