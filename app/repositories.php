<?php
declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use App\Domain\Publicacion\PublicacionRepository;
use App\Infrastructure\Persistence\Publicacion\InMemoryPublicacionRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(InMemoryUserRepository::class),
    ]);
    // $containerBuilder->addDefinitions([
    //     PublicacionRepository::class => \DI\autowire(InMemoryPublicacionRepository::class),
    // ]);
};
