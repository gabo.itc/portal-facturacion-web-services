<?php
declare(strict_types=1);

namespace App\Application\Actions\Plataforma;

use App\Application\Actions\Action;
use App\Domain\Plataforma\PlataformaRepository;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;

class ViewAction extends Action
{
    /**
     * @var publicacionrepository
     */
    protected $plataformaRepository;

    /**
     * @param LoggerInterface $logger
     * @param PlataformaRepository $plataformaRepository
     */
    public function __construct(LoggerInterface $logger,
                                PlataformaRepository $plataformaRepository
    ) {
        parent::__construct($logger);
        $this->plataformaRepository = $plataformaRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if(isset( $this->args['id'] )){
            $id = (int) $this->resolveArg('id');

            $this->logger->info("Plataforma del id `${id}` visualizado.");

            $plataforma = $this->plataformaRepository->findById($id);
        }
        if(isset( $this->args['acronimo'] )){
            $acronimo = $this->resolveArg('acronimo');

            $this->logger->info("Plataforma del acronimo `${acronimo}` visualizado.");

            $plataforma = $this->plataformaRepository->findByAcronimo($acronimo);
        }

        return $this->respondWithData($plataforma);
    }
}
