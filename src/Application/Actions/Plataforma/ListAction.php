<?php
declare(strict_types=1);

namespace App\Application\Actions\Plataforma;

use App\Application\Actions\Action;
use App\Domain\Plataforma\PlataformaRepository;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;

class ListAction extends Action
{
    /**
     * @var publicacionrepository
     */
    protected $plataformaRepository;

    /**
     * @param LoggerInterface $logger
     * @param PublicacionRepository $plataformaRepository
     */
    public function __construct(LoggerInterface $logger,
        PlataformaRepository $plataformaRepository
    ) {
        parent::__construct($logger);
        $this->plataformaRepository = $plataformaRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $plataforma = $this->plataformaRepository->findAll();

        $this->logger->info("consulta plataformas devuelta");

        return $this->respondWithData($plataforma);
    }
}