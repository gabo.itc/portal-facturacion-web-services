<?php
declare(strict_types=1);

namespace App\Application\Actions;

use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;

abstract class Action
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        try {
            return $this->action();
        } catch (DomainRecordNotFoundException $e) {
            throw new HttpNotFoundException($this->request, $e->getMessage());
        }
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    abstract protected function action(): Response;

    /**
     * @return array|object
     * @throws HttpBadRequestException
     */
    protected function getFormData()
    {
        $data = file_get_contents('php://input');
        if(empty($data)){
            throw new HttpBadRequestException($this->request, 'Entrada vacia.');
        }else{

            $input = json_decode($data);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new HttpBadRequestException($this->request, 'Entrada JSON con formato incorrecto.');
            }
        }

        return $input;
    }

    private function getParametros(){

        $json = file_get_contents('php://input');
        $params = json_decode($json);
        
        return $params;
    }

    /**
     * 
     */
    protected function validFormData(array $parametros_obligatorios=array()): string
    {
        // extract($this->getParametros());//extraemos las variables post
        $parametros = $this->getFormData();//extraemos las variables post
        $parametros_no_recibidos = "";

        foreach ($parametros_obligatorios as $key => $value) {
            if(is_null($parametros) || !isset($parametros->$value)){/*si la variable existe*/
                $parametros_no_recibidos .= empty($parametros_no_recibidos) ? "" : ",";
                $parametros_no_recibidos .= $value;
            }
        }

        return $parametros_no_recibidos;
    }
    

    /**
     * @param  string $name
     * @return mixed
     * @throws HttpBadRequestException
     */
    protected function resolveArg(string $name)
    {
        if (!isset($this->args[$name])) {
            throw new HttpBadRequestException($this->request, "Parametro `{$name}` no encontrado.");
        }

        return $this->args[$name];
    }

    /**
     * @param array|object|null $data
     * @param int $statusCode
     * @return Response
     */
    protected function respondWithData($data = null, int $statusCode = 200): Response
    {
        $payload = new ActionPayload($statusCode, $data);

        return $this->respond($payload);
    }

    /**
     * @param ActionPayload $payload
     * @return Response
     */
    protected function respond(ActionPayload $payload): Response
    {
        $json = json_encode($payload, JSON_PRETTY_PRINT);
        $this->response->getBody()->write($json);

        return $this->response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus($payload->getStatusCode());
    }
}
