<?php
declare(strict_types=1);

namespace App\Application\Actions\Publicacion;

use App\Application\Actions\Action;
use App\Domain\Publicacion\PublicacionRepository;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Portal\PortalException;

class AddAction extends Action
{
    /**
     * @var publicacionrepository
     */
    protected $repository;

    /**
     * @param LoggerInterface $logger
     * @param PublicacionRepository $repository
     */
    public function __construct(LoggerInterface $logger,
        PublicacionRepository $repository
    ) {
        parent::__construct($logger);
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $parametros_obligatorios = array('publicaciones');
        $parametros_no_recibidos = $this->validFormData($parametros_obligatorios);
        if(!empty($parametros_no_recibidos)){
            throw new PortalException("Parametros no recibidos [$parametros_no_recibidos]");
        }
        $json = $this->getFormData()->publicaciones;

        $publicacion = $this->repository->add($json);

        $this->logger->info("registro de publicacion");

        return $this->respondWithData($publicacion);
    }
}