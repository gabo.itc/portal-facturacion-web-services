<?php
declare(strict_types=1);

namespace App\Application\Actions\Publicacion;

use App\Application\Actions\Action;
use App\Domain\Publicacion\PublicacionRepository;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;

class ListAction extends Action
{
    /**
     * @var publicacionrepository
     */
    protected $publicacionRepository;

    /**
     * @param LoggerInterface $logger
     * @param PublicacionRepository $publicacionRepository
     */
    public function __construct(LoggerInterface $logger,
        PublicacionRepository $publicacionRepository
    ) {
        parent::__construct($logger);
        $this->publicacionRepository = $publicacionRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $publicacion = $this->publicacionRepository->findAll();

        $this->logger->info("Publicacion list was viewed.");

        return $this->respondWithData($publicacion);
    }
}