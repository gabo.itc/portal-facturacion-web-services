<?php
declare(strict_types=1);

namespace App\Application\Actions\Publicacion;

use App\Application\Actions\Action;
use App\Domain\Publicacion\PublicacionRepository;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;

class ViewAction extends Action
{
    /**
     * @var publicacionrepository
     */
    protected $publicacionRepository;

    /**
     * @param LoggerInterface $logger
     * @param PublicacionRepository $publicacionRepository
     */
    public function __construct(LoggerInterface $logger,
        PublicacionRepository $publicacionRepository
    ) {
        parent::__construct($logger);
        $this->publicacionRepository = $publicacionRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if(isset( $this->args['id'] )){
            $pk_publicacion = (int) $this->resolveArg('id');

            $this->logger->info("publicacion del id `${pk_publicacion}` visualizado.");
            
            $publicacion = $this->publicacionRepository->findById($pk_publicacion);
        }
        if(isset( $this->args['acronimo'] )){
            $acronimo = $this->resolveArg('acronimo');

            $this->logger->info("publicacion del acronimo `${acronimo}` visualizado.");
            
            $publicacion = $this->publicacionRepository->findByAcronimo($acronimo);
        }

        return $this->respondWithData($publicacion);
    }
}
