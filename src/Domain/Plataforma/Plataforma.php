<?php
declare(strict_types=1);

namespace App\Domain\Plataforma;

use JsonSerializable;

class Plataforma implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $pk_plataforma;

    /**
     * @var string
     */
    private $plataforma_acronimo;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $estatus;

    /**
     * @param int|null  $id
     * @param string    $username
     * @param string    $firstName
     * @param string    $lastName
     */
    public function __construct(?int $pk_plataforma,string $plataforma_acronimo,string $descripcion,string $estatus)
    {
        $this->pk_plataforma = $pk_plataforma;
        $this->plataforma_acronimo = $plataforma_acronimo;
        $this->descripcion = $descripcion;
        $this->estatus = $estatus;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            // 'pk_plataforma' => $this->pk_plataforma,
            'plataforma_acronimo' => $this->plataforma_acronimo,
            'descripcion' => $this->descripcion,
            'estatus' => $this->estatus,
        ];
    }
}
