<?php
declare(strict_types=1);

namespace App\Domain\Plataforma;


use WebServices\portal\ConexionDB;
use App\Domain\Portal\PortalException;
use PDO;

class PlataformaRepository
{
    /**
     * @var User[]
     */
    private $plataforma = [];

    public function consulta(int $pk_plataforma=null,string $acronimo=null){
        /*Consulta para obtener los timbres usados*/
        $conexion = new ConexionDB();

        $condicion = isset($pk_plataforma) ? "AND pk_plataforma = '$pk_plataforma'":""; 
        $condicion = isset($acronimo) ? "AND plataforma_acronimo = '$acronimo'":$condicion; 

        $sql = "SELECT * FROM ".cat_plataforma." MV WHERE estatus = 'A' $condicion";
        $stmt = $conexion->queryPrepare($sql);
        $stmt->execute();
        $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(sizeof($datos) > 0){

            foreach ($datos as $key => $data) {
                $plataforma = new Plataforma(intval($data['pk_plataforma']),$data['plataforma_acronimo'],$data['descripcion'],$data['estatus']); 
                $this->plataforma[] = $plataforma;
            }
        }
    }

    
    /**
     * @return Plataforma[]
     */
    public function findAll(): array
    {   
        $this->consulta();
        return array_values($this->plataforma);
    }

    /**
    * @param int $id
    * @return Plataforma
    * @throws UserNotFoundException
    */
    public function findById(int $id): Plataforma
    {
        $this->consulta($id);
        if (sizeof($this->plataforma) <= 0) {
            throw new PlataformaNotFoundException();
        }
        return $this->plataforma[0];
    }

    /**
    * @param int $id
    * @return Plataforma
    * @throws UserNotFoundException
    */
    public function findByAcronimo(string $acronimo,?bool $devulve_throw = true):Plataforma
    {
        $this->consulta(null,$acronimo);
        if (sizeof($this->plataforma) <= 0) {
            if($devulve_throw){
                throw new PortalException("Identificador de Plataforma[$acronimo] No Encontrado.");
            }else{
                return null;
            }
        }else{
        // if (sizeof($this->plataforma) > 1) {
        //     return array_values($this->plataforma);
        // }else{
            return $this->plataforma[0];
        // }
        }
    }
}
