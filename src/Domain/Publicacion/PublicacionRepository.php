<?php
declare(strict_types=1);

namespace App\Domain\Publicacion;
use App\Domain\Plataforma\PlataformaRepository;
use App\Domain\Portal\PortalException;
use WebServices\portal\ConexionDB;
use PDO;

class PublicacionRepository
{
    /**
     * array de resultados recuperado de la base
     * @var data[]
     */
    private $data;

    private $conexion;

    public function __construct(){
        $this->conexion = new ConexionDB();
    }

    private function consulta(int $pk=null){

        $condicion = isset($pk) ? "AND pk_publicacion = '$pk'":""; 

        $sql = "SELECT * FROM ".cat_publicacion." MV WHERE estatus = 'A' $condicion";
        $stmt = $this->conexion->queryPrepare($sql);
        $stmt->execute();
        $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(sizeof($datos) > 0){

            foreach ($datos as $key => $data) {
                $data = new Publicacion(intval($data['pk_publicacion']),$data['fecha_publicacion'],$data['plataforma_acronimo'],$data['titulo'],$data['numero_identificador'],$data['unidad'],$data['sat_clave_unidad'],$data['sat_clave_producto_servicio'],floatval($data['tasa_iva']),$data['aplica_iva'],$data['incluye_iva'],floatval($data['importe']),$data['link'],$data['estatus']); 
                $this->publicacion[] = $data;
            }
        }
    }

    /**
     * 
     */
    private function consultaPlataforma(int $pk=null){

        $condicion = isset($pk) ? "AND pk_publicacion = '$pk'":""; 

        $sql = "SELECT * FROM ".cat_publicacion." MV WHERE estatus = 'A' $condicion";
        $stmt = $this->conexion->queryPrepare($sql);
        $stmt->execute();
        $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(sizeof($datos) > 0){

            foreach ($datos as $key => $data) {
                $data = new Publicacion(intval($data['pk_publicacion']),$data['fecha_publicacion'],$data['plataforma_acronimo'],$data['titulo'],$data['numero_identificador'],$data['unidad'],$data['sat_clave_unidad'],$data['sat_clave_producto_servicio'],floatval($data['tasa_iva']),$data['aplica_iva'],$data['incluye_iva'],floatval($data['importe']),$data['link'],$data['estatus']); 
                $this->publicacion[] = $data;
            }
        }
    }

    
    /**
     * @return Publicacion[]
     */
    public function findAll(): array
    {   
        $this->consulta();
        return array_values($this->publicacion);
    }

    /**
    * @param int $id
    * @return Publicacion
    * @throws UserNotFoundException
    */
    public function findById(int $id): Publicacion
    {
        $this->consulta($id);
        if (sizeof($this->publicacion) <= 0) {
            throw new PortalException("Identificador de Publicación[$acronimo] No Encontrado.");
        }
        return $this->publicacion[0];
    }

    /**
     * agrega una lista de elementos con la informacion de publicaciones
     * @param Publicacion $data
     * @return string
     */
    public function add(array $data_array): string
    {

        $cadena_insert = "";
        foreach ($data_array as $key => $data) {
            if($data->fecha_publicacion==''){
                $data->fecha_publicacion = '1900-01-01';
            }
            $plataforma = new PlataformaRepository();
            $plataforma = $plataforma->findByAcronimo($data->plataforma_acronimo);
            
            $data->estatus = $data->estatus?? "A";
            $cadena_insert .= empty($cadena_insert) ? "":",";
            $cadena_insert .= "("                
            ."'$data->fecha_publicacion','$data->plataforma_acronimo','$data->titulo',"
            ."'$data->identificador','$data->unidad','$data->sat_clave_unidad',"
            ."'$data->sat_claveprodserv','$data->importe','$data->link','$data->estatus'"
            .")";
        }

        if(!empty($cadena_insert)){
            $sql = "INSERT INTO ".cat_publicacion." (fecha_publicacion,plataforma_acronimo,titulo,numero_identificador,unidad,sat_clave_unidad,sat_clave_producto_servicio,importe,link,estatus) VALUES {$cadena_insert}";
            $stmt = $this->conexion->queryPrepare($sql);

            $stmt->execute();

            $respuesta = "";
            if(sizeof($data_array)==1){
                $respuesta = "Publicacion Registrada";
            }else{
                $respuesta = "Publicaciones Registradas";
            }
        }else{
            throw new PortalException("Publicaciones No Registradas");
        }


        return $respuesta;
    }

}
