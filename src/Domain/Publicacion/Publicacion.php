<?php
declare(strict_types=1);

namespace App\Domain\Publicacion;

use JsonSerializable;

class Publicacion implements JsonSerializable
{
    /**
     * @var int|null
     */
    protected $pk_publicacion;

    /**
     * @var string
     */
    protected $fecha_publicacion;

    /**
     * @var string
     */
    protected $plataforma_acronimo;

    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var string
     */
    protected $numero_identificador;

    /**
     * @var string
     */
    protected $unidad;

    /**
     * @var string
     */
    protected $sat_clave_unidad;

    /**
     * @var string
     */
    protected $sat_clave_producto_servicio;

    /**
     * @var float
     */
    protected $tasa_iva;

    /**
     * @var string
     */
    protected $aplica_iva;

    /**
     * @var string
     */
    protected $incluye_iva;

    /**
     * @var float
     */
    protected $importe;

    /**
     * @var string
     */
    protected $link;

    /**
     * @var string
     */
    protected $estatus;

    /**
     * @param int|null  $id
     * @param string    $username
     * @param string    $firstName
     * @param string    $lastName
     */
    public function __construct(?int $pk_publicacion,string $fecha_publicacion,string $plataforma_acronimo,string $titulo,string $numero_identificador,string $unidad,string $sat_clave_unidad,string $sat_clave_producto_servicio,float $tasa_iva,string $aplica_iva,string $incluye_iva,float $importe,string $link,string $estatus)
    {
        $this->pk_publicacion = $pk_publicacion;
        $this->fecha_publicacion = $fecha_publicacion;
        $this->plataforma_acronimo = $plataforma_acronimo;
        $this->titulo = $titulo;
        $this->numero_identificador = $numero_identificador;
        $this->unidad = $unidad;
        $this->sat_clave_unidad = $sat_clave_unidad;
        $this->sat_clave_producto_servicio = $sat_clave_producto_servicio;
        $this->tasa_iva = $tasa_iva;
        $this->aplica_iva = $aplica_iva;
        $this->incluye_iva = $incluye_iva;
        $this->importe = $importe;
        $this->link = $link;
        $this->estatus = $estatus;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            // 'id' => $this->pk_publicacion,
            'fecha_publicacion' => $this->fecha_publicacion,
            'plataforma_acronimo' => $this->plataforma_acronimo,
            'titulo' => $this->titulo,
            'numero_identificador' => $this->numero_identificador,
            'unidad' => $this->unidad,
            'sat_clave_unidad' => $this->sat_clave_unidad,
            'sat_clave_producto_servicio' => $this->sat_clave_producto_servicio,
            'tasa_iva' => $this->tasa_iva,
            'aplica_iva' => $this->aplica_iva,
            'incluye_iva' => $this->incluye_iva,
            'importe' => $this->importe,
            'link' => $this->link,
            'estatus' => $this->estatus,
        ];
    }
}
