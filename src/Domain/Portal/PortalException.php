<?php
declare(strict_types=1);

namespace App\Domain\Portal;

use Exception;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use WebServices\portal\Funciones;

class PortalException extends Exception
{
    /**
     * @var int
     */
    protected $code = 400;
    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @param ServerRequestInterface $request
     * @param string                 $message
     * @param int                    $code
     * @param Throwable|null         $previous
     */
    public function __construct(
        string $message = '',
        ?ServerRequestInterface $request = null,
        int $code = 0,
        ?Throwable $previous = null
    ) {
        if($code == 0){
            $code = $this->code;
        }
        
        // $name_function = json_encode($this->getTrace()[0]);
        $name_function = $this->getTrace()[0]['class'];

        // $message .= ' '.$name_function;
        // $message .= ' '.$this->getCode();
        // $message .= ' '.$code;

        parent::__construct($message, $code, $previous);
        $this->request = $request;
    }

    private function register()
    {
        $funcion = self::$funcion_nombre;
        if(is_null($estatus_code)){
            $estatus_code = self::$codigo_estatus;
        }

        $contenidoError = $this->getMessage();
        if(isset(self::$sifeiCodigo)){
            $contenidoError = "";
            $contenidoError .= ";Codigo: ".self::$sifeiCodigo;
            $contenidoError .= ";Error: ".self::$sifeiError;
            $contenidoError .= ";Mensaje: ".self::$sifeiMessage;
        }

        $errorMsg = 'Error en la Linea: '.$this->getLine().' en '.$this->getFile().': '.$contenidoError;

        $respuesta = array();
        $respuesta["error"]   = true;
        $respuesta["message"] = $this->getMessage(); 
        $respuesta["body"]    = null;

        $cadenaMysql =  ((json_encode($respuesta,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)));
        // $errorMsg .= ' Codificacion: '.$cadenaMysql;
        // echo "l ".$this->getTrace()[2]['function'].PHP_EOL;
        // var_dump($this->getTrace()[2]);
        // $funcion = $this->getTrace()[2]['function'];
        // Funciones::cargarSolicitud("No User",$funcion, $estatus_code,json_encode($response),$errorMsg);
        /**
         * nombre de la funcion que mando la excepcion
         * @var string
         */
        $name_function = $this->getTrace()[0]['class'];
        $timbres_usados = 0;
        Funciones::cargarSolicitud($name_function,$timbres_usados, $estatus_code,$cadenaMysql,$errorMsg);
    }

    /**
     * @return ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }
}
