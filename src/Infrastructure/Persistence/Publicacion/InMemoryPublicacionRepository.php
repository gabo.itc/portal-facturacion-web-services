<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Publicacion;

use App\Domain\Publicacion\Publicacion;
use App\Domain\Publicacion\PublicacionNotFoundException;
use WebServices\portal\ConexionDB;
use PDO;

class InMemoryPublicacionRepository
{
    /**
     * @var User[]
     */
    private $publicacion;

    /**
     * InMemoryPublicacionRepository constructor.
     *
     * @param array|null $publicacion
     */
    public function __construct(array $publicacion = null)
    {
        // $this->publicacion = $publicacion ?? [
        //     0 => new Publicacion(0, '1900-01-01', 'ML', 'Venta de Prueba','01','E48','E48','01010101',0.0000,'N','N',500.00,'','A'),
        //     1 => new Publicacion(1, '1900-01-01', 'ML', 'Venta de Prueba 1','01','E48','E48','01010101',0.0000,'N','N',500.00,'','A'),
        //     2 => new Publicacion(2, '1900-01-01', 'ML', 'Venta de Prueba 2','01','E48','E48','01010101',0.0000,'N','N',500.00,'','A'),
        //     3 => new Publicacion(3, '1900-01-01', 'ML', 'Venta de Prueba 3','01','E48','E48','01010101',0.0000,'N','N',500.00,'','A'),
        //     4 => new Publicacion(4, '1900-01-01', 'ML', 'Venta de Prueba 4','01','E48','E48','01010101',0.0000,'N','N',500.00,'','A'),
        //     5 => new Publicacion(5, '1900-01-01', 'ML', 'Venta de Prueba 5','01','E48','E48','01010101',0.0000,'N','N',500.00,'','A')
        // ];
    }

    private function consulta(int $pk_publicacion=null,string $acronimo=null){
    	/*Consulta para obtener los timbres usados*/
		$conexion = new ConexionDB();
		// $sql = "SELECT * FROM ".cat_publicacion." MV WHERE estatus = 'A' /* AND pk_publicacion = '{$parametros->pk_publicacion}'*/";
		$condicion_pk = isset($pk_publicacion) ? "AND pk_publicacion = '$pk_publicacion'":""; 
		$sql = "SELECT * FROM ".cat_publicacion." MV WHERE estatus = 'A' $condicion_pk";
		$stmt = $conexion->queryPrepare($sql);
		$stmt->execute();
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if(sizeof($datos) > 0){

			foreach ($datos as $key => $data) {
				$publicacion = new Publicacion(intval($data['pk_publicacion']),$data['fecha_publicacion'],$data['plataforma_acronimo'],$data['titulo'],$data['numero_identificador'],$data['unidad'],$data['sat_clave_unidad'],$data['sat_clave_producto_servicio'],floatval($data['tasa_iva']),$data['aplica_iva'],$data['incluye_iva'],floatval($data['importe']),$data['link'],$data['estatus']); 
				$this->publicacion[] = $publicacion;
			}
		}
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {	
    	$this->consulta();
        return array_values($this->publicacion);
    }

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): Publicacion
    {
        $this->consulta($id);

        if (sizeof($this->publicacion)>0) {
            throw new PublicacionNotFoundException();
        }


        return $this->publicacion[0];
    }
}
